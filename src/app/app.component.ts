import { Component, OnInit, AfterContentInit } from "@angular/core";
import { BreadcrumbService } from "./services/breadcrumb/breadcrumb.service";
import { AutenticacionService } from './services/auth/autenticacion.service';
import * as jQuery from "jquery";
import { MessageBoxService } from './services/message_box_app/message-box.service';
import { ApiService } from './services/api/api.service';

function estilosContenBody() {
  var h = jQuery(window).height();
  h = h - 172;
  jQuery("div#content-body").css({ "min-height": h + "px" });
}

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.sass"],
})
export class AppComponent implements OnInit {

  constructor(private apiService: ApiService, private breadcrumbService: BreadcrumbService, private autenticacionService: AutenticacionService, private messageBoxService: MessageBoxService) {
    this.breadcrumbService.initBreadcrumb();
  }

  ngOnInit() {
    estilosContenBody();
  }
}
