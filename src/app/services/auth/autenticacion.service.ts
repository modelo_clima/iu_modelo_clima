import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";

import { ApiService } from "../api/api.service";

import { JwtResponseI } from "src/app/classmodels/interfaces/auth/jwt-response";
import { Router } from '@angular/router';
@Injectable({
  providedIn: "root",
})

export class AutenticacionService {
  headers: HttpHeaders = new HttpHeaders({
    Authorization: "jwt " + this.getToken(),
  });

  public menu: any = [];

  constructor(public apiService: ApiService, private httpClient: HttpClient, private router: Router) { }

  /**
   * loginuser(username, password)
   * @author Brayan Hernan Castillo Rodriguez
   * Realiza transacción Post a la ruta api/token
   * @return @JSON JwtResponseI
   */
  loginuser(username: string, password: string): Observable<any> {
    return this.httpClient.post<JwtResponseI>(`${this.apiService.urlAPI}token/`, { username, password }, { headers: this.headers });
    ///  .pipe(map((data) => data));
  }

  /**
   * refreshToken(token)
   * @author Brayan Hernan Castillo Rodriguez
   * Realiza transacción Post a la ruta api/token/refresh/,
   * @return @string token
   */
  refreshToken(token: string, metodo?: () => void): Observable<any> {
    return this.httpClient.post<JwtResponseI>(
      `${this.apiService.urlAPI}token/refresh/`,
      { token },
      { headers: this.headers }
    );
    // .pipe(map((data) => data));
  }

  /**
   * getToken()
   * @author Brayan Hernan Castillo Rodriguez
   * consulta de LocalStorage "accessToken"
   * @return @string token
   *
   */
  getToken() {
    return localStorage.getItem("accessToken");
  }

  /**
   * setToken(token)
   * @author Brayan Hernan Castillo Rodriguez
   * almacena parametro token en localStorage "accessToken"
   */
  setToken(token) {
    localStorage.setItem("accessToken", token);
  }
  /**
   * setUser(first_name)
   * @author Brayan Hernan Castillo Rodriguez
   * almacena parametro estado en localStorage "currentUser"
   */
  setUser(first_name: JwtResponseI) {
    let user_string = JSON.stringify(first_name);
    localStorage.setItem("currentUser", user_string);
  }

  loadMenu() {
    let menu = localStorage.getItem("menu_user");
    if (menu != null) {
      this.menu = JSON.parse(localStorage.getItem("menu_user"));
    }
  }

  islogged() {
    let statusLogged = localStorage.getItem("statusLogged");
    let accessToken = localStorage.getItem("accessToken");
    if (statusLogged != null && accessToken != null && parseInt(statusLogged) == 1) {
      return true;
    }
    return false;
  }

  /**
   * logout()
   * @author Brayan Hernan Castillo Rodriguez
   * Borra todas las LocalStorage almacenadas,
   * recarga pagina, redireccionando a pagina de login
   */
  logout() {
    localStorage.clear()
    this.router.navigate(["/auth/login"]);
  }

}
