import { Injectable } from '@angular/core';
import { ApiService } from '../api/api.service';
import { Router } from '@angular/router';
import { MessageBoxService } from '../message_box_app/message-box.service';
import { labels_app } from 'src/app/config_app/labels-app';
import { AutenticacionService } from '../auth/autenticacion.service';

@Injectable({
  providedIn: 'root'
})
export class PermisosAppService {

  url: string;
  permisos: any;

  constructor(private apiService: ApiService, private router: Router, private messageBoxService: MessageBoxService, private autenticacionService: AutenticacionService) {
    this.url = "";
    this.cleanpermisos();
  }

  private cleanpermisos(permiso?: string) {
    if (permiso == 'consultar') { this.permisos.index = false; }
    else if (permiso == 'crear') { this.permisos.crear = false; }
    else if (permiso == 'modificar') { this.permisos.modificar = false; }
    else if (permiso == 'eliminar') { this.permisos.eliminar = false; }
    else {
      this.permisos = { index: false, crear: false, modificar: false, eliminar: false };
    }
  }

  async loadPermisosAppSeccion(url, permiso?: string) {
    this.url = url;
    return new Promise((resolve, reject) => this.apiService.getApi('permisoapp/permisosUrl/', { url: url, permiso: permiso }).subscribe(
      (data: any) => {
        if (permiso == 'consultar') { this.permisos.index = data.estado; }
        else if (permiso == 'crear') { this.permisos.crear = data.estado; }
        else if (permiso == 'modificar') { this.permisos.modificar = data.estado; }
        else if (permiso == 'eliminar') { this.permisos.eliminar = data.estado; }
        else {
          this.permisos.index = data.consultar;
          this.permisos.crear = data.crear;
          this.permisos.modificar = data.modificar;
          this.permisos.eliminar = data.eliminar;
        }
        resolve(true);
      }, error => {
        if (error.status == 423) {
          this.messageBoxService.showMessageError(labels_app.messages.errorD, () => {
            this.messageBoxService.closeMessage();
            this.autenticacionService.logout();
          });
        } else {
          this.messageBoxService.showMessageError(labels_app.messages.errorB);
        }
        reject(error)
      }
    ));
  }

  onClickCrear(url: string) {
    this.router.navigate([url]);
  }

  onClickModificar(url: string, id) {
    this.router.navigate([url, id]);
  }

  async onClickEliminar(id, classModel, url_seccion: string) {
    let e = await this.loadPermisosAppSeccion(url_seccion, 'eliminar');
    if (e) {
      if (this.permisos.eliminar) {
        this.messageBoxService.showMessageConfirmation(labels_app.messages.msg_cofirm_eliminacion,
          () => {
            this.messageBoxService.closeMessage();
            classModel.deleteItem(id);
          });
      } else {
        this.messageBoxService.showMessageError(labels_app.messages.errorC)
      }
    }
  }

  async onClickEliminarRol(id, classModel, url_seccion: string,classModel2,classModel3) {
    let e = await this.loadPermisosAppSeccion(url_seccion, 'eliminar');
    if (e) {
      if (this.permisos.eliminar) {
        this.messageBoxService.showMessageConfirmation(labels_app.messages.msg_cofirm_eliminacion,
          async() => {
            this.messageBoxService.closeMessage();
            if(!await this.getUsuariosRol(id,classModel2)===true){
              await this.eliminarCascadaclassModel(id, classModel3)
              await classModel.deleteItem(id);
            } else{
              this.messageBoxService.showMessageError(labels_app.messages.msg_rol_relacionado)
            }
            
            
          });
      } else {
        this.messageBoxService.showMessageError(labels_app.messages.errorC)
      }
    }
  }

  onSubmit(classModel, data, operacion: string, urlIndex: string) {
    if (operacion === "crear") {
      classModel.addItem(data, () => {
        this.router.navigate([urlIndex]);
      });
    } else {
      classModel.updateItem(data.id, data, () => {
        this.router.navigate([urlIndex]);
      });
    }
  }

  async eliminarCascadaclassModel(id, classModel){
    let items = await classModel.getItems();
    for (var i = 0; i < classModel.getItems().length; i++) {
      if ( id === (items[i]).id_rol) {
        await classModel.deleteItem(items[i].id);
      }
    }
  }
  async getUsuariosRol(id, classModel){
    let items = await classModel.getItems();
    for (var i = 0; i < classModel.getItems().length; i++) {
      if ( id === (items[i]).id_rol) {
        return true
        
      }
    }
  }
}
