import { TestBed } from '@angular/core/testing';

import { PermisosAppService } from './permisos-app.service';

describe('PermisosAppService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PermisosAppService = TestBed.get(PermisosAppService);
    expect(service).toBeTruthy();
  });
});
