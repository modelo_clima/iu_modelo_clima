import { Injectable } from '@angular/core';
import { IBreadcrumb } from '../../classmodels/interfaces/ibreadcrumb';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class BreadcrumbService {

  private breadcrumb: IBreadcrumb[];

  constructor(private router: Router) {
    this.initBreadcrumb();
  }

  addItemBreadcrumb(item: IBreadcrumb) {
    this.breadcrumb.push(item);
  }

  removeLastItemBreadcrumb() {
    let i = this.breadcrumb.length - 1;
    this.breadcrumb.splice(i, 1);
  }

  getBreadcrumb() {
    return this.breadcrumb;
  }

  initBreadcrumb() {
    this.breadcrumb = [{ "title": "Inicio", "url": "/" }];
  }

  validationAddItem(breadcrumb: IBreadcrumb) {
    let i = 0;
    let rows = this.getBreadcrumb();
    while (i < rows.length) {
      if (rows[i].title == breadcrumb.title) {
        break;
      }
      i++;
    }
    if (i == rows.length) {
      this.addItemBreadcrumb(breadcrumb);
    }
  }

  public goBack(index: number) {
    let i = index + 1;
    if (i < this.breadcrumb.length) {
      this.breadcrumb.splice(i, this.breadcrumb.length + i);
      //this.router.navigate([this.breadcrumb[index].url]);
    }
  }

}
