import { Injectable, OnInit } from "@angular/core";
import { ApiService } from "../api/api.service";
import { Router } from "@angular/router";
import { isNullOrUndefined } from "util";
@Injectable({
  providedIn: "root",
})
export class PermisosService implements OnInit {
  menus: any = [];
  permisosAppSeccion: any = [];
  isForm: boolean = true;
  lengthUrl: number =21; // tamaño de la url
  constructor(public apiService: ApiService, private router: Router) {}

  async ngOnInit() {
    this.menus = await this.getDatosMenu();
  
  }

  async onClickCrear(rutaHija: string) {
    this.permisosAppSeccion = await this.getPermisosSeccionApp(false);

    if (this.permisosAppSeccion["crear"] === true) {
      this.router.navigate([`${rutaHija}/crear`]);
    } else {
      this.router.navigate(["/sinpermisos"]);
    }
  }
  
  async onClickModificar(data: any, rutaHija: string) {
    this.permisosAppSeccion = await this.getPermisosSeccionApp(false);
    if (this.permisosAppSeccion["modificar"] === true) {
      this.router.navigate([`${rutaHija}/modificar`, data.id]);
    } else {
      this.router.navigate(["/sinpermisos"]);
    }
  }

  async onClickEliminar(data, classModel) {
    this.permisosAppSeccion = await this.getPermisosSeccionApp(false);
    let eliminar = this.permisosAppSeccion["eliminar"];
    if (eliminar === true) {
      classModel.deleteItem(data.id);
    } else {
      this.router.navigate(["/sinpermisos"]);
    }
  }

  async onSubmit(accion: string, classModel, data) {
    console.log('data :', data);
    console.log('accion :', accion);
    if (accion === "crear") {
      this.permisosAppSeccion = await this.getPermisosSeccionApp(
        this.isForm,
        `/${accion}`
      );
      let permiso = this.permisosAppSeccion[accion];
      if (permiso === true) {
        classModel.addItem(data, () => {});
        this.goToIndex(this.permisosAppSeccion["urlIndex"]);
      } else {
        this.router.navigate(["/sinpermisos"]);
      }
    } else {
      let accionModificar: string = `/${accion}/${data.id}`;

      this.permisosAppSeccion = await this.getPermisosSeccionApp(
        this.isForm,
        accionModificar
      );

      let permiso = this.permisosAppSeccion[accion];
      if (permiso === true) {
        classModel.updateItem(data.id, data, () => {
          this.goToIndex(this.permisosAppSeccion["urlIndex"]);
        });
      } else {
        this.router.navigate(["/sinpermisos"]);
      }
    }
  }

  goToIndex(urlIndex: string) {
    this.router.navigate([urlIndex]);
    //this.router.navigateByUrl(urlIndex);
  }

  // rebicar$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
  getEstado() {
    const ESTADO = new Promise((resolve, reject) =>
      this.apiService.list("authusuario/estado?username=brayan").subscribe((estado) =>
        resolve(estado)
      )
    );
    return ESTADO;
  }

  getRuta() {
    let ruta = document.location.href;
    let rutaformateada = ruta.substr(this.lengthUrl, ruta.length);
    return rutaformateada;
  }

  getSeccion() {
    let seccion = localStorage.getItem("urlSeccion");
    localStorage.removeItem("urlSeccion");
    return seccion;
  }

  getDatosMenu() {
    const PROMESA = new Promise((resolve, reject) =>
      this.apiService.list("seccionapp/permisosmenu").subscribe((data) => {
        resolve(data);
      },(error)=>{
        console.error("error al Obtener permisos de menu",error);
        reject();
      })
    );
    return PROMESA;
  }

  getPermisosSeccion(ruta: string, isForm: boolean) {
    let dbRuta: string;
    if (isForm == true) {
      dbRuta = "url_hija";
    } else {
      dbRuta = "url_index";
    }
    let nombre: string;
    let nombreForm: string;
    let urlIndex: string;
    let urlHija: string;
    let consultar: boolean;
    let crear: boolean;
    let modificar: boolean;
    let eliminar: boolean;
    let seccion: any = [];
    const PROMESA = new Promise((resolve, reject) =>
      this.apiService.list("seccionapp/permisosmenu").subscribe(
        (data) => {
          for (let item of data) {
            for (let i = 0; i < item["submenus"].length; i++) {
              //   console.log("parametro", nombreUrl, "== DB",item["submenus"][i][dbRuta]);
              if (ruta === item["submenus"][i][dbRuta]) {
                nombre = item["submenus"][i]["nombre"];
                nombreForm = item["submenus"][i]["nombre_form"];
                urlIndex = item["submenus"][i]["url_index"];
                urlHija = item["submenus"][i]["url_hija"];
                consultar = item["submenus"][i]["permisos"]["consultar"];
                crear = item["submenus"][i]["permisos"]["crear"];
                modificar = item["submenus"][i]["permisos"]["modificar"];
                eliminar = item["submenus"][i]["permisos"]["eliminar"];
                seccion = {
                  nombre: nombre,
                  nombreForm: nombreForm,
                  urlIndex: urlIndex,
                  urlHija: urlHija,
                  consultar: consultar,
                  crear: crear,
                  modificar: modificar,
                  eliminar: eliminar,
                };
                resolve(seccion);
                break;
              }
            }
          }
        },
        (err) => {
          console.error("error al obtener ",err);
          reject();
        }
      )
    );
    return PROMESA;
  }

  async getPermisosSeccionApp(isForm: boolean, accion?: string) {
    let seccion: string;
    if (isForm === true) {
      seccion = this.getSeccion();

      if (isNullOrUndefined(seccion)) {
        seccion = this.getRuta()
          .substr(1, this.getRuta().length)
          .replace(accion, "");
      }
    } else {
      seccion = this.getSeccion();

      if (isNullOrUndefined(seccion)) {
        seccion = this.getRuta().substr(1, this.getRuta().length);
      }
    }

    let permisosAppSeccion = await this.getPermisosSeccion(
      seccion,
      isForm
    );
    let nombreIndex = await permisosAppSeccion["nombre"];
    let nombreForm = await permisosAppSeccion["nombreForm"];
    let urlIndex = await permisosAppSeccion["urlIndex"];
    let urlHija = await permisosAppSeccion["urlHija"];
    let estado = await this.getEstado();
    let consultar = await permisosAppSeccion["consultar"];
    let crear = await permisosAppSeccion["crear"];
    let modificar = await permisosAppSeccion["modificar"];
    let eliminar = await permisosAppSeccion["eliminar"];
    return {
      nombre: nombreIndex,
      nombreForm: nombreForm,
      estado: estado,
      consultar: consultar,
      crear: crear,
      modificar: modificar,
      eliminar: eliminar,
      urlIndex: urlIndex,
      urlHija: urlHija,
    };
  }
  

  /**
   * logout()
   * @author Brayan Hernan Castillo Rodriguez
   * Borra todas las LocalStorage almacenadas,
   * recarga pagina, redireccionando a pagina de login
   */
  logout(): void {
    localStorage.removeItem("accessToken");
    localStorage.removeItem("currentUser");
    localStorage.removeItem("seccion");
    localStorage.removeItem("estado");
    localStorage.removeItem("rol");
    localStorage.removeItem("consultar");
    localStorage.removeItem("crear");
    localStorage.removeItem("modificar");
    localStorage.removeItem("eliminar");
    location.reload();
    location.replace("/auth/login");
  }
}
