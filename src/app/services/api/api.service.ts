import { Injectable, EventEmitter } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { HttpHeaders } from "@angular/common/http";
import { BehaviorSubject } from "rxjs";
import { isNullOrUndefined } from "util";
import { JwtResponseI } from 'src/app/classmodels/interfaces/auth/jwt-response';

@Injectable({
  providedIn: "root",
})
export class ApiService {

  loadingShow: boolean;
  urlAPI: string = "/api/";
  headers: HttpHeaders;
  private token: string;

  constructor(private httpClient: HttpClient) {
    this.loadingShow = false;
    this.headers = new HttpHeaders({
      Authorization: "jwt " + this.getToken(),
    });
  }

  lista(urlTabla, data) {
    let param = "";
    if (data != null) {
      for (let item in data) {
        param += (param == "" ? "?" : "&") + item + "=" + data[item];
      }
    }
    return this.httpClient.get<any[]>(this.urlAPI + urlTabla + data, {
      headers: this.headers,
    });
  }

  list(urlTabla, data?: {}) {
    let param = "";
    if (data != null) {
      for (let item in data) {
        param += (param == "" ? "?" : "&") + item + "=" + data[item];
      }
    }
    return this.httpClient.get<any[]>(this.urlAPI + urlTabla + param, {
      headers: this.headers,
    });
  }

  detail(urlTabla, id: number) {
    return this.httpClient.get<any>(this.urlAPI + urlTabla + "/" + id, {
      headers: this.headers,
    });
  }

  create(urlTabla, data: any) {
    return this.httpClient.post<any>(this.urlAPI + urlTabla + "/", data, {
      headers: this.headers,
    });
  }

  update(urlTabla, id: number, data: any) {
    return this.httpClient.put<any>(
      this.urlAPI + urlTabla + "/" + id + "/",
      data,
      { headers: this.headers }
    );
  }

  updatePartial(urlTabla, id: number, data: any) {
    return this.httpClient.patch<any>(
      this.urlAPI + urlTabla + "/" + id + "/",
      data,
      { headers: this.headers }
    );
  }

  delete(urlTabla, id: number) {
    return this.httpClient.delete<any>(
      this.urlAPI + urlTabla + "/" + id + "/",
      { headers: this.headers }
    );
  }

  getApi(urlTabla, data?: {}) {
    let param = "";
    if (data != null) {
      for (let item in data) {
        param += (param == "" ? "?" : "&") + item + "=" + data[item];
      }
    }
    return this.httpClient.get<any[]>(this.urlAPI + urlTabla + param, { headers: this.headers });
  }

  /**
   * getToken()
   * @author Brayan Hernan Castillo Rodriguez
   * obtiene el token del usuario
   * @return @string token
   *
   */
  getToken() {
    return localStorage.getItem("accessToken");
  }

  /**
   * getidRol()
   * @author Brayan Hernan Castillo Rodriguez
   * consulta de LocalStorage "rol"
   * @return @int rol
   *
   */
  getidRol() {
    return localStorage.getItem("rol");
  }

  /**
   * getSeccion()
   * @author Brayan Hernan Castillo Rodriguez
   * consulta de LocalStorage "seccion"
   * @return @int seccion
   *
   */
  getSeccion() {
    return localStorage.getItem("seccion");
  }
  /**
   * getPermisoAppConsultar()
   * @author Brayan Hernan Castillo Rodriguez
   * consulta de LocalStorage "consultar"
   * @return @boolean consultar
   *
   */
  getPermisoAppConsultar() {
    return localStorage.getItem("consultar");
  }

  /**
   * getPermisoAppCrear()
   * @author Brayan Hernan Castillo Rodriguez
   * consulta de LocalStorage "crear"
   * @return @boolean crear
   *
   */
  getPermisoAppCrear() {
    return localStorage.getItem("crear");
  }

  /**
   * getPermisoAppModificar()
   * @author Brayan Hernan Castillo Rodriguez
   * consulta de LocalStorage "modificar"
   * @return @boolean modificar
   *
   */
  getPermisoAppModificar() {
    return localStorage.getItem("modificar");
  }

  /**
   * getPermisoAppEliminar()
   * @author Brayan Hernan Castillo Rodriguez
   * consulta de LocalStorage "eliminar"
   * @return @boolean eliminar
   *
   */
  getPermisoAppEliminar() {
    return localStorage.getItem("eliminar");
  }

  /**
   * getCurrentUser()
   * @author Brayan Hernan Castillo Rodriguez
   * obtiene el username del usuario
   *
   */
  getUsername() {
    let username = JSON.parse(localStorage.getItem("currentUser"));
    if (!isNullOrUndefined(username)) {
      return username;
    } else {
      return null;
    }
  }

  /**
   * getEstado()
   * @author Brayan Hernan Castillo Rodriguez
   * consulta de LocalStorage "estado"
   * @return @boolean estado
   *
   */
  getEstado() {
    return localStorage.getItem("estado");
  }

  /**
   * setToken(token)
   * @author Brayan Hernan Castillo Rodriguez
   * almacena parametro token en localStorage "accessToken"
   */
  setToken(token): void {
    localStorage.setItem("accessToken", token);
  }

  /**
   * setEstado(estado)
   * @author Brayan Hernan Castillo Rodriguez
   * almacena parametro estado en localStorage "estado"
   */
  setEstado(estado) {
    localStorage.setItem("estado", estado);
  }

  /**
   * setUser(first_name)
   * @author Brayan Hernan Castillo Rodriguez
   * almacena parametro estado en localStorage "currentUser"
   */
  setUser(first_name: JwtResponseI): void {
    let user_string = JSON.stringify(first_name);
    localStorage.setItem("currentUser", user_string);
  }

  /**
   * setSeccion(id_seccion)
   * @author Brayan Hernan Castillo Rodriguez
   * almacena parametro id_seccion en localStorage "seccion"
   */
  setSeccion(id_seccion) {
    localStorage.setItem("seccion", id_seccion);
  }

  /**
   * setRol(id_rol)
   * @author Brayan Hernan Castillo Rodriguez
   * almacena parametro id_rol en localStorage "rol"
   */
  setRol(id_rol): void {
    //enviar id_rol a una localStorage
    localStorage.setItem("rol", id_rol);
  }


  validarSesion() {
    let token = localStorage.getItem("token");
    if (!isNullOrUndefined(token)) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * logout()
   * @author Brayan Hernan Castillo Rodriguez
   * Borra todas las LocalStorage almacenadas,
   * recarga pagina, redireccionando a pagina de login
   */
  logout(): void {
    this.token = "";
    localStorage.removeItem("accessToken");
    localStorage.removeItem("currentUser");
    localStorage.removeItem("seccion");
    localStorage.removeItem("estado");
    localStorage.removeItem("rol");
    localStorage.removeItem("consultar");
    localStorage.removeItem("crear");
    localStorage.removeItem("modificar");
    localStorage.removeItem("eliminar");

    location.reload();
    location.replace("/auth/login");
  }



}
