import { Injectable } from '@angular/core';
import { MessageBoxSise } from 'src/app/components/template/alertas-sise/message-box';

@Injectable({
  providedIn: 'root'
})
export class MessageBoxService {

  constructor(public messageBox: MessageBoxSise) {

  }

  closeMessage() {
    this.messageBox.closeMessage();
  }

  showMessage(msgTxt: string, onClickCerrar?: () => void) {
    this.messageBox.showMessage('ok', msgTxt, () => {
      if (onClickCerrar) {
        onClickCerrar();
      } else {
        this.messageBox.closeMessage();
      }
    });
  }

  showMessageError(msgTxt: string, onClickCerrar?: () => void) {
    this.messageBox.showMessage('error', msgTxt, () => {
      if (onClickCerrar) {
        onClickCerrar();
      } else {
        this.messageBox.closeMessage();
      }
    });
  }

  showMessageConfirmation(msgTxt: string, onClickSi: () => void, onClickNo?: () => void, onClickCerrar?: () => void) {
    this.messageBox.showMessageConf(msgTxt, onClickSi, onClickNo, onClickCerrar);
  }

}
