export interface IActa {
    "id": number;
    "nombre": string;
    "descripcion": string;    
    "fecha_creacion": String;
    "adjunto": File;
    "id_mesa_tecnica":number;
}
