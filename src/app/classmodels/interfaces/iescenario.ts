export interface IEscenario {
    "id": number;
    "descripcion": string;
    "fecha_creacion": string;
    "fecha_aprobacion": string;
    "id_mesa_tecnica": number;
    "base":number;
    "id_estado": number;
    "codigo": string;
    "estado": string;
    "fecha_inicio_simulacion": Date;
    "fecha_fin_simulacion": Date;
}
