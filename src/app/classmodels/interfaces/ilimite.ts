export interface ILimite {
    "id": number;
    "nombre": string;
    "valor": number;
    "constante":number;
    "id_punto_gestion": number;
    "id_variable": number;
    "id_tipo_uso": number;
}
