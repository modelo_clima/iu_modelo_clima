export interface IUnidadMedida {
    "id": number;
    "nombre": string;
    "simbolo": string;
}
