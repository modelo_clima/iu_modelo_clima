export interface IModificarmtprin {
    "id": number;
    "codigo": string;
    "fecha_creacion": Date;
    "fecha_cierre": Date;
    "fecha_inicio_simulacion": string;
    "fecha_fin_simulacion": string;
    "descripcion": string;
    "id_tipo_mesa_tecnica": number;
    "id_mesa_tecnica_principal": number;
}

