export interface IEstacion {
    "id": number;
    "codigo":string;
    "nombre":string;
    "latitud": string;
    "longitud": string;
    "altitud": string;
    "estado": number;
    "fecha_instalacion": string;
    "fecha_suspension": string;
    "fecha_inicial_datos": Date;
    "fecha_final_datos": Date;
    "id_entidad": number;
    "id_categoria_estaciones": number;
    "id_tecnologia": number;
    "id_municipio": number;
}
