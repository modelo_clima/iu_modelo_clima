export interface IPuntoGestion {
    "id": number;
    "nombre": string;
    "latitud": string;
    "longitud": string;
    "altitud": string;
    "descripcion": string;
}
