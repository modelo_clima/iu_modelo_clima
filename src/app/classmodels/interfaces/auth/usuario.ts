export interface IUsuario {
    id?:number,
    first_name?:string,
    last_name?: string,
    username?: string,
    email?: string,
    password?: string,
    estado?:boolean,
    id_rol?:string,
    id_entidad?:string
}
