export interface IReglasoperacion {
    "valor": number,
	"fecha": Date,
	"id_escenario": number,
	"id_variable_regla_operacion": number
}
