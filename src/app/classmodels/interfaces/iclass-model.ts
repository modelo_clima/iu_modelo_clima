import { ApiService } from '../../services/api/api.service';

export interface IClassModel {
    refresh(id: any);
    setApiService(apiService: ApiService)
    getItems();
    getItem(id: number);
    getItemDetail(id: number);
    addItem(item: any, metodo?: any);
    deleteItem(id: number, d: any);
    updateItem(id: number, item: any);
    
}

