export interface IPermisoApp {
    "id"?: number;
    "consultar"?: string;
    "crear"?: string;
    "modificar"?: string;
    "eliminar"?: string;
    "id_rol"?: string;
    "id_seccion_app"?: string;
}
