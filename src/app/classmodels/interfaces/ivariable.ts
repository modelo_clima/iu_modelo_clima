export interface IVariable {
    "id": number;
    "nombre": string;
    "constante":number;
    "calculada": number;
    "valor_defecto": string;
    "id_unidad_medida": number;
    "id_tipo_variable": number;
    
}
