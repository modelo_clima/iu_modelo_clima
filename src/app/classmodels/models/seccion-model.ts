import { isNull } from "util";

import { IClassModel } from "../interfaces/iclass-model";
import { ISeccion } from "../interfaces/iseccion";

import { MessageBoxService } from 'src/app/services/message_box_app/message-box.service';
import { labels_app } from 'src/app/config_app/labels-app';

import { ApiService } from "../../services/api/api.service";

export class SeccionModel implements IClassModel {

    private apiService: ApiService = null;
    private messageBoxService: MessageBoxService = null;
    private urlApi: string = 'seccion';
    items: ISeccion[] = [];
    item: ISeccion;

    constructor() {
    }

    setApiService(apiService: ApiService) {
        this.apiService = apiService;
    }

    setMessageBoxService(messageBoxService: MessageBoxService) {
        this.messageBoxService = messageBoxService;
    }

    refresh() {
        this.apiService.loadingShow = true;
        this.apiService.list(this.urlApi).subscribe(data => {
            this.items = data;
            this.apiService.loadingShow = false;
        }, error => {
            this.apiService.loadingShow = false;
            console.error(error);
            if (this.messageBoxService != null) {
                this.messageBoxService.showMessageError(labels_app.messages.errorA)
            }
        });
    }

    _refresh(metodo?: () => void) {
        this.apiService.loadingShow = true;
        this.apiService.list(this.urlApi).subscribe(data => {
            this.items = data;
            if (!isNull(metodo)) {
                metodo();
            }
            this.apiService.loadingShow = false;
        }, error => {
            this.apiService.loadingShow = false;
            console.error(error);
            if (this.messageBoxService != null) {
                this.messageBoxService.showMessageError(labels_app.messages.errorA)
            }
        });
    }

    getItems() {
        return this.items;
    }

    getItem() {
        return this.item;
    }

    setItem(item) {
        this.item = item;
    }

    getItemDetail(id: number, metodo?: () => void) {
        this.apiService.loadingShow = true;
        this.apiService.detail(this.urlApi, id).subscribe(data => {
            this.item = data;
            this.apiService.loadingShow = false;
            if (!isNull(metodo)) {
                metodo();
            }
        }, error => {
            this.apiService.loadingShow = false;
            console.error(error);
            if (this.messageBoxService != null) {
                this.messageBoxService.showMessageError(labels_app.messages.errorA)
            }
        });
    }

    addItem(item: ISeccion, metodo?: () => void) {
        this.apiService.loadingShow = true;
        this.apiService.create(this.urlApi, item).subscribe(data => {
            this.item = data;
            this.apiService.loadingShow = false;
            if (this.messageBoxService != null) {
                this.messageBoxService.showMessage(labels_app.messages.datos_gurdados, () => {
                    this.messageBoxService.closeMessage();
                    if (!isNull(metodo)) {
                        metodo();
                    }
                });
            } else {
                if (!isNull(metodo)) {
                    metodo();
                }
            }
        }, error => {
            this.apiService.loadingShow = false;
            console.error(error);
            if (this.messageBoxService != null) {
                this.messageBoxService.showMessageError(labels_app.messages.errorA)
            }
        });

    }

    updateItem(id: number, item: ISeccion, metodo?: () => void) {
        this.apiService.loadingShow = true;
        this.apiService.update(this.urlApi, id, item).subscribe(data => {
            this.item = data;
            this.apiService.loadingShow = false;
            if (this.messageBoxService != null) {
                this.messageBoxService.showMessage(labels_app.messages.datos_modificados, () => {
                    this.messageBoxService.closeMessage();
                    if (!isNull(metodo)) {
                        metodo();
                    }
                });
            } else {
                if (!isNull(metodo)) {
                    metodo();
                }
            }
        }, error => {
            this.apiService.loadingShow = false;
            console.error(error);
            if (this.messageBoxService != null) {
                this.messageBoxService.showMessageError(labels_app.messages.errorA)
            }
        });
    }

    deleteItem(id: number) {
        this.apiService.loadingShow = true;
        this.apiService.delete(this.urlApi, id).subscribe(data => {
            this.item = null;
            this.apiService.loadingShow = false;
            if (this.messageBoxService != null) {
                this.messageBoxService.showMessage(labels_app.messages.datos_eliminados, () => {
                    this.messageBoxService.closeMessage();
                    this.refresh();
                });
            }
        }, error => {
            this.apiService.loadingShow = false;
            console.error(error);
            if (this.messageBoxService != null) {
                this.messageBoxService.showMessageError(labels_app.messages.errorA)
            }
        });
    }


}
