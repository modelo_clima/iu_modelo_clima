import { ApiService } from '../../services/api/api.service';
import { IClassModel } from '../interfaces/iclass-model';
import { ITablaBasica } from '../interfaces/itabla-basica';
import { IEscenario } from '../interfaces/iescenario';
import { INewEscenario } from '../interfaces/inewescenario';
import { isNull } from 'util';


export class EscenarioModel implements IClassModel {

    private apiService: ApiService;
    private urlApi: string = 'escenario';
    private urlParams: string = '?id_mesa_tecnica=';
    items: IEscenario[] = [];
    item: IEscenario;
    newItem: INewEscenario;
    id_mesa_tecnica: number;

    constructor() {
    }

    setApiService(apiService: ApiService) {
        this.apiService = apiService;
    }

    refresh(id_mesa_tecnica: number) {
        if (id_mesa_tecnica > 0){
            this.id_mesa_tecnica = id_mesa_tecnica;         
        }
        this.apiService.lista(this.urlApi, (this.urlParams+this.id_mesa_tecnica)).subscribe(data => {
            this.items = data;
        });
    }

   
    getItems() {
       return this.items;
    }

    setItems(items) {
        this.items = items;
     }

    
    getItem() {
        return this.item;
    }

    setItem(item) {
        this.item = item;
    }

    getItemDetail(id: number, metodo?: () => void) {
        this.apiService.detail(this.urlApi, id).subscribe(data => {
            this.item = data;
            if (!isNull(metodo)) {
                metodo();
            }
        });
    }

    addItem(newItem: INewEscenario, metodo?: (newData?:any) => void) {
        this.apiService.create(this.urlApi, newItem).subscribe(data => {
            this.item = data;
            if (!isNull(metodo)) {
                metodo(data);
            }
        });

    }

    updateItem(id: number, item: IEscenario, metodo?: () => void) {
        this.apiService.update(this.urlApi, id, item).subscribe(data => {
            this.item = data;
            if (!isNull(metodo)) {
                metodo();
            }
        });
    }

    deleteItem(id: number, d:number) {
        this.apiService.delete(this.urlApi, id).subscribe(data => {
            this.item = null;
            this.refresh(0);
        });
    }


}
