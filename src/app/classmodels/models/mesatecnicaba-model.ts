import { ApiService } from '../../services/api/api.service';
import { IClassModel } from '../interfaces/iclass-model';
import { IMesatecnicaba } from '../interfaces/imesatecnicaba';
import { IModificarmtba } from '../interfaces/imodificarmtba';
import { isNull } from 'util';
import { IEscenario } from '../interfaces/iescenario';


export class MesatecnicabaModel implements IClassModel {

    private apiService: ApiService;
    private urlApi: string = 'mesatecnica';
    private paramsApi: string = '?id_tipo_mesa_tecnica=2&asociar=';
    private paramsApi2: string = '?asociar=';
    items: IMesatecnicaba[] = [];
    item: IMesatecnicaba;
    escenarios: IEscenario[] = [];
    errorMessage: string;

    constructor() {
    }

    setApiService(apiService: ApiService) {
        this.apiService = apiService;
    }

    refresh(param: string, metodo?: () => void) {
        this.apiService.lista(this.urlApi,(this.paramsApi+param)).subscribe(data => {
            this.items = data;
            if (!isNull(metodo)) {
                metodo();
            }
        }, error => {
            this.errorMessage = this.items['detail'];     
          });
    }

    getItems() {
        return this.items;
    }

    getItem() {
        return this.item;
    }

    setItem(item) {
        this.item = item;
    }

    getItemDetail(id: number, metodo?: () => void) {
        this.apiService.detail(this.urlApi, id).subscribe(data => {
            this.item = data.info;
            this.escenarios = data.escenarios
            if (!isNull(metodo)) {
                metodo();
            }
        });
    }

    getEscenarios(){
        return this.escenarios;
    }

    addItem(item: IMesatecnicaba, metodo?: () => void) {
        this.apiService.create(this.urlApi, item).subscribe(data => {
            this.item = data;
            if (!isNull(metodo)) {
                metodo();
            }
        });

    }

    updateItem(id: number, itemMod: IModificarmtba, metodo?: () => void) {
        this.apiService.update(this.urlApi,  id, itemMod).subscribe(data => {
            this.item = data;
            if (!isNull(metodo)) {
                metodo();
            }
        });
    }

    deleteItem(id: number) {
        this.apiService.delete(this.urlApi, id).subscribe(data => {
            this.item = null;
            this.refresh("");
        });
    }


}
