import { ApiService } from '../../services/api/api.service';
import { IClassModel } from '../interfaces/iclass-model';
import { ITablaBasica } from '../interfaces/itabla-basica';
import { IReglasoperacion } from '../interfaces/ireglasoperacion';
import { isNull } from 'util';


export class ReglasoperacionModel implements IClassModel {

    private apiService: ApiService;
    private urlApi: string = 'Reglas Operacion';
    items: IReglasoperacion[] = [];
    item: IReglasoperacion;

    constructor() {
    }

    setApiService(apiService: ApiService) {
        this.apiService = apiService;
    }

    refresh() {
        this.apiService.list(this.urlApi).subscribe(data => {
            this.items = data;
        });
    }

    getItems() {
        return this.items;
    }

    getItem() {
        return this.item;
    }

    setItem(item) {
        this.item = item;
    }

    getItemDetail(id: number, metodo?: () => void) {
        this.apiService.detail(this.urlApi, id).subscribe(data => {
            this.item = data;
            if (!isNull(metodo)) {
                metodo();
            }
        });
    }

    addItem(item: IReglasoperacion, metodo?: () => void) {
        this.apiService.create(this.urlApi, item).subscribe(data => {
            this.item = data;
            if (!isNull(metodo)) {
                metodo();
            }
        });

    }

    updateItem(id: number, item: IReglasoperacion, metodo?: () => void) {
        this.apiService.update(this.urlApi, id, item).subscribe(data => {
            this.item = data;
            if (!isNull(metodo)) {
                metodo();
            }
        });
    }

    deleteItem(id: number) {
        this.apiService.delete(this.urlApi, id).subscribe(data => {
            this.item = null;
            this.refresh();
        });
    }


}
