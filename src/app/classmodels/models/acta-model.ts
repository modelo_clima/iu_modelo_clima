import { ApiService } from '../../services/api/api.service';
import { IClassModel } from '../interfaces/iclass-model';
import { ITablaBasica } from '../interfaces/itabla-basica';
import { IActa } from '../interfaces/iacta';
import { isNull } from 'util';


export class ActaModel implements IClassModel {

    private apiService: ApiService;
    private urlApi: string = 'actasmesatecnica';
    private urlParams: string = '?id_mesa_tecnica=';
    items: IActa[] = [];
    item: IActa;    
    id_mesa_tecnica: number;

    constructor() {
    }

    setApiService(apiService: ApiService) {
        this.apiService = apiService;
    }

    refresh(id_mesa_tecnica: number) {
        if (id_mesa_tecnica > 0){
            this.id_mesa_tecnica = id_mesa_tecnica;
            this.urlParams += id_mesa_tecnica;            
        }
        this.apiService.lista(this.urlApi, this.urlParams).subscribe(data => {
            this.items = data;
        });
    }

   
    getItems() {
       return this.items;
    }

    
    getItem() {
        return this.item;
    }

    setItem(item) {
        this.item = item;
    }

    getItemDetail(id: number, metodo?: () => void) {
        this.apiService.detail(this.urlApi, id).subscribe(data => {
            this.item = data;
            if (!isNull(metodo)) {
                metodo();
            }
        });
    }

    //addItem(newItem: INewEscenario, metodo?: (newData?:any) => void) {
      //  this.apiService.create(this.urlApi, newItem).subscribe(data => {
        //    this.item = data;
          //  if (!isNull(metodo)) {
            //    metodo(data);
           // }
       // });

    //}

    addItem(item: IActa, metodo?: () => void) {
        this.apiService.create(this.urlApi, item).subscribe(data => {
            this.item = data;
            if (!isNull(metodo)) {
                metodo();
            }
        });
    }
    
    updateItem(id: number, item: IActa, metodo?: () => void) {
        this.apiService.update(this.urlApi, id, item).subscribe(data => {
            this.item = data;
            if (!isNull(metodo)) {
                metodo();
            }
        });
    }

    deleteItem(id: number, d:number) {
        this.apiService.delete(this.urlApi, id).subscribe(data => {
            this.item = null;
            this.refresh(0);
        });
    }


}
