import { ApiService } from "../../services/api/api.service";
import { IClassModel } from "../interfaces/iclass-model";
import { ITablaBasica } from "../interfaces/itabla-basica";
import { IAuth } from "../interfaces/iauth";
import { isNull } from "util";

export class AuthModel implements IClassModel {
  private apiService: ApiService;
  private urlApi: string = "authusuario";
  items: IAuth[] = [];
  item: IAuth;

  constructor() {}

  setApiService(apiService: ApiService) {
    this.apiService = apiService;
  }

  refresh() {
    this.apiService.list(this.urlApi).subscribe(data => {
      this.items = data;
    });
  }

  getItems() {
    return this.items;
  }

  getItem() {
    return this.item;
  }

  setItem(item) {
    this.item = item;
  }

  getItemDetail(id: number, metodo?: () => void) {
    this.apiService.detail(this.urlApi, id).subscribe(data => {
      this.item = data;
      if (!isNull(metodo)) {
        metodo();
      }
    });
  }

  addItem(item: IAuth, metodo?: () => void) {
    this.apiService.create(this.urlApi, item).subscribe(data => {
      this.item = data;
      if (!isNull(metodo)) {
        metodo();
      }
    });
  }

  updateItem(id: number, item: IAuth, metodo?: () => void) {
    this.apiService.update(this.urlApi, id, item).subscribe(data => {
      this.item = data;
      if (!isNull(metodo)) {
        metodo();
      }
    });
  }

  deleteItem(id: number) {
    this.apiService.delete(this.urlApi, id).subscribe(data => {
      this.item = null;
      this.refresh();
    });
  }
}
