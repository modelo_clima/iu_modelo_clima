import { ApiService } from "../../services/api/api.service";
import { IClassModel } from "../interfaces/iclass-model";
import { ITablaBasica } from "../interfaces/itabla-basica";
import { IPermisoApp } from "../interfaces/ipermiso-app";
import { isNull } from "util";
import { UsuarioModel } from "./usuario-model";
import { Router, Route } from "@angular/router";
import { OnInit } from "@angular/core";
import { ICategoriaEstacion } from "../interfaces/icategoria-estacion";

export class PermisoAppModel implements IClassModel, OnInit {
  private apiService: ApiService;
  private urlApi: string = "permisoapp";
  items: IPermisoApp[] = [];
  item: IPermisoApp;

  private usuarioModel: UsuarioModel;
  private router: Router;

  ngOnInit() {}
  constructor() {
    this.usuarioModel = new UsuarioModel();
  }

  setRouter(router: Router) {
    this.router = router;
  }

  setApiService(apiService: ApiService) {
    this.apiService = apiService;
    this.usuarioModel.setApiService(this.apiService);
  }

  refresh() {
    this.apiService.list(this.urlApi).subscribe((data) => {
      this.items = data;
    });
  }
  _refresh(metodo?: () => void) {
    this.apiService.list(this.urlApi).subscribe((data) => {
      this.items = data;
      if (!isNull(metodo)) {
        metodo();
      }
    });
  }
 
  getItems() {
    return this.items;
  }

  getItem() {
    return this.item;
  }

  setItem(item) {
    this.item = item;
  }

  getItemDetail(id: number, metodo?: () => void) {
    this.apiService.detail(this.urlApi, id).subscribe((data) => {
      this.item = data;
      if (!isNull(metodo)) {
        metodo();
      }
    });
  }

  addItem(item: IPermisoApp, metodo?: () => void) {
    this.apiService.create(this.urlApi, item).subscribe((data) => {
      this.item = data;
      if (!isNull(metodo)) {
        metodo();
      }
    });
  }

  updateItem(id: number, item: IPermisoApp, metodo?: () => void) {
    this.apiService.update(this.urlApi, id, item).subscribe((data) => {
      this.item = data;
      if (!isNull(metodo)) {
        metodo();
      }
    });
  }

  deleteItem(id: number) {
    this.apiService.delete(this.urlApi, id).subscribe((data) => {
      this.item = null;
      this.refresh();
    });
  }
}
