import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { Route, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { NgxSpinnerModule } from "ngx-spinner";
import { ConsultarGuard } from './guards/consultar.guard';
import { ApiService } from './services/api/api.service';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { components_app } from './config_app/list-components-app';
import { PermisosAppService } from './services/permisos_app/permisos-app.service';
import { MessageBoxService } from './services/message_box_app/message-box.service';

const routes: Route[] = [
  { path: 'welcome', component: WelcomeComponent, canActivate: [ConsultarGuard] },
];

@NgModule({
  declarations: components_app.declarations,
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(components_app.routes),
    FormsModule,
    ReactiveFormsModule,
    NgxSpinnerModule,
  ],
  providers: [ApiService, PermisosAppService, MessageBoxService],
  bootstrap: [AppComponent],
  exports: [RouterModule]
})
export class AppModule { }
