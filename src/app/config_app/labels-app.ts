export const labels_app: any = {
    buttons: {
        crear: 'Crear',
        editar: 'Editar',
        eliminar: 'Eliminar',
        cancelar: 'Cancelar',
        guardar: 'Guardar',
        si: 'SI',
        no: 'NO'
    },
    labels: {
        nombre: 'Nombre',
        codigo: 'Código',
        nombres:'Nombres',
        apellidos:'Apellidos',
        email:'Correo',
        rol:'Rol',
        entidad:'Entidad',
        longitud:'longitud',
        latitud:'latitud',
        altitud:'altitud',
        estado:'estado',
        fecha_instalacion:'Fecha Instalación',
        fecha_suspension:'Fecha Suspensión',
        fecha_final_datos:'Fecha Final Datos',
        fecha_inicial_datos:'Fecha Inicial Datos',
        categoria_estacion:'Categoría Estación',
        tecnologia_estacion:'Tecnología Estación',
        password:'Contraseña',
        repetir_password:'Confirmar Contraseña',
        municipio:'Municipio',
        variable:'Variable',
        tipo_uso:'Tipo Uso',
        tipos_variable:'Tipo Variable',
        constante:'Constante',
        calculada:'Calculada',
        valor_defecto:'Valor Defecto',
        unidad_medida:'Unidad Medida',
        descripcion: 'Descripción',
        punto_gestion: 'Punto Gestión',
        simbolo: 'Simbolo',
        username: 'Username',
 

    },
    messages: {
        titles: {
            mensaje: 'Mensaje',
            error: 'Error',
            msg_cofirmacion: 'Mensaje de confirmación'
        },
        datos_gurdados: 'Datos guardados.',
        datos_modificados: 'Registro actualizado.',
        datos_eliminados: 'Registro eliminado.',
        msg_cofirm_eliminacion: 'El registro se eliminará permanentemente. ¿Desea continuar?',
        msg_rol_relacionado: 'El rol que intenta eliminar esta relacionado con uno o mas usuarios',
        errorA: 'No se puede realizar la operación.',
        errorB: 'Se ha presentado un error con este enlace.',
        errorC: 'No tiene permisos para realizar esta operación.',
        errorD: 'Se ha cerrado la sesión.',
    },
    input_error:{
        requerido: 'El campo es obligatorio',
        min_length_a: 'El campo debe tener un mínimo de 3 caracteres',
        min_length_b: 'El campo debe tener un mínimo de 8 caracteres',
        option: 'Debe seleccionar una opción',
        isnotsame:'la contraseña no coincide',
        existe_username: 'Ya Existe un usuario con este Username',
        existe_email: 'Ya Existe un usuario con este correo',
        email_valido: 'Digite un Correo valido'
    },
    roles: {
        title_index: 'Roles',
        title_crear: 'Registrar Rol',
        title_editar: 'Modificar Rol',
    },
    usuarios: {
        title_index: 'Usuarios',
        title_crear: 'Registrar Usuario',
        title_editar: 'Modificar Usuario',
    },
    entidades: {
        title_index: 'Entidades',
        title_crear: 'Registrar entidad',
        title_editar: 'Modificar entidad',
    },
    categorias_estaciones: {
        title_index:'Categoría Estaciones',
        title_crear:'Registrar Categoría Estación',
        title_editar:'Modificar Categoría Estación',
    },
    categorias_series: {
        title_index:'Categorías Series',
        title_crear:'Registrar Categoría Serie',
        title_editar:'Modificar Categoría Serie',
    },
    estaciones: {
        title_index:'Estaciones',
        title_crear:'Registrar Estación',
        title_editar:'Modificar Estación',
    },
    estados: {
        title_index:'Estados',
        title_crear:'Registrar Estado',
        title_editar:'Modificar Estado',
    },
    limites: {
        title_index:'Limites',
        title_crear:'Registrar Limite',
        title_editar:'Modificar Limite',
    },
    microcuencas: {
        title_index:'Microcuencas',
        title_crear:'Registrar Microcuenca',
        title_editar:'Modificar Microcuenca',
    },
    modelos: {
        title_index:'Modelos',
        title_crear:'Registrar Modelo',
        title_editar:'Modificar Modelo',
    },
    puntos_gestion: {
        title_index:'Puntos Gestión',
        title_crear:'Registrar Punto Gestión',
        title_editar:'Modificar Punto Gestión',
    },
    secciones: {
        title_index:'Secciones',
        title_crear:'Registrar Sección ',
        title_editar:'Modificar Sección',
    },
    tecnologias_estaciones: {
        title_index:'Tecnologías Estaciones',
        title_crear:'Registrar Tecnología Estación',
        title_editar:'Modificar Tecnología Estación',
    },

    tipos_caudales: {
        title_index:'Tipos Caudales',
        title_crear:'Registrar Tipo Caudal',
        title_editar:'Modificar Tipo Caudal',
    },
    tipos_usos: {
        title_index:'Tipos Usos',
        title_crear:'Registrar Tipo Uso',
        title_editar:'Modificar Tipo Uso',
    },
    tipos_variables: {
        title_index:'Tipos Variables',
        title_crear:'Registrar Tipo Variable',
        title_editar:'Modificar Tipo Variable',
    },
    unidades_medidas: {
        title_index:'Unidades Medidas',
        title_crear:'Registrar Unidad Medida',
        title_editar:'Modificar Unidad Medida',
    },
    variables: {
        title_index:'Variables',
        title_crear:'Registrar Variable',
        title_editar:'Modificar Variable',
    },
    variables_reglas_operacion: {
        title_index:'Variables Reglas Operación',
        title_crear:'Registrar Variable Regla Operación',
        title_editar:'Modificar  Variable Regla Operación',
    },

}