
import { Route } from '@angular/router';

import { TiposVariablesComponent } from '../components/configuracion_basica/tiposvariables/tiposvariables.component';
import { FormComponent as TipoVaribaleFormComponent } from '../components/configuracion_basica/tiposvariables/form/form.component';

import { UnidadesMedidasComponent } from '../components/configuracion_basica/unidadmedida/unidadmedida.component';
import { FormComponent as UnidadMedidaFormComponent } from '../components/configuracion_basica/unidadmedida/form/form.component';

import { EstacionesComponent } from '../components/configuracion_basica/estaciones/estaciones.component';
import { FormComponent as EstacionFormComponent } from '../components/configuracion_basica/estaciones/form/form.component';

import { MunicipiosComponent } from '../components/configuracion_basica/municipios/municipios.component';

import { SeccionesComponent } from '../components/configuracion_basica/secciones/secciones.component';
import { FormComponent as SeccionFormComponent } from '../components/configuracion_basica/secciones/form/form.component';

import { LimitesComponent } from '../components/configuracion_basica/limites/limites.component';
import { FormComponent as LimiteFormComponent } from '../components/configuracion_basica/limites/form/form.component';

import { VariablesComponent } from '../components/configuracion_basica/variables/variables.component';
import { FormComponent as VariableFormComponent } from '../components/configuracion_basica/variables/form/form.component';

import { ModelosComponent } from '../components/configuracion_basica/modelos/modelos.component';
import { FormComponent as ModeloFormComponent } from '../components/configuracion_basica/modelos/form/form.component';

import { CategoriasSeriesComponent } from '../components/configuracion_basica/categoriaseries/categoriasseries.component';
import { FormComponent as CategoriaSerieFormComponent } from '../components/configuracion_basica/categoriaseries/form/form.component';

import { EstadosComponent } from '../components/configuracion_basica/estados/estados.component';
import { FormComponent as EstadoFormComponent } from '../components/configuracion_basica/estados/form/form.component';

import { TiposCaudalComponent } from '../components/configuracion_basica/tiposcaudal/tiposcaudal.component';
import { FormComponent as TipoCaudalFormComponent } from '../components/configuracion_basica/tiposcaudal/form/form.component';

import { EntidadesComponent } from '../components/configuracion_basica/entidades/entidades.component';
import { FormComponent as EntidadFormComponent } from '../components/configuracion_basica/entidades/form/form.component';

import { TecnologiasEstacionesComponent } from '../components/configuracion_basica/tecnologiaestaciones/tecnologiaestaciones.component';
import { FormComponent as TecnologiaEstacionFormComponent } from '../components/configuracion_basica/tecnologiaestaciones/form/form.component';

import { RolesComponent } from '../components/administracion/roles/roles.component';
import { FormComponent as RolFormComponent } from '../components/administracion/roles/form/form.component';

import { CategoriaEstacionesComponent } from '../components/configuracion_basica/categoriaestaciones/categoriaestaciones.component';
import { FormComponent as CategoriaEstacionFormComponent } from '../components/configuracion_basica/categoriaestaciones/form/form.component';

import { TiposUsosComponent } from '../components/configuracion_basica/tiposusos/tiposusos.component';
import { FormComponent as TipoUsoFormComponent } from '../components/configuracion_basica/tiposusos/form/form.component';

import { PuntosGestionComponent } from '../components/configuracion_basica/puntosgestion/puntosgestion.component';
import { FormComponent as PuntoGestionFormComponent } from '../components/configuracion_basica/puntosgestion/form/form.component';

import { MicrocuencasComponent } from '../components/configuracion_basica/microcuencas/microcuencas.component';
import { FormComponent as MicrocuencaFormComponent } from '../components/configuracion_basica/microcuencas/form/form.component';

import { MesatecnicabaComponent } from '../components/mesas_tecnicas/mesatecnicaba/mesatecnicaba.component';
import { FormComponent as MesatecnicabaFormComponent } from '../components/mesas_tecnicas/mesatecnicaba/form/form.component';
import { MesatecnicaprinComponent } from '../components/mesas_tecnicas/mesatecnicaprin/mesatecnicaprin.component';

import { FormComponent as ModificarmtbaFormComponent } from '../components/mesas_tecnicas/modificarmtba/form/form.component';



import { FormComponent as EscenarioFormComponent } from '../components/mesas_tecnicas/escenarios/form/form.component';

import { ActasComponent } from '../components/mesas_tecnicas/actas/actas.component';
import { FormComponent as ActaFormComponent } from '../components/mesas_tecnicas/actas/form/form.component';




import { ModeloClimaComponent } from '../components/datos_gestion/modelo-clima/modelo-clima.component';
import { DatosValidadosComponent } from '../components/datos_gestion/datos-validados/datos-validados.component';
import { UsuariosComponent } from '../components/administracion/usuarios/usuarios.component';

import { LoginComponent } from '../components/administracion/usuarios/login/login.component';
import { FormComponent as UsuarioFormComponent } from '../components/administracion/usuarios/form/form.component';

import { SinPermisosComponent } from '../components/administracion/sinpermisos/sinpermisos.component';

import { ConsultarGuard } from '../guards/consultar.guard';
import { CrearGuard } from '../guards/crear.guard';
import { ModificarGuard } from '../guards/modificar.guard';
import { WelcomeComponent } from '../components/welcome/welcome.component';
import { AppComponent } from '../app.component';
import { ModificarmtbaComponent } from '../components/mesas_tecnicas/modificarmtba/modificarmtba.component';

import { EscenariosComponent } from '../components/mesas_tecnicas/escenarios/escenarios.component';
import { HeaderSiseComponent } from '../components/template/header-sise/header-sise.component';
import { FooterSiseComponent } from '../components/template/footer-sise/footer-sise.component';
import { MenuSiseComponent } from '../components/template/menu-sise/menu-sise.component';
import { LoadingSiseComponent } from '../components/template/loading-sise/loading-sise.component';
import { AlertasSiseComponent } from '../components/template/alertas-sise/alertas-sise.component';
import { urls_app } from './urls-app';


const routes_app: Route[] = [
    { path: '', component: LoginComponent },
    { path: 'auth/login', component: LoginComponent },
    { path: 'sinpermisos', component: SinPermisosComponent, },
    { path: 'inicio', component: WelcomeComponent, canActivate: [ConsultarGuard] },

    //administración Básica

    { path: urls_app.roles.index, component: RolesComponent, canActivate: [ConsultarGuard] },
    { path: urls_app.roles.modificar + '/:id', component: RolFormComponent, canActivate: [ModificarGuard] },
    { path: urls_app.roles.crear, component: RolFormComponent, canActivate: [CrearGuard] },

    { path: urls_app.usuarios.index, component: UsuariosComponent, canActivate: [ConsultarGuard] },
    { path: urls_app.usuarios.modificar  + '/:id', component: UsuarioFormComponent, canActivate: [ModificarGuard] },
    { path: urls_app.usuarios.crear, component: UsuarioFormComponent, canActivate: [CrearGuard] },

    // Configuración Básica

    { path: urls_app.entidades.index, component: EntidadesComponent, canActivate: [ConsultarGuard], },
    { path: urls_app.entidades.modificar + '/:id', component: EntidadFormComponent, canActivate: [ModificarGuard] },
    { path: urls_app.entidades.crear, component: EntidadFormComponent, canActivate: [CrearGuard] },

    { path: urls_app.categorias_estaciones.index , component: CategoriaEstacionesComponent, canActivate: [ConsultarGuard] },
    { path: urls_app.categorias_estaciones.modificar +'/:id', component: CategoriaEstacionFormComponent, canActivate: [ModificarGuard] },
    { path: urls_app.categorias_estaciones.crear, component: CategoriaEstacionFormComponent, canActivate: [CrearGuard] },

    { path: urls_app.categorias_series.index, component: CategoriasSeriesComponent, canActivate: [ConsultarGuard] },
    { path: urls_app.categorias_series.modificar  + '/:id', component: CategoriaSerieFormComponent, canActivate: [ModificarGuard] },
    { path: urls_app.categorias_series.crear, component: CategoriaSerieFormComponent, canActivate: [CrearGuard] },

    { path: urls_app.estaciones.index, component: EstacionesComponent, canActivate: [ConsultarGuard] },
    { path: urls_app.estaciones.modificar + '/:id', component: EstacionFormComponent, canActivate: [ModificarGuard] },
    { path: urls_app.estaciones.crear, component: EstacionFormComponent, canActivate: [CrearGuard] },

    { path: urls_app.estados.index, component: EstadosComponent, canActivate: [ConsultarGuard] },
    { path: urls_app.estados.modificar + '/:id', component: EstadoFormComponent, canActivate: [ModificarGuard] },
    { path: urls_app.estados.crear, component: EstadoFormComponent, canActivate: [CrearGuard] },

    { path: urls_app.limites.index, component: LimitesComponent, canActivate: [ConsultarGuard] },
    { path: urls_app.limites.modificar + '/:id', component: LimiteFormComponent, canActivate: [ModificarGuard] },
    { path: urls_app.limites.crear, component: LimiteFormComponent, canActivate: [CrearGuard] },

    { path: urls_app.microcuencas.index, component: MicrocuencasComponent, canActivate: [ConsultarGuard] },
    { path: urls_app.microcuencas.modificar + '/:id', component: MicrocuencaFormComponent, canActivate: [ModificarGuard] },
    { path: urls_app.microcuencas.crear, component: MicrocuencaFormComponent, canActivate: [CrearGuard] },

    { path: urls_app.modelos.index, component: ModelosComponent, canActivate: [ConsultarGuard] },
    { path: urls_app.modelos.modificar + '/:id', component: ModeloFormComponent, canActivate: [ModificarGuard] },
    { path: urls_app.modelos.crear, component: ModeloFormComponent, canActivate: [CrearGuard] },
    
    { path: urls_app.puntos_gestion.index, component: PuntosGestionComponent, canActivate: [ConsultarGuard] },
    { path: urls_app.puntos_gestion.modificar + '/:id', component: PuntoGestionFormComponent, canActivate: [ModificarGuard] },
    { path: urls_app.puntos_gestion.crear, component: PuntoGestionFormComponent, canActivate: [CrearGuard] },
    
    { path: urls_app.secciones.index, component: SeccionesComponent, canActivate: [ConsultarGuard] },
    { path: urls_app.secciones.modificar + '/:id', component: SeccionFormComponent, canActivate: [ModificarGuard] },
    { path: urls_app.secciones.crear, component: SeccionFormComponent, canActivate: [CrearGuard] },

    { path: urls_app.tecnologias_estaciones.index, component: TecnologiasEstacionesComponent, canActivate: [ConsultarGuard] },
    { path: urls_app.tecnologias_estaciones.modificar + '/:id', component: TecnologiaEstacionFormComponent, canActivate: [ModificarGuard] },
    { path: urls_app.tecnologias_estaciones.crear, component: TecnologiaEstacionFormComponent, canActivate: [CrearGuard] },

    { path: urls_app.tipos_caudales.index, component: TiposCaudalComponent, canActivate: [ConsultarGuard] },
    { path: urls_app.tipos_caudales.modificar + '/:id', component: TipoCaudalFormComponent, canActivate: [ModificarGuard] },
    { path: urls_app.tipos_caudales.crear, component: TipoCaudalFormComponent, canActivate: [CrearGuard] },

    { path: urls_app.tipos_usos.index, component: TiposUsosComponent, canActivate: [ConsultarGuard] },
    { path: urls_app.tipos_usos.modificar + '/:id', component: TipoUsoFormComponent, canActivate: [ModificarGuard] },
    { path: urls_app.tipos_usos.crear, component: TipoUsoFormComponent, canActivate: [CrearGuard] },

    { path: urls_app.tipos_variables.index, component: TiposVariablesComponent, canActivate: [ConsultarGuard] },
    { path: urls_app.tipos_variables.modificar + '/:id', component: TipoVaribaleFormComponent, canActivate: [ModificarGuard] },
    { path: urls_app.tipos_variables.crear, component: TipoVaribaleFormComponent, canActivate: [CrearGuard] },
    
    { path: urls_app.unidades_medidas.index, component: UnidadesMedidasComponent, canActivate: [ConsultarGuard] },
    { path: urls_app.unidades_medidas.modificar + '/:id', component: UnidadMedidaFormComponent, canActivate: [ModificarGuard] },
    { path: urls_app.unidades_medidas.crear, component: UnidadMedidaFormComponent, canActivate: [CrearGuard] },

    { path: urls_app.variables.index, component: VariablesComponent, canActivate: [ConsultarGuard] },
    { path: urls_app.variables.modificar + '/:id', component: VariableFormComponent, canActivate: [ModificarGuard] },
    { path: urls_app.variables.crear, component: VariableFormComponent, canActivate: [CrearGuard] },

    
    
    { path: urls_app.Gestion_MTBA.index, component: MesatecnicabaComponent },
    { path: urls_app.Gestion_MTBA.modificar + '/:id', component: ModificarmtbaFormComponent },
    { path: urls_app.Gestion_MTBA.crear, component: MesatecnicabaFormComponent },
    
    { path: urls_app.MTP.index, component: MesatecnicaprinComponent },
    { path: urls_app.MTP.modificar+ '/:id', component: ModificarmtbaFormComponent },

    { path: urls_app.Escenarios.modificar + '/:id', component: EscenarioFormComponent },
    { path: urls_app.Escenarios.crear, component: EscenarioFormComponent },

    //{ path: 'reglasoperacion', component: ReglasoperacionComponent },
    //{ path: 'reglasoperacion/crear', component: ReglasoperacionFormComponent },

    { path: urls_app.Actas.index, component: ActasComponent },
    { path: urls_app.Actas.modificar + '/:id', component: ActaFormComponent },
    { path: urls_app.Actas.crear, component: ActaFormComponent },

    { path: urls_app.datos_validados.index, component: DatosValidadosComponent },
    { path: urls_app.modelo_clima.index, component: ModeloClimaComponent },
];


export const components_app: any = {
    routes: routes_app,
    declarations: [
        AppComponent,
        WelcomeComponent,
        TiposVariablesComponent,
        TipoVaribaleFormComponent,

        VariablesComponent,
        VariableFormComponent,
        EstacionesComponent,
        EstacionFormComponent,
        SeccionesComponent,
        SeccionFormComponent,
        LimitesComponent,
        LimiteFormComponent,
        ModelosComponent,
        ModeloFormComponent,
        MunicipiosComponent,

        UnidadesMedidasComponent,
        UnidadMedidaFormComponent,

        CategoriasSeriesComponent,
        CategoriaSerieFormComponent,

        EstadosComponent,
        EstadoFormComponent,
        
        TiposCaudalComponent,
        TipoCaudalFormComponent,

        EntidadesComponent,
        EntidadFormComponent,

        TecnologiasEstacionesComponent,
        TecnologiaEstacionFormComponent,

        RolesComponent,
        RolFormComponent,

        CategoriaEstacionesComponent,
        CategoriaEstacionFormComponent,

        TiposUsosComponent,
        TipoUsoFormComponent,

        PuntosGestionComponent,
        PuntoGestionFormComponent,

        MicrocuencasComponent,
        MicrocuencaFormComponent,

        VariablesComponent,
        VariableFormComponent,

        EstacionesComponent,
        EstacionFormComponent,

        SeccionesComponent,
        SeccionFormComponent,

        LimitesComponent,
        LimiteFormComponent,
        
        MesatecnicabaComponent,
        MesatecnicabaFormComponent,
        ModificarmtbaComponent,
        ModificarmtbaFormComponent,
        MesatecnicaprinComponent,
        ModelosComponent,
        ModeloFormComponent,
    
        EscenariosComponent,
        EscenarioFormComponent,
        ActasComponent,
        ActaFormComponent,
        ModeloClimaComponent,
        DatosValidadosComponent,
        HeaderSiseComponent,
        FooterSiseComponent,
        MenuSiseComponent,
        LoadingSiseComponent,
        AlertasSiseComponent,
        UsuariosComponent,
        LoginComponent,
        UsuarioFormComponent,
        SinPermisosComponent,
    ]
};

