export const urls_app: any = {
    //adminstracion de usuario
    roles: {
        index: 'roles',
        crear: 'rol/crear',
        modificar: 'rol/modificar',
    },
    usuarios: {
        index: 'usuarios',
        crear: 'usuario/crear',
        modificar: 'usuario/modificar',
    },

    //configuración basica
    entidades: {
        index: 'entidades',
        crear: 'entidad/crear',
        modificar: 'entidad/modificar',
    },
    categorias_estaciones: {
        index: 'categoriasestaciones',
        crear: 'categoriaestacion/crear',
        modificar: 'categoriaestacion/modificar',
    },
    categorias_series: {
        index: 'categoriasseries',
        crear: 'categoriaserie/crear',
        modificar: 'categoriaserie/modificar',
    },
    estaciones: {
        index: 'estaciones',
        crear: 'estacion/crear',
        modificar: 'estacion/modificar',
    },
    estados: {
        index: 'estados',
        crear: 'estado/crear',
        modificar: 'estado/modificar',
    },
    limites: {
        index: 'limites',
        crear: 'limite/crear',
        modificar: 'limite/modificar',
    },
    microcuencas: {
        index: 'microcuencas',
        crear: 'microcuenca/crear',
        modificar: 'microcuenca/modificar',
    },
    modelos: {
        index: 'modelos',
        crear: 'modelo/crear',
        modificar: 'modelo/modificar',
    },
    puntos_gestion: {
        index: 'puntosgestion',
        crear: 'puntogestion/crear',
        modificar: 'puntogestion/modificar',
    },
    secciones: {
        index: 'secciones',
        crear: 'seccion/crear',
        modificar: 'seccion/modificar',
    },
    tecnologias_estaciones: {
        index: 'tecnologiasestaciones',
        crear: 'tecnologiaestacion/crear',
        modificar: 'tecnologiaestacion/modificar',
    },

    tipos_caudales: {
        index: 'tiposcaudal',
        crear: 'tipocaudal/crear',
        modificar: 'tipocaudal/modificar',
    },
    tipos_usos: {
        index: 'tiposusos',
        crear: 'tipouso/crear',
        modificar: 'tipouso/modificar',
    },
    tipos_variables: {
        index: 'tiposvariables',
        crear: 'tipovariable/crear',
        modificar: 'tipovariable/modificar',
    },
    unidades_medidas: {
        index: 'unidadesmedidas',
        crear: 'unidadmedida/crear',
        modificar: 'unidadmedida/modificar',
    },
    variables: {
        index: 'variables',
        crear: 'variable/crear',
        modificar: 'variable/modificar',
    },
    variables_reglas_operacion: {
        index: 'variablesreglasoperaciones',
        crear: 'variablereglaoperacion/crear',
        modificar: 'variablereglaoperacion/modificar',
    },
    Gestion_MTBA: {
        index: 'mesatecnicaba',
        crear: 'mesatecnicaba/crear',
        modificar: 'modificarmtba/modificar',
    },
    //Modificar_MTBA: {
    //  crear: 'modificarmtba/crear',
    // modificar: 'modificarmtba/modificar/:id',
    //},
    MTP: {
        index: 'mesatecnicaprin',
        modificar: 'modificarmtba/modificar',
    },
    Escenarios: {
        index: '',
        crear: 'escenario/crear',
        modificar: 'escenario/modificar',
    },

    Actas: {
        index: 'actas',
        crear: 'acta/crear',
        modificar: 'acta/modificar/:id',
    },
    datos_validados: {
        index: 'datosvalidados',

    },
    modelo_clima: {
        index: 'modeloclima',
    },

}