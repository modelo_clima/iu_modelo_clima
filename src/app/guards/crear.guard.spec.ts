import { TestBed, async, inject } from '@angular/core/testing';

import { CrearGuard } from './crear.guard';

describe('CrearGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CrearGuard]
    });
  });

  it('should ...', inject([CrearGuard], (guard: CrearGuard) => {
    expect(guard).toBeTruthy();
  }));
});
