import { TestBed, async, inject } from '@angular/core/testing';

import { ModificarGuard } from './modificar.guard';

describe('ModificarGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ModificarGuard]
    });
  });

  it('should ...', inject([ModificarGuard], (guard: ModificarGuard) => {
    expect(guard).toBeTruthy();
  }));
});
