import { Injectable } from "@angular/core";
import { CanActivate, Router, ActivatedRouteSnapshot } from "@angular/router";
import { PermisosService } from '../services/permisos/permisos.service';
import { AutenticacionService } from '../services/auth/autenticacion.service';
import { PermisosAppService } from '../services/permisos_app/permisos-app.service';
@Injectable({
  providedIn: "root"
})
/**
 * class class ModificarGuard{}
 * @author Brayan Hernan Castillo Rodriguez
 * Se encarga de proteger las rutas de los componentes que representan la interfaz de usuario para modificar.
 * @LastUpdate 12/01/2020 "Mauricio Fonseca"
 */
export class ModificarGuard implements CanActivate {
  constructor(private autenticacionService: AutenticacionService, private permisosAppService: PermisosAppService, private router: Router) { }

  async canActivate(route: ActivatedRouteSnapshot) {
    let u = (route.url).toString().split(',');
    let url = u[0] + '/' + u[1];
    let urlReturn = '/auth/login';
    if (this.autenticacionService.islogged()) {
      urlReturn = 'sinpermisos';
      let e = await this.permisosAppService.loadPermisosAppSeccion(url, 'modificar');
      if (e && this.permisosAppService.permisos.modificar) {
        return true;
      }
    }
    this.router.navigate([urlReturn]);
    return false;
  }
}
