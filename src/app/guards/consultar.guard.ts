import { Injectable } from "@angular/core";
import { CanActivate, Router, ActivatedRouteSnapshot } from "@angular/router";
import { PermisosService } from "../services/permisos/permisos.service";
import { AutenticacionService } from '../services/auth/autenticacion.service';
import { PermisosAppService } from '../services/permisos_app/permisos-app.service';

@Injectable({
  providedIn: "root",
})

/**
 * class ConsultarGuard{}
 * @author Brayan Hernan Castillo Rodriguez
 * Se encarga de proteger las rutas de los componentes que representan la interfaz de usuario index.
 * @LastUpdate 12/01/2020 "Mauricio Fonseca"
 */
export class ConsultarGuard implements CanActivate {
  constructor(private autenticacionService: AutenticacionService, private permisosAppService: PermisosAppService, private router: Router) { }

  async canActivate(route: ActivatedRouteSnapshot) {
    let url = (route.url).toString().replace(',', '/');
    let urlReturn = '/auth/login';
    if (this.autenticacionService.islogged()) {
      urlReturn = 'sinpermisos';
      if (url == 'inicio') {
        return true;
      }
      let e = await this.permisosAppService.loadPermisosAppSeccion(url, 'all');
      if (e && this.permisosAppService.permisos.index) {
        return true;
      }
    }
    this.router.navigate([urlReturn]);
    return false;
  }
}
