import { TestBed, async, inject } from '@angular/core/testing';

import { ConsultarGuard } from './consultar.guard';

describe('ConsultarGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ConsultarGuard]
    });
  });

  it('should ...', inject([ConsultarGuard], (guard: ConsultarGuard) => {
    expect(guard).toBeTruthy();
  }));
});
