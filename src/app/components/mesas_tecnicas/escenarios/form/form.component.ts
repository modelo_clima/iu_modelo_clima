import { ApiService } from '../../../../services/api/api.service';
import { Component, OnInit } from '@angular/core';
import { IEscenario } from '../../../../classmodels/interfaces/iescenario';
import { EscenarioModel } from '../../../../classmodels/models/escenario-model';
import { Option } from '../../../../classmodels/models/Option-model';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { IBreadcrumb } from 'src/app/classmodels/interfaces/ibreadcrumb';
import { BreadcrumbService } from 'src/app/services/breadcrumb/breadcrumb.service';
import { flatMap } from 'rxjs/operators';


@Component({
  selector: 'app-form-escenario',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.sass']
})
export class FormComponent implements OnInit {

  title: string;
  escenarioModel: EscenarioModel;
  id: number;
  operacion: string;
  inputEstado: any ={estado: '' }
  itemForm: IEscenario = {
    'id': null, 'descripcion': null, 'id_mesa_tecnica': 0, 'fecha_aprobacion': null, 'fecha_creacion': null, 
    'base': 0, 'id_estado': 0, 'estado': null, 'codigo': null, 'fecha_inicio_simulacion': null, 'fecha_fin_simulacion':null
     };
  nombreVariable: string //Variable que trae la api QE QS AC
  editarForm: boolean;//Permiso para editar el escenario dependiendo de su estado
  datos: any = [];
  codigosReglasOperacion: any = []
  valoresReglasOperacion: any = {}
  formularioValoresRO: any = {titulo: '', ver: false, editar:false};
  estadoBoton: boolean;// para ocultar el boton guardar al visualizar
  nombreBoton: any = {tituloboton: ''};
  posicion = 0;

  escenarioBase: IEscenario; 

  estados: Option[] = [
    {id:1, descripcion:'Creado'},
    {id:9, descripcion:'Visualizar'}
  ]

  afertSaveNuevoGrupo(data: any){}
  

  constructor(private activatedRoute: ActivatedRoute, private apiService: ApiService, private router: Router, private breadcrumbService: BreadcrumbService) {
    this.escenarioModel = new EscenarioModel();
    this.escenarioModel.setApiService(this.apiService);
    this.id = this.activatedRoute.snapshot.params.id;
    this.operacion = (!this.id ? 'crear' : 'modificar');
    this.title = 'Crear Escenario';
    if (this.id) {
      this.title = 'Modificar Escenario';
      this.escenarioModel.getItemDetail(this.id, () => {
        this.itemForm = this.escenarioModel.getItem();
        if(this.itemForm.id_estado != 1){
          this.editarForm = false;
          this.nombreBoton.tituloboton="Volver";
        }else{
          this.nombreBoton.tituloboton="Cancelar";
          this.editarForm = true;
        }
      });
    }
    
    console.log(this.itemForm.id_estado);
    console.log(this.editarForm);
    this.initBreadcrumb()
  }

  ngOnInit() {
    this.onCLickReglasOperacion('QE');
  }

  onSubmit() {
    console.log(this.itemForm)
    if (this.operacion == 'modificar') {
      
      this.modificar();
    } else {
      
      this.crear();
    }
  }
  
  

  oncambiarIdEstado(obj){
    this.itemForm.id_estado = parseInt(obj);
    console.log(this.itemForm.id_estado);
  }
  

  private crear() {
    this.escenarioModel.addItem(this.itemForm, () => {
      this.goToIndex();
    });
  }

  private modificar() {
    this.escenarioModel.updateItem(this.id, this.itemForm, () => {
     
     
      this.goToIndex();
    });
  }

  private goToIndex() {
    this.router.navigate(['/modificarmtba/modificar/', this.itemForm.id_mesa_tecnica]);
  }

  private initBreadcrumb() {
   /* let rows = this.breadcrumbService.getBreadcrumb();
    if (rows[rows.length - 1].title != 'Escenarios') {
      let breadcrumb_father: IBreadcrumb = { 'title': 'Escenarios', 'url': '/escenarios' };
      this.breadcrumbService.addItemBreadcrumb(breadcrumb_father);
    }*/
    let url = (!this.id ? '/escenario/crear' : '/escenario/modificar/' + this.id);
    let breadcrumb: IBreadcrumb = { 'title': this.title, 'url': url };
    this.breadcrumbService.validationAddItem(breadcrumb);
  }

  onCLickReglasOperacion(nombreVariable: string) {
    let estado = true;
    for (let item of this.codigosReglasOperacion){
      if(item.modificado){
        estado = false;
        break;
      }
    }
    if (estado) {
      this.nombreVariable = nombreVariable;
      this.datos = [];
      this.codigosReglasOperacion = [];
      this.apiService.list('codigoreglasoperacion/', {
        nombre_variable: nombreVariable,
        "id_escenario": this.id
      }).subscribe(data => {
        for (let item in data) {
          this.datos.push(data[item]);
          for (let itemRO of data[item].codigos_reglas_operacion) {
            if (itemRO.selected) {
              this.codigosReglasOperacion.push({
                id: itemRO.id_regla_operacion_escenario,
                id_codigo_reglas_operacion: itemRO.id,
                descripcion: itemRO.descripcion,
                id_escenario: this.id,
                modificado: false
              });
              break;
            }
          }
        }
      });
    } else{
      
    alert('Se han presentado cambios debe guardar dichos cambios.')
    }
  
  }
  
  


  onChangeCodigoRO(i) {
    console.log(i);
    this.posicion = i;
    let codigos_reglas_operacion = this.datos[i].codigos_reglas_operacion;
    this.codigosReglasOperacion[i].modificado = true;
    for (let item of codigos_reglas_operacion) {
      if (item.id == this.codigosReglasOperacion[i].id_codigo_reglas_operacion) {
        this.codigosReglasOperacion[i].descripcion = item.descripcion
        break;
      }
    }
  }

  onClickCancelarReglasOperacion(){
    this.codigosReglasOperacion = [];
    for (let item of this.datos) {
      for (let itemRO of item.codigos_reglas_operacion) {
        if (itemRO.selected) {
          this.codigosReglasOperacion.push({
            id: itemRO.id_regla_operacion_escenario,
            id_codigo_reglas_operacion: itemRO.id,
            descripcion: itemRO.descripcion,
            id_escenario: this.id,
            modificado: false
          });
          break;
        }
      }
    }
  }

  onClickReglasOperacion(){
    let datosPost ={id_escenario:this.id,datos:[]};
    for (let item of this.codigosReglasOperacion){
      datosPost.datos.push({
        id: item.id,
        id_codigo_reglas_operacion: item.id_codigo_reglas_operacion,
        id_escenario: item.id_escenario,
      });
    }
    this.apiService.create('reglasoperacionescenario/', datosPost).subscribe(data=>{
      this.codigosReglasOperacion = [];
      this.onCLickReglasOperacion(this.nombreVariable);
      alert('Datos guardados');
    })
  }

  onClickCrearReglaOperacion(i){
    let codigos_reglas_operacion = this.datos[i].codigos_reglas_operacion;
    let numCodigo = codigos_reglas_operacion.length + 1;
    let params = {id_codigo_regla_operacion: '', fecha: null};
    this.valoresReglasOperacion= {
      codigo:'RO_'+ numCodigo, 
      descripcion:'', 
      escenario_base:0, 
      id_variable_regla_operacion: '',
      id_escenario: 0,
      valores:[]
    }
    
    for (let item of codigos_reglas_operacion){
      if(item.escenario_base){
        params.id_codigo_regla_operacion = item.id
        params.fecha = this.itemForm.fecha_inicio_simulacion
        this.valoresReglasOperacion.id_variable_regla_operacion = item.id_variable_regla_operacion
        break;
      }
    }
    
    this.apiService.list('valoresreglasoperacion/', params).subscribe(data=>{
      for (let item of data){
        this.valoresReglasOperacion.valores.push(
          {
            valor: item.valor,
            dia: item.dia,
            mes: item.mes
          }
        )
      }
      this.nombreBoton.tituloboton = 'Cancelar';
      this.estadoBoton = true;
      this.formularioValoresRO.titulo = 'Crear reglas de operación';
      this.formularioValoresRO.ver = true;
      this.formularioValoresRO.editar = true;
    });

    this.afertSaveNuevoGrupo = (data) => {
      this.datos[i].codigos_reglas_operacion.push(data)
        this.codigosReglasOperacion[i].id_codigo_reglas_operacion = data.id;
        this.codigosReglasOperacion[i].descripcion = data.descripcion;
        this.codigosReglasOperacion[i].modificado = true;      
    };
  }
  onClickVerReglaOperacion(i){
    let codigos_reglas_operacion = this.datos[i].codigos_reglas_operacion;
    console.log(this.posicion);
    console.log(this.datos[i].codigos_reglas_operacion);
    let params = {id_codigo_regla_operacion: this.codigosReglasOperacion[i].id_codigo_reglas_operacion, 
     fecha : this.itemForm.fecha_inicio_simulacion};
    this.valoresReglasOperacion= {
      codigo:'', 
      descripcion:'', 
      escenario_base:0, 
      id_variable_regla_operacion: '',
      id_escenario: 0,
      valores:[]
    }
    for (let item of this.datos[i].codigos_reglas_operacion){
      if(item.id==this.codigosReglasOperacion[i].id_codigo_reglas_operacion){
        this.valoresReglasOperacion.codigo=  item.codigo;
        this.valoresReglasOperacion.descripcion=  item.descripcion;
        this.valoresReglasOperacion.escenario_base=item.escenario_base;
        this.valoresReglasOperacion.id_variable_regla_operacion= item.id_variable_regla_operacion;

      }
    }
    
    
    this.apiService.list('valoresreglasoperacion/', params).subscribe(data=>{
      for (let item of data){
        this.valoresReglasOperacion.valores.push(
          {
            valor: item.valor,
            dia: item.dia,
            mes: item.mes
          }
        )
      }
      this.nombreBoton.tituloboton = 'Volver';
      this.estadoBoton=false;
      this.formularioValoresRO.titulo = 'Reglas de operación';
      this.formularioValoresRO.ver = true;
      this.formularioValoresRO.editar = false;
    })
  }

  guardarNuevoGrupo(){
    let newItem = '';
      console.log(this.valoresReglasOperacion);
      this.valoresReglasOperacion.id_escenario = this.itemForm.id;
      this.apiService.create('codigoreglasoperacion/', this.valoresReglasOperacion).subscribe(data => {
        this.formularioValoresRO.ver= false;
        newItem = data;
        this.afertSaveNuevoGrupo(data);
        alert('Datos Guardados');
      });
      console.log("Prueba create")
      console.log(newItem)
      
  }
 
  }


