import { ApiService } from '../../../services/api/api.service';
import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { IEscenario } from '../../../classmodels/interfaces/iescenario';
import { INewEscenario } from '../../../classmodels/interfaces/inewescenario';
import { EscenarioModel } from '../../../classmodels/models/escenario-model';
import { Router } from '@angular/router';
import { IBreadcrumb } from 'src/app/classmodels/interfaces/ibreadcrumb';
import { BreadcrumbService } from '../../../services/breadcrumb/breadcrumb.service';
import { IMesatecnicaprin } from 'src/app/classmodels/interfaces/imesatecnicaprin';
import { MesatecnicaprinModel } from 'src/app/classmodels/models/mesatecnicaprin-model';


@Component({
  selector: 'app-escenarios',
  templateUrl: './escenarios.component.html',
  styleUrls: ['./escenarios.component.sass']
})
export class EscenariosComponent implements OnInit {
  title: string = "Escenarios";
  escenarioModel: EscenarioModel;
  mt: MesatecnicaprinModel;
  id: number;
  editarForm: boolean;//permsio para editar escenarios si se encuentra creado. no deja en estado visualizar
  
  dataMt: IMesatecnicaprin  = { 'id': 0, 'codigo':null , 'descripcion': null, 'fecha_cierre': null, 'fecha_creacion':null,
  'fecha_fin_simulacion':null, 'fecha_inicio_simulacion': null, 'id_mesa_tecnica_principal': null, 'id_tipo_mesa_tecnica':0, 'id_estado':0, 'estado':null, 'id_mt_asociada':0, 'codigo_mt_asociada':null};
  
  @Input('mesa_tecnica')
 set mesaTecnica(mesaT: IMesatecnicaprin) {
    this.dataMt = mesaT
    this.id = this.dataMt.id;
    if(this.dataMt.id_estado != 3){
      this.editarForm = false;
    }else{
      this.editarForm = true;
    }
  }

  @Input('escenarios')
  set setEscenarios(escenarios: IEscenario[]){
    this.escenarioModel.setItems(escenarios);
  }
  //------
  operacion: string;
  itemForm: IEscenario = { 'id': 0, 'descripcion': null, 'id_mesa_tecnica': this.dataMt.id, 'fecha_aprobacion': null,
   'fecha_creacion': null, 'base':0, 'estado':null, 'id_estado':0, 'codigo':null, 'fecha_inicio_simulacion': null, 'fecha_fin_simulacion': null
  };

  newItem: INewEscenario = {'id_mesa_tecnica':0, 'base': 0}
  //-------
 
 
  constructor(private apiService: ApiService, private breadcrumbService: BreadcrumbService, private router: Router) {
    //this.initBreadcrumb();
    this.escenarioModel = new EscenarioModel();
    this.escenarioModel.setApiService(this.apiService);
    
    
  }

  ngOnInit() {
  }

  onClickDelete(data: IEscenario) {
    
    this.escenarioModel.deleteItem(data.id, data.id_mesa_tecnica );
    //this.escenarioModel.refresh(data.id_mesa_tecnica);
    alert("No se pude eliminar porque se encuentra relacionado")

  }
  
  editEscenario(post){
    if(post.base==0){
     /* if(post.estado=="Creado"){
        this.router.navigate(['/escenario/modificar',post.id]);
      }else if(post.estado=="Inactivo"){
        alert("Mesa técnica inactiva")
      }else if(post.estado=="Cerrado"){
        alert("Mesa técnica cerrada")
      }else if(post.estado=="Visualizar"){
        alert("No se puede modificar escenario ya que se encuentra en estado Visualizar ")
      }*/
      this.router.navigate(['/escenario/modificar',post.id]);
    }else{
      alert("El Escenario base no se puede editar")
    }
   
    
  }

//-------------------------------------------------
crear() {
  this.newItem.id_mesa_tecnica = this.id;
  this.escenarioModel.addItem(this.newItem, (newData?:any) => {
    this.router.navigate(['/escenario/modificar/',newData.id]);
  });
}

onSubmit() {
  if (this.operacion == 'modificar') {
    
    
  } else {
    this.crear();
  }
}





//--------------------------------------------
  

 /* private initBreadcrumb() {
    this.breadcrumbService.initBreadcrumb();
    let breadcrumb: IBreadcrumb = { 'title': this.title, 'url': '/escenarios' };
    this.breadcrumbService.validationAddItem(breadcrumb);
  }
*/
}
