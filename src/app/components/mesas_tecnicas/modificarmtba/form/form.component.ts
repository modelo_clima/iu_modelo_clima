import { ApiService } from '../../../../services/api/api.service';
import { Component, OnInit } from '@angular/core';
import { IModificarmtba } from '../../../../classmodels/interfaces/imodificarmtba';
import { IMesatecnicaprin } from '../../../../classmodels/interfaces/imesatecnicaprin';
import { ModificarmtbaModel } from '../../../../classmodels/models/modificarmtba-model';
import { MesatecnicabaModel } from '../../../../classmodels/models/mesatecnicaba-model';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { IBreadcrumb } from 'src/app/classmodels/interfaces/ibreadcrumb';
import { BreadcrumbService } from 'src/app/services/breadcrumb/breadcrumb.service';
import { IMesatecnicaba } from 'src/app/classmodels/interfaces/imesatecnicaba';
import { IEscenario } from 'src/app/classmodels/interfaces/iescenario';

@Component({
  selector: 'app-form-modificarmtba',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.sass']
})
export class FormComponent implements OnInit {
  title: string = "";
  editEstado: any = {accionEstado: true, valorEstado: 0};//Mostrar,ocultar el estado depeniendo si esta cerrado o activa
  nombreBoton: any = {tituloboton: ''}; //configuracion botones guradar cancelar dependiendo del estado
  editarForm: boolean = false; //permiso para editar modificarmtba si no se encuentra cerrada
  modificarmtbaModel: ModificarmtbaModel;
  mesatecnicabaModel: MesatecnicabaModel;
  numero: string;
  id: number;
  id_mtba: number;
  itemForm: IMesatecnicaprin  = { 'id': 0, 'codigo':null , 'descripcion': null, 'fecha_cierre': null, 'fecha_creacion':null,
  'fecha_fin_simulacion':null, 'fecha_inicio_simulacion': null, 'id_mesa_tecnica_principal': null, 'id_tipo_mesa_tecnica':0, 'id_estado':0, 'estado':null, 'id_mt_asociada':0, 'codigo_mt_asociada':null};
  escenarios: IEscenario[] = [];
  cambioValorIndex: any = [];//tab de escenarios y actas
  nombreIndex: string;

  constructor(private activatedRoute: ActivatedRoute, private apiService: ApiService, private router: Router, private breadcrumbService: BreadcrumbService) {
    this.initBreadcrumb(); 
    this.mesatecnicabaModel = new MesatecnicabaModel();
    this.mesatecnicabaModel.setApiService(this.apiService);
    this.id = this.activatedRoute.snapshot.params.id;
    this.mesatecnicabaModel.getItemDetail(this.id, () => {
      this.itemForm = this.mesatecnicabaModel.getItem();
      this.escenarios = this.mesatecnicabaModel.getEscenarios();
      if(this.itemForm.id_tipo_mesa_tecnica==1){
        this.title += " Modificar mesa técncia Principal";
      }else{
        this.title += " Modificar mesa técncia Ambiental";
      }
      if(this.itemForm.id_estado != 3 && this.itemForm.id_estado != 7){
        this.editarForm = false;
        this.nombreBoton.tituloboton = "Volver";
        this.editEstado.accionEstado = false;
      }else{
        this.editEstado.accionEstado = true;
        this.editEstado.valorEstado = this.itemForm.id_estado;
        this.editarForm = true;
        this.nombreBoton.tituloboton = "Cancelar";
      }
    });
    this.cambioValorIndex[0] = false;
    this.cambioValorIndex[1] = false;
  }

  ngOnInit() {
    this.cambioIndex(1);  //Con 1 carga los escenarios autmaticamente al entrar a modificar
    
  }

  onSubmit() {
    this.mesatecnicabaModel.updateItem(this.id, this.itemForm, () => {
      this.goToIndex();
    });
  }

  goToIndex(){
    let urlReturn;
    if(this.itemForm.id_tipo_mesa_tecnica == 1){
      urlReturn = "/mesatecnicaprin";
    }else{
      urlReturn = "/mesatecnicaba";
    }
    this.router.navigate([urlReturn]);
  }

  handleChange(value){ 
    this.itemForm.id_estado=value;
    console.log(parseInt(value));
  }
  
  cambioIndex(i){
    this.cambioValorIndex[i] = true;
    if(i == 0){
      this.nombreIndex = "Actas";
      this.cambioValorIndex[i+1] = false;
    }else{
      this.nombreIndex = "Escenarios";
      this.cambioValorIndex[i-1] = false;
    }
  }

  private initBreadcrumb() {
    let rows = this.breadcrumbService.getBreadcrumb();
    if(rows[rows.length-1].title!='Modifcar mesa técnica '){
      let breadcrumb_father: IBreadcrumb = { 'title': this.title, 'url': '/modificarmtba' };
      this.breadcrumbService.addItemBreadcrumb(breadcrumb_father);
    }
    let url = (!this.itemForm.id ? '/mesatecnicaba/crear' : '/mesatecnicaba/modificar' + this.itemForm.id);
    let breadcrumb: IBreadcrumb = { 'title': this.title, 'url': url };
    this.breadcrumbService.validationAddItem(breadcrumb);
  }

}
