import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModificarmtbaComponent } from './modificarmtba.component';

describe('ModificarmtbaComponent', () => {
  let component: ModificarmtbaComponent;
  let fixture: ComponentFixture<ModificarmtbaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModificarmtbaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModificarmtbaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
