import { ApiService } from '../../../services/api/api.service';
import { BreadcrumbService } from '../../../services/breadcrumb/breadcrumb.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { IModificarmtba } from '../../../classmodels/interfaces/imodificarmtba';
import { ModificarmtbaModel } from '../../../classmodels/models/modificarmtba-model';
import { Router } from '@angular/router';
import { IBreadcrumb } from 'src/app/classmodels/interfaces/ibreadcrumb';

@Component({
  selector: 'app-modificarmtba',
  templateUrl: './modificarmtba.component.html',
  styleUrls: ['./modificarmtba.component.sass']
})
export class ModificarmtbaComponent implements OnInit {
  title: string = "Modificar mesa tecnica Ambiental";
  modificarmtbaModel: ModificarmtbaModel;
  
 
  constructor(private apiService: ApiService, private breadcrumbService: BreadcrumbService, private router: Router) {
    this.initBreadcrumb();
    this.modificarmtbaModel = new ModificarmtbaModel();
    this.modificarmtbaModel.setApiService(this.apiService);
  }

  ngOnInit() {
  }

  onClickDelete(data: IModificarmtba) {
    this.modificarmtbaModel.deleteItem(data.id);
  }
  private initBreadcrumb() {
    this.breadcrumbService.initBreadcrumb();
    let breadcrumb: IBreadcrumb = { 'title': this.title, 'url': '/modificarmtba' };
    this.breadcrumbService.validationAddItem(breadcrumb);
  }

}
