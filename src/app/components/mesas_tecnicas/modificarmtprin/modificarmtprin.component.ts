import { ApiService } from '../../../services/api/api.service';
import { BreadcrumbService } from '../../../services/breadcrumb/breadcrumb.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { IModificarmtprin } from '../../../classmodels/interfaces/imodificarmtprin';
import { ModificarmtprinModel } from '../../../classmodels/models/modificarmtprin-model';
import { Router } from '@angular/router';
import { IBreadcrumb } from 'src/app/classmodels/interfaces/ibreadcrumb';

@Component({
  selector: 'app-modificarmtprin',
  templateUrl: './modificarmtprin.component.html',
  styleUrls: ['./modificarmtprin.component.sass']
})
export class ModificarmtprinComponent implements OnInit {
  title: string = "Modificar mesa tecnica principal";
  modificarmtprin: ModificarmtprinModel;
 
  constructor(private apiService: ApiService, private breadcrumbService: BreadcrumbService, private router: Router) {
    this.initBreadcrumb();
    this.modificarmtprin = new ModificarmtprinModel();
    this.modificarmtprin.setApiService(this.apiService);
    this.modificarmtprin.refresh();
  }

  ngOnInit() {
  }

  onClickDelete(data: IModificarmtprin) {
    this.modificarmtprin.deleteItem(data.id);
  }
  private initBreadcrumb() {
    this.breadcrumbService.initBreadcrumb();
    let breadcrumb: IBreadcrumb = { 'title': this.title, 'url': '/modificarmtprin' };
    this.breadcrumbService.validationAddItem(breadcrumb);
  }

}
