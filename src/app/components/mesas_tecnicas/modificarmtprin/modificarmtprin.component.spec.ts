import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModificarmtprinComponent } from './modificarmtprin.component';

describe('ModificarmtprinComponent', () => {
  let component: ModificarmtprinComponent;
  let fixture: ComponentFixture<ModificarmtprinComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModificarmtprinComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModificarmtprinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
