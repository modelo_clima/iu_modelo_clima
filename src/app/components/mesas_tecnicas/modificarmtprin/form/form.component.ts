import { ApiService } from '../../../../services/api/api.service';
import { Component, OnInit } from '@angular/core';
import { IModificarmtprin } from '../../../../classmodels/interfaces/imodificarmtprin';
import { ModificarmtprinModel } from '../../../../classmodels/models/modificarmtprin-model';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { IBreadcrumb } from 'src/app/classmodels/interfaces/ibreadcrumb';
import { BreadcrumbService } from 'src/app/services/breadcrumb/breadcrumb.service';

@Component({
  selector: 'app-form-modificarmtprin',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.sass']
})
export class FormComponent implements OnInit {

  title: string;
  modificarmtprinModel: ModificarmtprinModel;
  id: number;
  operacion: string;
  itemForm  = { 'id': null, 'codigo':null , 'descripcion': '', 'fecha_cierre': null, 'fecha_creacion':null,
  'fecha_fin_simulacion':null, 'fecha_inicio_simulacion': null, 'id_mesa_tecnica_principal': null, 'id_tipo_mesa_tecnica':2 };

  constructor(private activatedRoute: ActivatedRoute, private apiService: ApiService, private router: Router, private breadcrumbService: BreadcrumbService) {
    this.modificarmtprinModel = new ModificarmtprinModel();
    this.activatedRoute.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.itemForm = this.router.getCurrentNavigation().extras.state.formDataParams;
        this.id = this.router.getCurrentNavigation().extras.state.formDataParams.id;
      }
    });
    this.modificarmtprinModel.setApiService(this.apiService);
    this.id = this.activatedRoute.snapshot.params.id;
    this.operacion = (!this.id ? 'crear' : 'modificar');
    this.title = 'Modificar mesa tecnica principal';
    if (this.id) {
      this.title = 'Modificar mesa tecnica';
      this.modificarmtprinModel.getItemDetail(this.id, () => {
        this.itemForm = this.modificarmtprinModel.getItem();
      });

    }
    this.initBreadcrumb()
  }

  ngOnInit() {
  }

  onSubmit() {
    if (this.operacion == 'modificar') {
      this.modificar();
    } else {
      this.crear();
    }
  }

  private crear() {
    this.modificarmtprinModel.addItem(this.itemForm, () => {
      this.goToIndex();
    });
  }

  private modificar() {
    this.modificarmtprinModel.updateItem(this.id, this.itemForm, () => {
      this.goToIndex();
    });
  }

  private goToIndex() {
    this.router.navigate(['/modificarmtprin']);
  }

  private initBreadcrumb() {
    let rows = this.breadcrumbService.getBreadcrumb();
    if(rows[rows.length-1].title!='Modificar mesa tecnica principal'){
      let breadcrumb_father: IBreadcrumb = { 'title': 'Ver mesa tecnica Principal', 'url': '/modificarmtprin' };
      this.breadcrumbService.addItemBreadcrumb(breadcrumb_father);
    }
    let url = (!this.id ? '/mesatecnicaprin/crear' : '/mesatecnicaprin/modificar/' + this.id);
    let breadcrumb: IBreadcrumb = { 'title': this.title, 'url': url };
    this.breadcrumbService.validationAddItem(breadcrumb);
  }

}
