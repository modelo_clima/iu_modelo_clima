import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReglasoperacionComponent } from './reglasoperacion.component';

describe('ReglasoperacionComponent', () => {
  let component: ReglasoperacionComponent;
  let fixture: ComponentFixture<ReglasoperacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReglasoperacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReglasoperacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
