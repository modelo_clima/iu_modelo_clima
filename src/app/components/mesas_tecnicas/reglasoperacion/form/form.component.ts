import { ApiService } from '../../../../services/api/api.service';
import { Component, OnInit } from '@angular/core';
import { IReglasoperacion } from '../../../../classmodels/interfaces/ireglasoperacion';
import { ReglasoperacionModel } from '../../../../classmodels/models/reglasoperacion-model';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { IBreadcrumb } from 'src/app/classmodels/interfaces/ibreadcrumb';
import { BreadcrumbService } from 'src/app/services/breadcrumb/breadcrumb.service';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-form-reglasoperacion',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.sass']
})
export class FormComponent implements OnInit {

  title: string;
  reglasoperacionModel: ReglasoperacionModel;
  id: number;
  operacion: string;
  inputMes: any ={anio: '', mes: '01' }
  itemForm: IReglasoperacion = { 'id_escenario': null, 'fecha':null,'id_variable_regla_operacion':null,'valor':null};

  constructor(private activatedRoute: ActivatedRoute, private apiService: ApiService, private router: Router,
     private breadcrumbService: BreadcrumbService, private spinner: NgxSpinnerService) {
    this.reglasoperacionModel = new ReglasoperacionModel();
    this.reglasoperacionModel.setApiService(this.apiService);
    this.id = this.activatedRoute.snapshot.params.id;
    this.operacion = (!this.id ? 'crear' : 'modificar');
    this.title = 'Crear reglas operacion';
    if (this.id) {
      this.title = 'Modificar reglas operacion';
      this.reglasoperacionModel.getItemDetail(this.id, () => {
        this.itemForm = this.reglasoperacionModel.getItem();
      });

    }
    this.initBreadcrumb()
  }

  ngOnInit() {
  }

  onSubmit() {
    this.spinner.show();
    if (this.operacion == 'modificar') {
      this.modificar();
    } else {
      this.crear();
 
      setTimeout(() => {
        /** spinner ends after 8 seconds */
        this.spinner.hide();
      },3000);
      
    }
  }

  editMesaTecnica(post) {
    let navigationExtras: NavigationExtras = {
      state: {
        formDataParams: post
      }
    };
    this.router.navigate(['/modificarmtprin/modificar'], navigationExtras);
  }

  private crear() {
    this.reglasoperacionModel.addItem(this.itemForm, () => {
      this.goToIndex();
      alert("Regla de operacion creada");
    });
  }

  private modificar() {
    this.reglasoperacionModel.updateItem(this.id, this.itemForm, () => {
      this.goToIndex();
    });
  }

  private goToIndex() {
    this.router.navigate(['/mesatecnicaprin']);
  }

  private initBreadcrumb() {
    let rows = this.breadcrumbService.getBreadcrumb();
    if(rows[rows.length-1].title!='Mesa técnica Ambiental'){
      let breadcrumb_father: IBreadcrumb = { 'title': '', 'url': '/mesatecnicaprin' };
      this.breadcrumbService.addItemBreadcrumb(breadcrumb_father);
    }
    let url = (!this.id ? '/mesatecnicaprin/crear' : '/mesatecnicaprin/modificar/' + this.id);
    let breadcrumb: IBreadcrumb = { 'title': this.title, 'url': url };
    this.breadcrumbService.validationAddItem(breadcrumb);
  }

}
