import { ApiService } from '../../../services/api/api.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { IReglasoperacion } from '../../../classmodels/interfaces/ireglasoperacion';
import { EstadoModel } from '../../../classmodels/models/estado-model';
import { BreadcrumbService } from '../../../services/breadcrumb/breadcrumb.service';
import { Router } from '@angular/router';
import { IBreadcrumb } from 'src/app/classmodels/interfaces/ibreadcrumb';
import { ReglasoperacionModel } from 'src/app/classmodels/models/reglasoperacion-model';

@Component({
  selector: 'app-reglasoperacion',
  templateUrl: './reglasoperacion.component.html',
  styleUrls: ['./reglasoperacion.component.sass']
})
export class ReglasoperacionComponent implements OnInit {

  title: string = "Reglas Operacion";
  reglasoperacionModel: ReglasoperacionModel;
 
  constructor(private apiService: ApiService, private breadcrumbService: BreadcrumbService, private router: Router) {
    this.initBreadcrumb();
    this.reglasoperacionModel = new ReglasoperacionModel();
    this.reglasoperacionModel.setApiService(this.apiService);
    this.reglasoperacionModel.refresh();
  }

  ngOnInit() {
  }

  onClickDelete(data: IReglasoperacion) {
    this.reglasoperacionModel.deleteItem(data.id_escenario);
  }

  private returnPage() {
    this.router.navigate(['/escenarios']);
  }

  private initBreadcrumb() {
    this.breadcrumbService.initBreadcrumb();
    let breadcrumb: IBreadcrumb = { 'title': this.title, 'url': '/reglas operacion' };
    this.breadcrumbService.validationAddItem(breadcrumb);
  }

}
