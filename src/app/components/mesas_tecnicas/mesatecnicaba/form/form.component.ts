import { ApiService } from '../../../../services/api/api.service';
import { Component, OnInit } from '@angular/core';
import { IMesatecnicaba } from '../../../../classmodels/interfaces/imesatecnicaba';
import { MesatecnicabaModel } from '../../../../classmodels/models/mesatecnicaba-model';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { IBreadcrumb } from 'src/app/classmodels/interfaces/ibreadcrumb';
import { BreadcrumbService } from 'src/app/services/breadcrumb/breadcrumb.service';
import { NgxSpinnerService } from "ngx-spinner";


@Component({
  selector: 'app-form-mesatecnicaba',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.sass']
})
export class FormComponent implements OnInit {

  title: string;
  mesatecnicabaModel: MesatecnicabaModel;
  id: number;
  operacion: string;
  loadingShow: boolean;
  alert_sise: any
  inputMes: any ={anio: '', mes: '01' }
  itemForm: IMesatecnicaba = { 'id': null, 'codigo': null, 'descripcion': '', 'fecha_cierre': null, 'fecha_creacion':null,
                                'fecha_fin_simulacion':null, 'fecha_inicio_simulacion': '', 'id_estado':1,'estado':null, 'id_mesa_tecnica_principal': null, 'id_tipo_mesa_tecnica':2, 'id_mt_asociada':0, 'codigo_mt_asociada': null};

  constructor(private activatedRoute: ActivatedRoute, private apiService: ApiService, 
    private router: Router, private breadcrumbService: BreadcrumbService, private spinner: NgxSpinnerService) {
    this.initBreadcrumb();
    this.loadingShow = false;
    this.alert_sise = { message: '', type: '', show: false };
    this.mesatecnicabaModel = new MesatecnicabaModel();
    this.mesatecnicabaModel.setApiService(this.apiService);
    this.id = this.activatedRoute.snapshot.params.id;
    this.operacion = (!this.id ? 'crear' : 'modificar');
    this.title = 'Crear mesa tecnica';
    if (this.id) {
      this.title = 'Modificar mesa tecnica';
      this.mesatecnicabaModel.getItemDetail(this.id, () => {
        this.itemForm = this.mesatecnicabaModel.getItem();
      });

    }
    this.initBreadcrumb()
    
  }

  ngOnInit() {
  }

  onSubmit() {
    //this.spinner.show();
    this.loadingShow = true;
    //if (this.operacion == 'modificar') {
      
    //} else {
      this.itemForm.fecha_inicio_simulacion = this.inputMes.anio+'-'+this.inputMes.mes+'-01';
      this.crear();        
    //}
    
    setTimeout(() => {
      /** spinner ends after 2 seconds */
      this.spinner.hide();
    },2000);
  }

  /*editMesaTecnica(post) {
    this.spinner.show();
    let navigationExtras: NavigationExtras = {
      state: {
        formDataParams: post
      }
    };
    this.router.navigate(['/modificarmtba/modificar'], navigationExtras);
    setTimeout(() => {
      /** spinner ends after 2 seconds */
      /*this.spinner.hide();
    },2000);
    
  }

  viewMesaTecnica(post) {
    let navigationExtras: NavigationExtras = {
      state: {
        formDataParams: post
      }
    };
    this.router.navigate(['/modificarmtbaread/modificar'], navigationExtras);
  }*/

  private crear() {    
    this.loadingShow = true;
    this.mesatecnicabaModel.addItem(this.itemForm,() => {
      this.goToIndex();
      this.showMessageBox('ok', 'Datos Guardados.');
      this.loadingShow = false;
      alert("Mesa Técnica Ambiental Creada");
    });
  }

  showMessageBox(type_msg, message_txt){
    this.alert_sise.message =message_txt;
    this.alert_sise.type = type_msg;
    this.alert_sise.show = true;
  }

 

  private goToIndex() {
    this.router.navigate(['/mesatecnicaba']);
  }

  private initBreadcrumb() {
    let rows = this.breadcrumbService.getBreadcrumb();
    if(rows[rows.length-1].title!='Mesa técnica Ambiental'){
      let breadcrumb_father: IBreadcrumb = { 'title': '', 'url': '/mesatecnicaba' };
      this.breadcrumbService.addItemBreadcrumb(breadcrumb_father);
    }
    let url = (!this.id ? '/mesatecnicaba/crear' : '/mesatecnicaba/modificar/' + this.id);
    let breadcrumb: IBreadcrumb = { 'title': this.title, 'url': url };
    this.breadcrumbService.validationAddItem(breadcrumb);
  }

}
