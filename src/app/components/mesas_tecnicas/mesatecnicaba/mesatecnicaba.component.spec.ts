import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MesatecnicabaComponent } from './mesatecnicaba.component';

describe('MesatecnicabaComponent', () => {
  let component: MesatecnicabaComponent;
  let fixture: ComponentFixture<MesatecnicabaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MesatecnicabaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MesatecnicabaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
