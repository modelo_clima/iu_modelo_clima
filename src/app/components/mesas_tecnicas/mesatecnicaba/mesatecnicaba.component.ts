import { ApiService } from '../../../services/api/api.service';
import { BreadcrumbService } from '../../../services/breadcrumb/breadcrumb.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { IMesatecnicaba } from '../../../classmodels/interfaces/imesatecnicaba';
import { MesatecnicabaModel } from '../../../classmodels/models/mesatecnicaba-model';
import { Router, NavigationExtras } from '@angular/router';
import { IBreadcrumb } from 'src/app/classmodels/interfaces/ibreadcrumb';
//import { ModalService } from '../modal/modal.service';

@Component({
  selector: 'app-mesatecnicaba',
  templateUrl: './mesatecnicaba.component.html',
  styleUrls: ['./mesatecnicaba.component.sass']
})
export class MesatecnicabaComponent implements OnInit {

  title: string = "Mesas Técnicas Bio Ambiental";
  mesatecnicabaModel: MesatecnicabaModel;
 
  constructor(private apiService: ApiService, private breadcrumbService: BreadcrumbService, private router: Router) {
    this.initBreadcrumb();
    this.mesatecnicabaModel = new MesatecnicabaModel();
    this.mesatecnicabaModel.setApiService(this.apiService);
    this.mesatecnicabaModel.refresh("NO", null);
  }

  ngOnInit() {
  }

  editMesaTecnica(post) {
 
  /*  if(post.estado=="Activo"){
      this.router.navigate(['/modificarmtba/modificar/'+post.id]);
    }else if(post.estado=="Inactivo"){
      alert("Mesa técnica inactiva")
    }else if(post.estado=="Cerrado"){
      alert("Mesa técnica cerrada")
    }*/
    this.router.navigate(['/modificarmtba/modificar/'+post.id]);
  }
  
  verMesaTecnica(post) {
 
    let navigationExtras: NavigationExtras = {
      state: {
        formDataParams: post
      }
    };
    this.router.navigate(['/modificarmtba/modificar'], navigationExtras);
  
  }
  
  onClickDelete(data: IMesatecnicaba) {

    this.mesatecnicabaModel.deleteItem(data.id);
  }



  private initBreadcrumb() {
    this.breadcrumbService.initBreadcrumb();
    let breadcrumb: IBreadcrumb = { 'title': this.title, 'url': '/mesatecnicaba' };
    this.breadcrumbService.validationAddItem(breadcrumb);
  }

}
