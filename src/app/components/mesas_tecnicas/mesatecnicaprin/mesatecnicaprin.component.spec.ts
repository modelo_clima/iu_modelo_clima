import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MesatecnicaprinComponent } from './mesatecnicaprin.component';

describe('MesatecnicaprinComponent', () => {
  let component: MesatecnicaprinComponent;
  let fixture: ComponentFixture<MesatecnicaprinComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MesatecnicaprinComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MesatecnicaprinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
