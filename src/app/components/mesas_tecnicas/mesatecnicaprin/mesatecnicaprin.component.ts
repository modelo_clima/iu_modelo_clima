import { ApiService } from '../../../services/api/api.service';
import { BreadcrumbService } from '../../../services/breadcrumb/breadcrumb.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { IMesatecnicaprin } from '../../../classmodels/interfaces/imesatecnicaprin';
import { MesatecnicaprinModel } from '../../../classmodels/models/mesatecnicaprin-model';
import { Router, NavigationExtras } from '@angular/router';
import { IBreadcrumb } from 'src/app/classmodels/interfaces/ibreadcrumb';
import { MesatecnicabaModel } from 'src/app/classmodels/models/mesatecnicaba-model';

@Component({
  selector: 'app-mesatecnicaprin',
  templateUrl: './mesatecnicaprin.component.html',
  styleUrls: ['./mesatecnicaprin.component.sass']
})
export class MesatecnicaprinComponent implements OnInit {

  title: string = "Index mesas técnicas Principal";
  mesatecnicaprinModel: MesatecnicaprinModel;
  formularioCrearMTP: any = {titulo: '', ver: false};
  mesatecnicabaModel: MesatecnicabaModel;
  itemForm: IMesatecnicaprin = { 'id': 0, 'codigo': null, 'descripcion': '', 'fecha_cierre': null, 'fecha_creacion':null,
                                'fecha_fin_simulacion':null, 'fecha_inicio_simulacion': null, 'id_estado':1,'estado':null, 'id_mesa_tecnica_principal': null, 'id_tipo_mesa_tecnica':1, 'id_mt_asociada':0, 'codigo_mt_asociada': null};
 
  constructor(private apiService: ApiService, private breadcrumbService: BreadcrumbService, private router: Router) {
    this.initBreadcrumb();
    this.mesatecnicaprinModel = new MesatecnicaprinModel();
    this.mesatecnicaprinModel.setApiService(this.apiService);
    this.mesatecnicaprinModel.refresh();
  }

  ngOnInit() {
  }

  crearMtp(){
    this.mesatecnicabaModel = new MesatecnicabaModel();    
    this.mesatecnicabaModel.setApiService(this.apiService); 
    this.formularioCrearMTP.titulo = "Selección Periodo Simulación";
    this.formularioCrearMTP.ver = true;
    this.mesatecnicabaModel.refresh("SI", ()=>{
      if(this.mesatecnicabaModel.getItems().length==0){
        alert("Apreciado usuario, recuerde que debe contar con una MTBA de base para poder crear una MTP.");
        this.formularioCrearMTP.ver = false;
      };
    });
  }

  editMesaTecnica(post) {
    let navigationExtras: NavigationExtras = {
      state: {
        formDataParams: post
      }
    };
    this.router.navigate(['/modificarmtprin/modificar'], navigationExtras);
  }

  onClickDelete(data: IMesatecnicaprin) {
    this.mesatecnicaprinModel.deleteItem(data.id);
  }

  private initBreadcrumb() {
    this.breadcrumbService.initBreadcrumb();
    let breadcrumb: IBreadcrumb = { 'title': this.title, 'url': '/mesatecnicaprin' };
    this.breadcrumbService.validationAddItem(breadcrumb);
  }

 // Asociar MTBA a MTP

 asociarMtba(id_mtba: number) {  
  this.itemForm.id_mt_asociada=id_mtba;
  this.mesatecnicaprinModel.addItem(this.itemForm,() => {
    this.formularioCrearMTP.ver = false;
    alert("Mesa Técnica Principal Creada");
    this.mesatecnicaprinModel.refresh();
  });       
}


}
