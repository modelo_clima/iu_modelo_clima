import { ApiService } from '../../../../services/api/api.service';
import { Component, OnInit, Input } from '@angular/core';
import { IActa } from '../../../../classmodels/interfaces/iacta';
import { ActaModel } from '../../../../classmodels/models/acta-model';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { IBreadcrumb } from 'src/app/classmodels/interfaces/ibreadcrumb';
import { BreadcrumbService } from 'src/app/services/breadcrumb/breadcrumb.service';



@Component({
  selector: 'app-form-actas',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.sass']
})

export class FormComponent implements OnInit {
  id_mesa_tecnica: number;
  title: string;
  actaModel: ActaModel;
  id: number;
  operacion: string;



  itemForm: IActa = { 'id': null, 'nombre': '', 'descripcion': '', 'id_mesa_tecnica': 4 , 'adjunto': null,
  'fecha_creacion': '' };

  constructor(private activatedRoute: ActivatedRoute, private apiService: ApiService, private router: Router, private breadcrumbService: BreadcrumbService) {
    this.actaModel = new ActaModel();
    this.actaModel.setApiService(this.apiService);
    this.id = this.activatedRoute.snapshot.params.id;
    this.operacion = (!this.id ? 'crear' : 'modificar');
    this.title = 'Crear Acta';
    if (this.id) {
      this.title = 'Modificar Acta';
      this.actaModel.getItemDetail(this.id, () => {
        this.itemForm = this.actaModel.getItem();
      });
    }
    this.initBreadcrumb()
  }

  ngOnInit() {
  }

  onSubmit() {
    if (this.operacion == 'modificar') {
      this.modificar();
    } else {
      this.crear();
    }
  }

  private crear() {
    this.actaModel.addItem(this.itemForm, () => {
      this.goToIndex();
    });
  }

  private modificar() {
    this.actaModel.updateItem(this.id, this.itemForm, () => {
      this.goToIndex();
    });
  }

  private goToIndex() {
    this.router.navigate(['/modificarmtba/modificar/',this.itemForm.id_mesa_tecnica]);
  }

  private initBreadcrumb() {
    let rows = this.breadcrumbService.getBreadcrumb();
    if(rows[rows.length-1].title!='Actas'){
      let breadcrumb_father: IBreadcrumb = { 'title': 'Actas', 'url': '/actas' };
      this.breadcrumbService.addItemBreadcrumb(breadcrumb_father);
    }
    let url = (!this.id ? '/acta/crear' : '/acta/modificar/' + this.id);
    let breadcrumb: IBreadcrumb = { 'title': this.title, 'url': url };
    this.breadcrumbService.validationAddItem(breadcrumb);
  }



}
