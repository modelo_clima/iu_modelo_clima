import { ApiService } from '../../../services/api/api.service';
import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { IActa } from '../../../classmodels/interfaces/iacta';
import { ActaModel } from '../../../classmodels/models/acta-model';
import { Router } from '@angular/router';
import { IBreadcrumb } from 'src/app/classmodels/interfaces/ibreadcrumb';
import { BreadcrumbService } from '../../../services/breadcrumb/breadcrumb.service';


@Component({
  selector: 'app-actas',
  templateUrl: './actas.component.html',
  styleUrls: ['./actas.component.sass']
})
export class ActasComponent implements OnInit {
  id_mesa_tecnica: number;
  title: string = "Actas";
  actaModel: ActaModel;
  
  @Input('id_mesa_tecnica')
  set mesaTecnica(id: number) {
    this.id_mesa_tecnica = id;
    this.actaModel.refresh(this.id_mesa_tecnica);
  }
  
  constructor(private apiService: ApiService, private breadcrumbService: BreadcrumbService, private router: Router) {
   // this.initBreadcrumb();
    this.actaModel = new ActaModel();
    this.actaModel.setApiService(this.apiService);
  }

  ngOnInit() {
  }

  onClickDelete(data: IActa) {
    this.actaModel.deleteItem(data.id, data.id_mesa_tecnica );
    
  }

  private returnPage() {
    this.router.navigate(['/actas']);
  }
  








  /*
   operacion: string;
   itemForm: IActa = { 'id': null, 'nombre': '', 'descripcion': '', 'id_mesa_tecnica': this.id_mesa_tecnica, 'adjunto': null,
   'fecha_creacion': null };

  private initBreadcrumb() {
    this.breadcrumbService.initBreadcrumb();
    let breadcrumb: IBreadcrumb = { 'title': this.title, 'url': '/actas' };
    this.breadcrumbService.validationAddItem(breadcrumb);
  }



  editEscenario(post){
    if(post.base==0){
      if(post.estado=="Creado"){
        this.router.navigate(['/escenario/modificar',post.id]);
      }else if(post.estado=="Inactivo"){
        alert("Mesa técnica inactiva")
      }else if(post.estado=="Cerrado"){
        alert("Mesa técnica cerrada")
      }
    }else{
      alert("El Escenario base no se puede editar")
    }
  }
*/
//-------------------------------------------------
/*
crear() {
  this.newItem.id_mesa_tecnica = this.id_mesa_tecnica
  this.escenarioModel.addItem(this.newItem, (newData?:any) => {
    this.router.navigate(['/escenario/modificar/',newData.id]);
  });
}

onSubmit() {
  if (this.operacion == 'modificar') {
    
  } else {
    this.crear();
  }
}

*/




//--------------------------------------------
  

 /* private initBreadcrumb() {
    this.breadcrumbService.initBreadcrumb();
    let breadcrumb: IBreadcrumb = { 'title': this.title, 'url': '/escenarios' };
    this.breadcrumbService.validationAddItem(breadcrumb);
  }
*/
}
