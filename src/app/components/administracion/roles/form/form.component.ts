import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";

import { IRol } from "../../../../classmodels/interfaces/irol";
import { EntidadModel } from "../../../../classmodels/models/entidad-model";

import { IBreadcrumb } from "../../../../classmodels/interfaces/ibreadcrumb";
import { labels_app } from '../../../../config_app/labels-app';
import { urls_app } from '../../../../config_app/urls-app';

import { ApiService } from "../../../../services/api/api.service";
import { MessageBoxService } from '../../../../services/message_box_app/message-box.service';
import { PermisosAppService } from '../../../../services/permisos_app/permisos-app.service';
import { BreadcrumbService } from "../../../../services/breadcrumb/breadcrumb.service";
import { RolModel } from '../../../../classmodels/models/rol-model';
import { SeccionAppModel } from '../../../../classmodels/models/seccion-app-model';
import { PermisoAppModel } from '../../../../classmodels/models/permiso-app-model';

@Component({
  selector: "app-form-rol",
  templateUrl: "./form.component.html",
  styleUrls: ["./form.component.sass"],
})
/**
 * class FormComponent{}
 * se encarga de los  procesos de creación y modificación de
 * para la base de datos
 * @authors
 * @author Johan Mauricio Fonseca Molano
 * @author Brayan Hernan Castillo Rodriguez
 */
export class FormComponent implements OnInit {
  labelsApp: any;
  urls: any;

  id: number;
  title: string;
  operacion: string;
  rutaAccion: string;
  itemFormRoles: any;
  itemsPermisosRolDB: any;
  itemsPermisosApp: {id: null, consultar: true, crear: true, modificar: true, eliminar: true, id_rol: null, id_seccion_app: null};
  itemForm: IRol = { id: null, nombre: "" };
  rolModel: RolModel;
  permisoAppModel: PermisoAppModel;
  seccionAppModel: SeccionAppModel;

  constructor(private activatedRoute: ActivatedRoute,private apiService: ApiService,private permisosAppService: PermisosAppService,private breadcrumbService: BreadcrumbService,
    private messageBoxService: MessageBoxService
  ) {

    this.labelsApp = labels_app;
    this.urls = urls_app;
    this.id = this.activatedRoute.snapshot.params.id;
    this.operacion = !this.id ? "crear" : "modificar";
    this.itemFormRoles = { rol: this.itemForm, permisos: [] };

    this.rolModel = new RolModel();
    this.rolModel.setApiService(this.apiService);
    this.rolModel.setMessageBoxService(this.messageBoxService);

    this.permisoAppModel = new PermisoAppModel();
    this.permisoAppModel.setApiService(this.apiService);
    this.permisoAppModel.refresh();

    this.seccionAppModel = new SeccionAppModel();
    this.seccionAppModel.setApiService(this.apiService);
    this.seccionAppModel._refresh(() => {
      for (let i = 0; i < this.seccionAppModel.getItems().length; i++) {
        let seccion = this.seccionAppModel.getItems()[i];
        let permiso = {id: null, consultar: true,crear: true, modificar: true, eliminar: true, id_rol: null, id_seccion_app: seccion.id,nombre: seccion.nombre, submenu: seccion.id_seccion_app == null ? false : true,};
        this.itemFormRoles.permisos.push(permiso);
      }
    });

    this.loadDataForm();

  }

  loadDataForm() {
    if (this.operacion == "modificar") {
      this.title = this.labelsApp.roles.title_editar;
      this.rutaAccion = `${this.urls.roles.modificar}/${this.id}`
      this.rolModel.getItemDetail(this.id, () => {
        this.itemForm = this.rolModel.getItem();
      });
      let idCabezera = this.id;
      this.permisoAppModel._refresh(() => {
        let permisosDB = this.permisoAppModel.getItems();
        this.itemFormRoles.permisos = [];
        this.itemsPermisosRolDB = [];

        for (let i = 0; i < permisosDB.length; i++) {
          var idRolDB = Number(permisosDB[i].id_rol);
          if (idRolDB == idCabezera) {
            this.itemsPermisosRolDB.push(permisosDB[i]);
          }
        }
        for (let i = 0; i < this.seccionAppModel.getItems().length; i++) {
          let seccion = this.seccionAppModel.getItems()[i];
          let permiso = {
            id: this.itemsPermisosRolDB[i].id,
            consultar: this.itemsPermisosRolDB[i].consultar,
            crear: this.itemsPermisosRolDB[i].crear,
            modificar: this.itemsPermisosRolDB[i].modificar,
            eliminar: this.itemsPermisosRolDB[i].eliminar,
            id_rol: idCabezera,
            id_seccion_app: seccion.id,
            nombre: seccion.nombre,
            submenu: seccion.id_seccion_app == null ? false : true,
          };
          this.itemFormRoles.permisos.push(permiso);
          //console.log(this.intemFormRoles.permisos)
        }
      });
  


    } else {
      this.title = this.labelsApp.roles.title_crear;
      this.rutaAccion = this.urls.roles.crear;
    }
  
  }

  ngOnInit() {
    this.initBreadcrumb();
   

  }

  private initBreadcrumb() {
    let rows = this.breadcrumbService.getBreadcrumb();
    if (rows[rows.length - 1].title != this.labelsApp.roles.title_index) {
      let breadcrumb_father: IBreadcrumb = { title: this.labelsApp.roles.title_index, url: this.urls.roles.index };
      this.breadcrumbService.addItemBreadcrumb(breadcrumb_father);
    }
    let breadcrumb: IBreadcrumb = { title: this.title, url: this.rutaAccion, };
    this.breadcrumbService.validationAddItem(breadcrumb);
  }

  async onSubmit() {
  
    this.permisosAppService.onSubmit(this.rolModel, this.itemForm, this.operacion, this.urls.roles.index);
    let idRolNuevo= await this.rolModel.ultimoIdRol()
    let permisos = this.itemFormRoles.permisos;
    for (let i=0; i<permisos.length;i++){
      permisos[i].id_rol = idRolNuevo;
      this.sendOnSubmit(permisos[i], this.operacion);
    }
  }
  sendOnSubmit(data, operacion: string) {
    if (operacion === "crear") {
      this.permisoAppModel.addItem(data,()=>{});
    } else {
      this.permisoAppModel.updateItem(data.id, data,()=>{});
    }
  }
  
  onClickConsultarActivar(permiso) {
    if (
      permiso.crear === false &&
      permiso.modificar === false &&
      permiso.eliminar === false
    ) {
      permiso.consultar = false;
    }
  }
  onClickCrearActivar(permiso) {
    if (permiso.crear === false) {
      permiso.consultar = true;
    }
  }
  onClickModificarActivar(permiso) {
    if (permiso.modificar === false) {
      permiso.consultar = true;
    }
  }
  onClickEliminarActivar(permiso) {
    if (permiso.eliminar === false) {
      permiso.consultar = true;
    }
  }
  onClickConsultarDesactivar(permiso) {
    if (permiso.consultar === true) {
      permiso.crear = false;
      permiso.modificar = false;
      permiso.eliminar = false;
    }
  }
  onClickCrearDesactivar(permiso) {
    if (
      permiso.crear === true &&
      permiso.eliminar === false &&
      permiso.modificar === false
    ) {
      permiso.consultar = false;
    }
  }
  onClickModificarDesactivar(permiso) {
    if (
      permiso.modificar === true &&
      permiso.eliminar === false &&
      permiso.crear === false
    ) {
      permiso.consultar = false;
    }
  }
  onClickEliminarDesactivar(permiso) {
    if (
      permiso.eliminar === true &&
      permiso.crear === false &&
      permiso.modificar === false
    ) {
      permiso.consultar = false;
    }
  }

}


