import { Component, OnInit } from "@angular/core";

import { RolModel } from "../../../classmodels/models/rol-model";

import { IBreadcrumb } from "src/app/classmodels/interfaces/ibreadcrumb";
import { labels_app } from 'src/app/config_app/labels-app';
import { urls_app } from 'src/app/config_app/urls-app';

import { ApiService } from "../../../services/api/api.service";
import { MessageBoxService } from 'src/app/services/message_box_app/message-box.service';
import { PermisosAppService } from 'src/app/services/permisos_app/permisos-app.service';
import { BreadcrumbService } from "../../../services/breadcrumb/breadcrumb.service";
import { PermisoAppModel } from '../../../classmodels/models/permiso-app-model';
import { UsuarioModel } from '../../../classmodels/models/usuario-model';
@Component({
  selector: "app-roles",
  templateUrl: "./roles.component.html",
  styleUrls: ["./roles.component.sass"],
})
/**
 * class RolesComponent{}
 * se encarga de los  procesos de listar, eliminación de
 * para la base de datos
 * además de hacer redireccionamiento a rutas de creación y modificación
 * @authors
 * @author Johan Mauricio Fonseca Molano
 * @author Brayan Hernan Castillo Rodriguez
 */
export class RolesComponent implements OnInit {
  labelsApp: any;
  urls: any;

  permisosAppSeccion: any = [];
  rolModel: RolModel;
  permisoappModel: PermisoAppModel;
  usuarioModel: UsuarioModel;


  constructor(private apiService: ApiService, private permisosAppService: PermisosAppService, private breadcrumbService: BreadcrumbService, private messageBoxService: MessageBoxService) {

    this.labelsApp = labels_app;
    this.urls = urls_app;

    this.rolModel = new RolModel();
    this.rolModel.setApiService(this.apiService);
    this.rolModel.setMessageBoxService(this.messageBoxService);
    this.rolModel.refresh();
    this.permisoappModel = new PermisoAppModel();
    this.permisoappModel.setApiService(this.apiService);
    this.permisoappModel.refresh();
    this.usuarioModel = new UsuarioModel();
    this.usuarioModel.setApiService(this.apiService);
    this.usuarioModel.refresh();

    this.initBreadcrumb();
  }

  async ngOnInit() {
    this.initBreadcrumb();
  }

  private initBreadcrumb() {
    this.breadcrumbService.initBreadcrumb();
    let breadcrumb: IBreadcrumb = {
      title: this.labelsApp.roles.title_index,
      url: this.urls.roles.index,
    };
    this.breadcrumbService.validationAddItem(breadcrumb);
  }

  onClickCrear() {
    this.permisosAppService.onClickCrear(this.urls.roles.crear)
  }

  onClickModificar(model) {
    this.permisosAppService.onClickModificar(this.urls.roles.modificar, model.id)
  }

  onClickEliminar(model) {
    this.permisosAppService.onClickEliminarRol(model.id, this.rolModel, this.urls.roles.index,this.usuarioModel,this.permisoappModel)
  }
}
