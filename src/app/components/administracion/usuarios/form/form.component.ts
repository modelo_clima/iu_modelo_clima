import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";

import { IUsuario } from "../../../../classmodels/interfaces/auth/usuario";
import { UsuarioModel } from "../../../../classmodels/models/usuario-model";

import { IBreadcrumb } from "../../../../classmodels/interfaces/ibreadcrumb";
import { labels_app } from '../../../../config_app/labels-app';
import { urls_app } from '../../../../config_app/urls-app';

import { ApiService } from "../../../../services/api/api.service";
import { MessageBoxService } from '../../../../services/message_box_app/message-box.service';
import { PermisosAppService } from '../../../../services/permisos_app/permisos-app.service';
import { BreadcrumbService } from "../../../../services/breadcrumb/breadcrumb.service";
import { RolModel } from '../../../../classmodels/models/rol-model';
import { EntidadModel } from 'src/app/classmodels/models/entidad-model';

@Component({
  selector: "app-form-entidad",
  templateUrl: "./form.component.html",
  styleUrls: ["./form.component.sass"],
})
/**
 * class FormComponent{}
 * se encarga de los  procesos de creación y modificación de
 * para la base de datos
 * @authors
 * @author Johan Mauricio Fonseca Molano
 * @author Brayan Hernan Castillo Rodriguez
 */
export class FormComponent implements OnInit {
  labelsApp: any;
  urls: any;

  id: number;
  title: string;
  operacion: string;
  rutaAccion: string;
  actualizaPassword: number = 0;
  password: string = "";
  existeUsername:boolean;
  existeEmail:boolean;
  itemForm: IUsuario = { id: null, first_name: "",last_name:"", email:"",username:"",password:"",estado:true,id_entidad:null,id_rol:null};
  usuarioModel: UsuarioModel;
  rolModel: RolModel;
  entidadModel: EntidadModel;


  constructor(private activatedRoute: ActivatedRoute,private apiService: ApiService,private permisosAppService: PermisosAppService,private breadcrumbService: BreadcrumbService,
    private messageBoxService: MessageBoxService
  ) {

    this.labelsApp = labels_app;
    this.urls = urls_app;

    this.usuarioModel = new UsuarioModel();
    this.usuarioModel.setApiService(this.apiService);
    this.usuarioModel.setMessageBoxService(this.messageBoxService);

    this.rolModel = new RolModel();
    this.rolModel.setApiService(this.apiService);
    this.rolModel.refresh();
    
    this.entidadModel = new EntidadModel();
    this.entidadModel.setApiService(this.apiService);
    this.entidadModel.refresh();


    this.id = this.activatedRoute.snapshot.params.id;
    this.operacion = !this.id ? "crear" : "modificar";
    this.loadDataForm();

  }

  loadDataForm() {
    if (this.operacion == "modificar") {
      this.title = this.labelsApp.usuarios.title_editar;
      this.rutaAccion = `${this.urls.usuarios.modificar}/${this.id}`
      this.usuarioModel.getItemDetail(this.id, () => {
        this.itemForm = this.usuarioModel.getItem();
      });
    } else {
      this.title = this.labelsApp.usuarios.title_crear;
      this.rutaAccion = this.urls.usuarios.crear;
    }
  }

  ngOnInit() {
    this.initBreadcrumb();
  }

  private initBreadcrumb() {
    let rows = this.breadcrumbService.getBreadcrumb();
    if (rows[rows.length - 1].title != this.labelsApp.usuarios.title_index) {
      let breadcrumb_father: IBreadcrumb = { title: this.labelsApp.usuarios.title_index, url: this.urls.usuarios.index };
      this.breadcrumbService.addItemBreadcrumb(breadcrumb_father);
    }
    let breadcrumb: IBreadcrumb = { title: this.title, url: this.rutaAccion, };
    this.breadcrumbService.validationAddItem(breadcrumb);
  }

  async onSubmit() {
    await this.ExisteEmail(this.itemForm.email)
    await this.ExisteUsername(this.itemForm.username)


   if(this.existeEmail === false && this.existeUsername=== false){
    if (this.actualizaPassword === 0 && this.id){
      delete this.itemForm["password"];
    }
    this.permisosAppService.onSubmit(this.usuarioModel, this.itemForm, this.operacion, this.urls.usuarios.index);
   }
   if (this.id){
    if (this.actualizaPassword === 0 && this.id){
      delete this.itemForm["password"];
    }
    this.permisosAppService.onSubmit(this.usuarioModel, this.itemForm, this.operacion, this.urls.usuarios.index);
   }
   
  }
  onClickCambiarPassword() {
    if (this.actualizaPassword == 0) {
      this.actualizaPassword = 1;
      this.password = this.itemForm.password;
      this.itemForm.password = "";
    }
  }
  onClickMantenerPassword() {
    if (this.actualizaPassword == 1) {
      if(!this.itemForm.password){
        this.itemForm.password = this.password
      }
      this.itemForm.password = this.password;
      this.actualizaPassword = 0;
      delete this.itemForm["password"];
    }
    this.itemForm.password;
  }
  async ExisteUsername(username:string){
    let respuesta =await this.usuarioModel.existeUsername(username)
    if (respuesta === true){
      this.existeUsername=true;
    }else{
      this.existeUsername=false;
    }
  }
  async ExisteEmail(email:string){
    let respuesta =await this.usuarioModel.existeEmail(email)
    if (respuesta === true){
      this.existeEmail=true;
    }else{
      this.existeEmail=false;
    }
  }
}
