import { Component, OnInit } from "@angular/core";

import { UsuarioModel } from "../../../classmodels/models/usuario-model";

import { IBreadcrumb } from "src/app/classmodels/interfaces/ibreadcrumb";
import { labels_app } from 'src/app/config_app/labels-app';
import { urls_app } from 'src/app/config_app/urls-app';

import { ApiService } from "../../../services/api/api.service";
import { MessageBoxService } from 'src/app/services/message_box_app/message-box.service';
import { PermisosAppService } from 'src/app/services/permisos_app/permisos-app.service';
import { BreadcrumbService } from "../../../services/breadcrumb/breadcrumb.service";
import { EntidadModel } from '../../../classmodels/models/entidad-model';
import { RolModel } from '../../../classmodels/models/rol-model';
@Component({
  selector: "app-usuarios",
  templateUrl: "./usuarios.component.html",
  styleUrls: ["./usuarios.component.sass"],
})
/**
 * class usuariosComponent{}
 * se encarga de los  procesos de listar, eliminación de
 * para la base de datos
 * además de hacer redireccionamiento a rutas de creación y modificación
 * @authors
 * @author Johan Mauricio Fonseca Molano
 * @author Brayan Hernan Castillo Rodriguez
 */
export class UsuariosComponent implements OnInit {
  labelsApp: any;
  urls: any;
  itemsRol: any = [];
  nombreEstadoTrue: string = "Activo";
  nombreEstadoFalse: string = "Inactivo";
  
  entidadModel: EntidadModel;
  rolModel: RolModel;

  permisosAppSeccion: any = [];
  usuarioModel: UsuarioModel;


  constructor(private apiService: ApiService, private permisosAppService: PermisosAppService, private breadcrumbService: BreadcrumbService, private messageBoxService: MessageBoxService) {

    this.labelsApp = labels_app;
    this.urls = urls_app;

    this.usuarioModel = new UsuarioModel();
    this.usuarioModel.setApiService(this.apiService);
    this.usuarioModel.setMessageBoxService(this.messageBoxService);
    this.usuarioModel.refresh();
    this.rolModel = new RolModel();
    this.rolModel.setApiService(this.apiService);
    this.rolModel.refresh();
    this.entidadModel = new EntidadModel();
    this.entidadModel.setApiService(this.apiService);
    this.entidadModel.refresh();


    this.initBreadcrumb();
  }

  async ngOnInit() {
    this.initBreadcrumb();
     
  }

  private initBreadcrumb() {
    this.breadcrumbService.initBreadcrumb();
    let breadcrumb: IBreadcrumb = {
      title: this.labelsApp.usuarios.title_index,
      url: this.urls.usuarios.index,
    };
    this.breadcrumbService.validationAddItem(breadcrumb);
  }

  onClickCrear() {
    this.permisosAppService.onClickCrear(this.urls.usuarios.crear)
  }

  onClickModificar(model) {
    this.permisosAppService.onClickModificar(this.urls.usuarios.modificar, model.id)
  }

  onClickEliminar(model) {
    this.permisosAppService.onClickEliminar(model.id, this.usuarioModel, this.urls.usuarios.index)
  }
  nombreEstado(estado: boolean) {
    if (estado === true) {
      return this.nombreEstadoTrue;
    } else {
      return this.nombreEstadoFalse;
    }
  }
  nombreRol(id_rol: number) {
    let items = this.rolModel.getItems();

    for (let i = 0; i < items.length; i++) {
      if (id_rol === items[i]["id"]) {
        return items[i]["nombre"];
      }
    }
  }
  nombreEntidad(id_entidad: number) {
    let items = this.entidadModel.getItems();

    for (let i = 0; i < items.length; i++) {
      if (id_entidad === items[i]["id"]) {
        return items[i]["nombre"];
      }
    }
  }

}
