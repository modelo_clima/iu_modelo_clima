import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { JwtResponseI } from "../../../../classmodels/interfaces/auth/jwt-response";
import { NgForm } from "@angular/forms";
import { AutenticacionService } from "../../../../services/auth/autenticacion.service";
@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.sass"],
})
/**
 * class LoginComponent{}
 * se encarga de hacer proceso de logueo de usuarios
 * para la base de datos
 * @author Brayan Hernan Castillo Rodriguez
 */
export class LoginComponent {

  public isError = false;
  private user: JwtResponseI = {
    username: "",
    password: "",
  };

  constructor(private autenticacionService: AutenticacionService, private router: Router) { }



  /**
   * onLogin(form: NgForm)
   * @author Brayan Hernan Castillo Rodriguez
   * se encargar del validar el formulario y dar acceso al usuario siempre y cuando
   * halla ingresado credenciales correctas
   **/

  onLogin(form: NgForm) {
    if (form.valid) {
      this.autenticacionService.loginuser(this.user.username, this.user.password).subscribe((data) => {
        if (!data.error) {
          let username = data.username;
          let token = data.token;
          this.autenticacionService.setUser( data.name);
          this.autenticacionService.setToken(token);
          localStorage.setItem("accessToken", token);
          this.autenticacionService.menu = data.menu;
          localStorage.setItem("menu_user", JSON.stringify(data.menu));
          localStorage.setItem("statusLogged", "1");
          //this.router.navigate(["/inicio"]);
          location.replace("/inicio"); // pagina a redireccionar luego de hacer login
          this.isError = false;
        } else {
          alert(data.error);
        }
      },
        (erro) => {
          console.log(erro);
          //console.log(erro.error.non_field_errors[0])
          alert("Usuario o Contraseña incorrecta");
          //location.replace("/auth/login");
        }
      );
    } else {
      this.onIsError();
    }
  }

  onIsError(): void {
    this.isError = true;
    setTimeout(() => {
      this.isError = false;
    }, 4200);
  }

  /**
   * onClickOlvidarPass()
   * @author Brayan Hernan Castillo Rodriguez
   * se envia un mensaje de alerta al usuario
   * cuando olvida contraseña
   */
  onClickOlvidarPass() {
    alert(
      "Si olvidaste tú contraseña o usuario, puedes contactar con soporte técnico al correo xxxxxx@uniboyaca.edu.co, o puede llamar a nuestra linea de atención al cliente 3xxxxxxxxx "
    );
  }
}
