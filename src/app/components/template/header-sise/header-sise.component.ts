import { Component, OnInit } from "@angular/core";
import * as jQuery from "jquery";
import { ApiService } from "../../../services/api/api.service";
import { isNullOrUndefined } from "util";
import { AutenticacionService } from '../../../services/auth/autenticacion.service';

function clickMenu() {
  jQuery("div#menu").css({ display: "block" });
}

@Component({
  selector: "app-header-sise",
  templateUrl: "./header-sise.component.html",
  styleUrls: ["./header-sise.component.sass"],
})
export class HeaderSiseComponent implements OnInit {

  username: string;

  constructor(public apiService: ApiService, public autenticacionService: AutenticacionService) {
    this.username = this.apiService.getUsername();
  }

  ngOnInit() { }

  onClickMenu() {
    clickMenu();
  }
}
