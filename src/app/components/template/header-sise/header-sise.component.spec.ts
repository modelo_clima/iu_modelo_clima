import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderSiseComponent } from './header-sise.component';

describe('HeaderSiseComponent', () => {
  let component: HeaderSiseComponent;
  let fixture: ComponentFixture<HeaderSiseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderSiseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderSiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
