import { Component, OnInit } from '@angular/core';
import { AutenticacionService } from 'src/app/services/auth/autenticacion.service';

@Component({
  selector: 'app-footer-sise',
  templateUrl: './footer-sise.component.html',
  styleUrls: ['./footer-sise.component.sass']
})
export class FooterSiseComponent implements OnInit {

  constructor(private autenticacionService: AutenticacionService) { }

  ngOnInit() {
  }

}
