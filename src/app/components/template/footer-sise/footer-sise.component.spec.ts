import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterSiseComponent } from './footer-sise.component';

describe('FooterSiseComponent', () => {
  let component: FooterSiseComponent;
  let fixture: ComponentFixture<FooterSiseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FooterSiseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterSiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
