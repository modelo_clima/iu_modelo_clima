import { Component, OnInit, Input } from '@angular/core';
import { labels_app } from 'src/app/config_app/labels-app';

@Component({
  selector: 'app-alertas-sise',
  templateUrl: './alertas-sise.component.html',
  styleUrls: ['./alertas-sise.component.sass']
})
export class AlertasSiseComponent implements OnInit {

  labelsApp: any;
  
  @Input()
  alert_sise: any;

  constructor() {
    this.labelsApp = labels_app;
  }

  ngOnInit() {
  }

}
