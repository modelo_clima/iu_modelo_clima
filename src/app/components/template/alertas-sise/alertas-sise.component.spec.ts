import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlertasSiseComponent } from './alertas-sise.component';

describe('AlertasSiseComponent', () => {
  let component: AlertasSiseComponent;
  let fixture: ComponentFixture<AlertasSiseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlertasSiseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlertasSiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
