import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class MessageBoxSise {

    alert_sise: any;

    constructor() {
        this.alert_sise = {
            message: '',
            type: '',
            show: false,
            onClickCerrar: () => { },
            onClickNo: () => { },
            onClickSi: () => { }
        };
        this.alert_sise.onClickCerrar = () => {
            this.alert_sise.show = false;
        }
        this.alert_sise.onClickNo = () => {
            this.alert_sise.show = false;
        }
    }
    closeMessage() {
        this.alert_sise.show = false;
    }

    showMessage(type_msg, message_text, onClickCerrar?: () => void) {
        this.alert_sise.message = message_text;
        this.alert_sise.type = type_msg;
        this.alert_sise.show = true;
        this.alert_sise.onClickCerrar = () => {
            if (onClickCerrar) {
                onClickCerrar();
            } else {
                this.closeMessage();
            }
        }
    }

    showMessageConf(message_text, onClickSi: () => void, onClickNo?: () => void, onClickCerrar?: () => void) {
        this.alert_sise.message = message_text;
        this.alert_sise.type = 'conf';
        this.alert_sise.show = true;
        this.alert_sise.onClickSi = () => {
            onClickSi();
        }
        this.alert_sise.onClickNo = () => {
            if (onClickNo) {
                onClickNo();
            } else {
                this.closeMessage();
            }
        }
        this.alert_sise.onClickCerrar = () => {
            if (onClickCerrar) {
                onClickCerrar();
            } else {
                this.closeMessage();
            }
        }
    }
}