import {
  Component,
  OnInit,
  AfterContentChecked,
  DoCheck,
  AfterContentInit,
} from "@angular/core";
import * as jQuery from "jquery";
import { BreadcrumbService } from "src/app/services/breadcrumb/breadcrumb.service";
import { ApiService } from "../../../services/api/api.service";
import { UsuarioModel } from "../../../classmodels/models/usuario-model";
import { PermisoAppModel } from "../../../classmodels/models/permiso-app-model";
// import { AuthService } from "../../../services/auth/_auth.service";
import { AutenticacionService } from "../../../services/auth/autenticacion.service";
import { PermisosService } from '../../../services/permisos/permisos.service';
import { isEmpty } from 'rxjs/operators';

function estilosMenu() {
  var h = jQuery(window).height();
  jQuery("div#menu").css({ "min-height": h + "px" });
  jQuery("div#content-menu-sise").css({ "max-height": h - 115 + "px" });
}

@Component({
  selector: "app-menu-sise",
  templateUrl: "./menu-sise.component.html",
  styleUrls: ["./menu-sise.component.sass"],
})
export class MenuSiseComponent implements OnInit {

  constructor(private breadcrumbService: BreadcrumbService, private apiService: ApiService, private permisosService: PermisosService, private autenticacionService: AutenticacionService) {
    this.autenticacionService.loadMenu();
  }
  ngOnInit() {
    estilosMenu();
  }

  onClickMenuOpen(item) {
    item.ver = !item.ver;
  }

  onClickItemMenu(item) {
    this.breadcrumbService.initBreadcrumb();
  }
}
