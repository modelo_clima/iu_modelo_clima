import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuSiseComponent } from './menu-sise.component';

describe('MenuSiseComponent', () => {
  let component: MenuSiseComponent;
  let fixture: ComponentFixture<MenuSiseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuSiseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuSiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
