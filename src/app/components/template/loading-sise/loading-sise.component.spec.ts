import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadingSiseComponent } from './loading-sise.component';

describe('LoadingSiseComponent', () => {
  let component: LoadingSiseComponent;
  let fixture: ComponentFixture<LoadingSiseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoadingSiseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadingSiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
