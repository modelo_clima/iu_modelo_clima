import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-loading-sise',
  templateUrl: './loading-sise.component.html',
  styleUrls: ['./loading-sise.component.sass']
})
export class LoadingSiseComponent implements OnInit {

  @Input()
  loadingShow: boolean

  constructor() { }

  ngOnInit() {
  }

}
