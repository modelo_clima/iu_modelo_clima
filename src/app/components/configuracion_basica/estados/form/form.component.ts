import { ApiService } from "../../../../services/api/api.service";
import { Component, OnInit } from "@angular/core";
import { IEstado } from "../../../../classmodels/interfaces/iestado";
import { EstadoModel } from "../../../../classmodels/models/estado-model";
import { ActivatedRoute, Router } from "@angular/router";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { IBreadcrumb } from "src/app/classmodels/interfaces/ibreadcrumb";
import { BreadcrumbService } from "src/app/services/breadcrumb/breadcrumb.service";
import { PermisoAppModel } from "../../../../classmodels/models/permiso-app-model";
import { UsuarioModel } from "../../../../classmodels/models/usuario-model";
import { PermisosService } from 'src/app/services/permisos/permisos.service';
@Component({
  selector: "app-form-estado",
  templateUrl: "./form.component.html",
  styleUrls: ["./form.component.sass"]
})
/**
   * class FormComponent{}
   * se encarga de los  procesos de creación y modificación de  
   * para la base de datos 
   * @authors
   * @author Johan Mauricio Fonseca Molano
   * @author Brayan Hernan Castillo Rodriguez
   */
export class FormComponent implements OnInit {
  id: number;
  title: string;
  nombreIndex: string;
  nombreForm: string;
  operacion: string;
  rutaPadre: string;
  rutaHija: string;
  rutaAccion: string;
  isForm: boolean = true;
  permisosAppSeccion: any = [];
  estadoModel: EstadoModel;
  itemForm: IEstado = { id: null, nombre: "" };

  constructor(
    private activatedRoute: ActivatedRoute,
    public apiService: ApiService,
    public permisosService: PermisosService,
    public breadcrumbService: BreadcrumbService
  ) {
    this.id = this.activatedRoute.snapshot.params.id;
    this.operacion = !this.id ? "crear" : "modificar";
    this.estadoModel = new EstadoModel();
    this.estadoModel.setApiService(this.apiService);
    if (this.id) {
      this.rutaAccion = `/${this.operacion}/${this.id}`;
    } else {
      this.rutaAccion = `/${this.operacion}`;
    }
  }

  async ngOnInit() {
    this.permisosAppSeccion = await this.permisosService.getPermisosSeccionApp(
      this.isForm,
      this.rutaAccion
    );
    this.nombreIndex = this.permisosAppSeccion["nombre"];
    this.nombreForm = this.permisosAppSeccion["nombreForm"];
    this.rutaPadre = this.permisosAppSeccion["url"];
    this.rutaHija = this.permisosAppSeccion["urlHija"];
    this.title = `${this.operacion} ${this.nombreForm}`;

    if (this.id) {
      this.title = `${this.operacion} ${this.nombreForm}`;
      this.estadoModel.getItemDetail(this.id, () => {
        this.itemForm = this.estadoModel.getItem();
      });
    }
    this.initBreadcrumb();
  }

  private initBreadcrumb() {
    let rows = this.breadcrumbService.getBreadcrumb();
    if (rows[rows.length - 1].title != `${this.nombreIndex}`) {
      let breadcrumb_father: IBreadcrumb = {
        title: this.nombreIndex,
        url: this.rutaPadre,
      };
      this.breadcrumbService.addItemBreadcrumb(breadcrumb_father);
    }
    let breadcrumb: IBreadcrumb = {
      title: this.title,
      url: `${this.nombreForm}/${this.operacion}/`,
    };
    this.breadcrumbService.validationAddItem(breadcrumb);
  }
}
