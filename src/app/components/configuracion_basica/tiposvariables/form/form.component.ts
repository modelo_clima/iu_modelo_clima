import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";

import { ITipoVariable } from "../../../../classmodels/interfaces/itipo-variable";
import { TipoVariableModel } from "../../../../classmodels/models/tipo-variable-model";

import { IBreadcrumb } from "../../../../classmodels/interfaces/ibreadcrumb";
import { labels_app } from '../../../../config_app/labels-app';
import { urls_app } from '../../../../config_app/urls-app';

import { ApiService } from "../../../../services/api/api.service";
import { MessageBoxService } from '../../../../services/message_box_app/message-box.service';
import { PermisosAppService } from '../../../../services/permisos_app/permisos-app.service';
import { BreadcrumbService } from "../../../../services/breadcrumb/breadcrumb.service";

@Component({
  selector: "app-form-tipo-variable",
  templateUrl: "./form.component.html",
  styleUrls: ["./form.component.sass"],
})
/**
 * class FormComponent{}
 * se encarga de los  procesos de creación y modificación de
 * para la base de datos
 * @authors
 * @author Johan Mauricio Fonseca Molano
 * @author Brayan Hernan Castillo Rodriguez
 */
export class FormComponent implements OnInit {
  labelsApp: any;
  urls: any;

  id: number;
  title: string;
  operacion: string;
  rutaAccion: string;
  itemForm: ITipoVariable = { id: null, nombre: "" };
  tipoVariableModel: TipoVariableModel;

  constructor(private activatedRoute: ActivatedRoute,private apiService: ApiService,private permisosAppService: PermisosAppService,private breadcrumbService: BreadcrumbService,
    private messageBoxService: MessageBoxService
  ) {

    this.labelsApp = labels_app;
    this.urls = urls_app;

    this.tipoVariableModel = new TipoVariableModel();
    this.tipoVariableModel.setApiService(this.apiService);
    this.tipoVariableModel.setMessageBoxService(this.messageBoxService);

    this.id = this.activatedRoute.snapshot.params.id;
    this.operacion = !this.id ? "crear" : "modificar";
    this.loadDataForm();

  }

  loadDataForm() {
    if (this.operacion == "modificar") {
      this.title = this.labelsApp.tipos_variables.title_editar;
      this.rutaAccion = `${this.urls.tipos_variables.modificar}/${this.id}`
      this.tipoVariableModel.getItemDetail(this.id, () => {
        this.itemForm = this.tipoVariableModel.getItem();
      });
    } else {
      this.title = this.labelsApp.tipos_variables.title_crear;
      this.rutaAccion = this.urls.tipos_variables.crear;
    }
  }

  ngOnInit() {
    this.initBreadcrumb();
  }

  private initBreadcrumb() {
    let rows = this.breadcrumbService.getBreadcrumb();
    if (rows[rows.length - 1].title != this.labelsApp.tipos_variables.title_index) {
      let breadcrumb_father: IBreadcrumb = { title: this.labelsApp.tipos_variables.title_index, url: this.urls.tipos_variables.index };
      this.breadcrumbService.addItemBreadcrumb(breadcrumb_father);
    }
    let breadcrumb: IBreadcrumb = { title: this.title, url: this.rutaAccion, };
    this.breadcrumbService.validationAddItem(breadcrumb);
  }

  onSubmit() {
    this.permisosAppService.onSubmit(this.tipoVariableModel, this.itemForm, this.operacion, this.urls.tipos_variables.index);
  }
}
