import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TiposVariablesComponent } from './tiposvariables.component';

describe('TiposVariablesComponent', () => {
  let component: TiposVariablesComponent;
  let fixture: ComponentFixture<TiposVariablesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TiposVariablesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TiposVariablesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
