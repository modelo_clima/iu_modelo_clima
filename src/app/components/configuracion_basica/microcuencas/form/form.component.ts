import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";

import { IMicrocuenca } from "../../../../classmodels/interfaces/imicrocuenca";
import { MicrocuencaModel } from "../../../../classmodels/models/microcuenca-model";

import { IBreadcrumb } from "../../../../classmodels/interfaces/ibreadcrumb";
import { labels_app } from '../../../../config_app/labels-app';
import { urls_app } from '../../../../config_app/urls-app';

import { ApiService } from "../../../../services/api/api.service";
import { MessageBoxService } from '../../../../services/message_box_app/message-box.service';
import { PermisosAppService } from '../../../../services/permisos_app/permisos-app.service';
import { BreadcrumbService } from "../../../../services/breadcrumb/breadcrumb.service";

@Component({
  selector: "app-form-modelo",
  templateUrl: "./form.component.html",
  styleUrls: ["./form.component.sass"],
})
/**
 * class FormComponent{}
 * se encarga de los  procesos de creación y modificación de
 * para la base de datos
 * @authors
 * @author Johan Mauricio Fonseca Molano
 * @author Brayan Hernan Castillo Rodriguez
 */
export class FormComponent implements OnInit {
  labelsApp: any;
  urls: any;

  id: number;
  title: string;
  operacion: string;
  rutaAccion: string;
  itemForm: IMicrocuenca = { id: null, nombre: "" };
  microcuencaModel: MicrocuencaModel;

  constructor(private activatedRoute: ActivatedRoute,private apiService: ApiService,private permisosAppService: PermisosAppService,private breadcrumbService: BreadcrumbService,
    private messageBoxService: MessageBoxService
  ) {

    this.labelsApp = labels_app;
    this.urls = urls_app;

    this.microcuencaModel = new MicrocuencaModel();
    this.microcuencaModel.setApiService(this.apiService);
    this.microcuencaModel.setMessageBoxService(this.messageBoxService);

    this.id = this.activatedRoute.snapshot.params.id;
    this.operacion = !this.id ? "crear" : "modificar";
    this.loadDataForm();

  }

  loadDataForm() {
    if (this.operacion == "modificar") {
      this.title = this.labelsApp.microcuencas.title_editar;
      this.rutaAccion = `${this.urls.microcuencas.modificar}/${this.id}`
      this.microcuencaModel.getItemDetail(this.id, () => {
        this.itemForm = this.microcuencaModel.getItem();
      });
    } else {
      this.title = this.labelsApp.microcuencas.title_crear;
      this.rutaAccion = this.urls.microcuencas.crear;
    }
  }

  ngOnInit() {
    this.initBreadcrumb();
  }

  private initBreadcrumb() {
    let rows = this.breadcrumbService.getBreadcrumb();
    if (rows[rows.length - 1].title != this.labelsApp.microcuencas.title_index) {
      let breadcrumb_father: IBreadcrumb = { title: this.labelsApp.microcuencas.title_index, url: this.urls.microcuencas.index };
      this.breadcrumbService.addItemBreadcrumb(breadcrumb_father);
    }
    let breadcrumb: IBreadcrumb = { title: this.title, url: this.rutaAccion, };
    this.breadcrumbService.validationAddItem(breadcrumb);
  }

  onSubmit() {
    this.permisosAppService.onSubmit(this.microcuencaModel, this.itemForm, this.operacion, this.urls.microcuencas.index);
  }
}
