import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MicrocuencasComponent } from './microcuencas.component';

describe('MicrocuencasComponent', () => {
  let component: MicrocuencasComponent;
  let fixture: ComponentFixture<MicrocuencasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MicrocuencasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MicrocuencasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
