import { Component, OnInit } from "@angular/core";

import { MicrocuencaModel } from "../../../classmodels/models/microcuenca-model";

import { IBreadcrumb } from "src/app/classmodels/interfaces/ibreadcrumb";
import { labels_app } from 'src/app/config_app/labels-app';
import { urls_app } from 'src/app/config_app/urls-app';

import { ApiService } from "../../../services/api/api.service";
import { MessageBoxService } from 'src/app/services/message_box_app/message-box.service';
import { PermisosAppService } from 'src/app/services/permisos_app/permisos-app.service';
import { BreadcrumbService } from "../../../services/breadcrumb/breadcrumb.service";
@Component({
  selector: "app-microcuencas",
  templateUrl: "./microcuencas.component.html",
  styleUrls: ["./microcuencas.component.sass"],
})
/**
 * class MicrocuencasComponent{}
 * se encarga de los  procesos de listar, eliminación de
 * para la base de datos
 * además de hacer redireccionamiento a rutas de creación y modificación
 * @authors
 * @author Johan Mauricio Fonseca Molano
 * @author Brayan Hernan Castillo Rodriguez
 */
export class MicrocuencasComponent implements OnInit {
  labelsApp: any;
  urls: any;

  permisosAppSeccion: any = [];
  microcuencaModel: MicrocuencaModel;


  constructor(private apiService: ApiService, private permisosAppService: PermisosAppService, private breadcrumbService: BreadcrumbService, private messageBoxService: MessageBoxService) {

    this.labelsApp = labels_app;
    this.urls = urls_app;

    this.microcuencaModel = new MicrocuencaModel();
    this.microcuencaModel.setApiService(this.apiService);
    this.microcuencaModel.setMessageBoxService(this.messageBoxService);
    this.microcuencaModel.refresh();
    this.initBreadcrumb();
  }

  async ngOnInit() {
    this.initBreadcrumb();
  }

  private initBreadcrumb() {
    this.breadcrumbService.initBreadcrumb();
    let breadcrumb: IBreadcrumb = {
      title: this.labelsApp.microcuencas.title_index,
      url: this.urls.microcuencas.index,
    };
    this.breadcrumbService.validationAddItem(breadcrumb);
  }

  onClickCrear() {
    this.permisosAppService.onClickCrear(this.urls.microcuencas.crear)
  }

  onClickModificar(model) {
    this.permisosAppService.onClickModificar(this.urls.microcuencas.modificar, model.id)
  }

  onClickEliminar(model) {
    this.permisosAppService.onClickEliminar(model.id, this.microcuencaModel, this.urls.microcuencas.index)
  }
}