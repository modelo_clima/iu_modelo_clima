import { Component, OnInit } from "@angular/core";
import { IBreadcrumb } from "src/app/classmodels/interfaces/ibreadcrumb";
import { labels_app } from 'src/app/config_app/labels-app';
import { urls_app } from 'src/app/config_app/urls-app';

import { ApiService } from "../../../services/api/api.service";
import { MessageBoxService } from 'src/app/services/message_box_app/message-box.service';
import { PermisosAppService } from 'src/app/services/permisos_app/permisos-app.service';
import { BreadcrumbService } from "../../../services/breadcrumb/breadcrumb.service";

import { EstacionModel } from "../../../classmodels/models/estacion-model";
import { CategoriaEstacionModel } from '../../../classmodels/models/categoria-estacion-model';
import { EntidadModel } from '../../../classmodels/models/entidad-model';
import { MunicipioModel } from 'src/app/classmodels/models/municipio-model';



@Component({
  selector: "app-estaciones",
  templateUrl: "./estaciones.component.html",
  styleUrls: ["./estaciones.component.sass"],
})
/**
 * class EstacionesComponent{}
 * se encarga de los  procesos de listar, eliminación de
 * para la base de datos
 * además de hacer redireccionamiento a rutas de creación y modificación
 * @authors
 * @author Johan Mauricio Fonseca Molano
 * @author Brayan Hernan Castillo Rodriguez
 * @author Federico Estupiñan Carreño
 */
export class EstacionesComponent  implements OnInit {
 
  labelsApp: any;
  urls: any;
  itemsRol: any = [];
  nombreEstadoTrue: string = "Activo";
  nombreEstadoFalse: string = "Inactivo";

  estacionModel: EstacionModel;
  categoriaEstacionModel: CategoriaEstacionModel;
  entidadModel: EntidadModel;
  municipioModel: MunicipioModel;

  permisosAppSeccion: any = [];


  constructor(private apiService: ApiService, private permisosAppService: PermisosAppService, private breadcrumbService: BreadcrumbService, private messageBoxService: MessageBoxService) {

    this.labelsApp = labels_app;
    this.urls = urls_app;

    this.estacionModel = new EstacionModel();
    this.estacionModel.setApiService(this.apiService);
    this.estacionModel.setMessageBoxService(this.messageBoxService);
    this.estacionModel.refresh();
    

    this.categoriaEstacionModel = new CategoriaEstacionModel();
    this.categoriaEstacionModel.setApiService(this.apiService);
    this.categoriaEstacionModel.refresh();

    this.entidadModel = new EntidadModel();
    this.entidadModel.setApiService(this.apiService);
    this.entidadModel.refresh();

    this.municipioModel = new MunicipioModel();
    this.municipioModel.setApiService(this.apiService);
    this.municipioModel.refresh();
   
    this.initBreadcrumb();;
  }

  async ngOnInit() {
    this.initBreadcrumb();
  }


private initBreadcrumb() {
    this.breadcrumbService.initBreadcrumb();
    let breadcrumb: IBreadcrumb = {
      title: this.labelsApp.estaciones.title_index,
      url: this.urls.estaciones.index,
    };
    this.breadcrumbService.validationAddItem(breadcrumb);
  }

  onClickCrear() {
    this.permisosAppService.onClickCrear(this.urls.estaciones.crear)
  }

  onClickModificar(model) {
    this.permisosAppService.onClickModificar(this.urls.estaciones.modificar, model.id)
  }

  onClickEliminar(model) {
    this.permisosAppService.onClickEliminar(model.id, this.estacionModel, this.urls.estaciones.index)
  }

  nombreCategoriaEstacion(id_categoria_estaciones: number) {
    let items = this.categoriaEstacionModel.getItems();

    for (let i = 0; i < items.length; i++) {
      if (id_categoria_estaciones === items[i]["id"]) {
        return items[i]["nombre"];
      }
    }
  }

  nombreEntidad(id_entidad: number) {
    let items = this.entidadModel.getItems();

    for (let i = 0; i < items.length; i++) {
      if (id_entidad === items[i]["id"]) {
        return items[i]["nombre"];
      }
    }
  }

  nombreM(id_muncipio: number) {
    let items = this.municipioModel.getItems();

    for (let i = 0; i < items.length; i++) {
      if (id_muncipio === items[i]["id"]) {
        return items[i]["nombre"];
      }
    }
  }






}
