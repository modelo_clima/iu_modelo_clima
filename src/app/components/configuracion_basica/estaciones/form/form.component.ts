import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";

import { IBreadcrumb } from "../../../../classmodels/interfaces/ibreadcrumb";
import { labels_app } from '../../../../config_app/labels-app';
import { urls_app } from '../../../../config_app/urls-app';

import { ApiService } from "../../../../services/api/api.service";
import { MessageBoxService } from '../../../../services/message_box_app/message-box.service';
import { PermisosAppService } from '../../../../services/permisos_app/permisos-app.service';
import { BreadcrumbService } from "../../../../services/breadcrumb/breadcrumb.service";

import { IEstacion } from "../../../../classmodels/interfaces/iestacion";
import { EstacionModel } from "../../../../classmodels/models/estacion-model";
import { EntidadModel } from "../../../../classmodels/models/entidad-model";
import { CategoriaEstacionModel } from "../../../../classmodels/models/categoria-estacion-model";
import { TecnologiaEstacionModel } from "../../../../classmodels/models/tecnologia-estacion-model";
import { MunicipioModel } from 'src/app/classmodels/models/municipio-model';





@Component({
  selector: "app-form-estacion",
  templateUrl: "./form.component.html",
  styleUrls: ["./form.component.sass"]
})
/**
   * class FormComponent{}
   * se encarga de los  procesos de creación y modificación de  
   * para la base de datos 
   * @authors
   * @author Johan Mauricio Fonseca Molano
   * @author Brayan Hernan Castillo Rodriguez
   * @author Federico Estupiñan Carreño
   */
export class FormComponent implements OnInit {
  labelsApp: any;
  urls: any;

  id: number;
  title: string;
  operacion: string;
  rutaAccion: string;
  estacionModel: EstacionModel;
  entidadModel: EntidadModel;  
  categoriaEstacionModel: CategoriaEstacionModel;
  tecnologiaEstacionModel: TecnologiaEstacionModel;
  municipioModel: MunicipioModel;

  itemForm: IEstacion = {
    id: null,
    nombre: "",
    codigo: "",
    latitud: "",
    longitud: "",
    altitud: "",
    estado: 0,
    fecha_instalacion: "",
    fecha_suspension: "",
    fecha_inicial_datos: null,
    fecha_final_datos: null,
    id_entidad: null,
    id_categoria_estaciones: null,
    id_tecnologia: null,
    id_municipio: null
  };

  constructor(private activatedRoute: ActivatedRoute,private apiService: ApiService,private permisosAppService: PermisosAppService,private breadcrumbService: BreadcrumbService,
    private messageBoxService: MessageBoxService
  ) {

    this.labelsApp = labels_app;
    this.urls = urls_app;

    this.estacionModel = new EstacionModel();
    this.estacionModel.setApiService(this.apiService);
    this.estacionModel.setMessageBoxService(this.messageBoxService);

    this.entidadModel = new EntidadModel();
    this.entidadModel.setApiService(this.apiService);
    this.entidadModel.refresh();    

    this.municipioModel = new MunicipioModel();
    this.municipioModel.setApiService(this.apiService);
    this.municipioModel.refresh();

    this.tecnologiaEstacionModel = new TecnologiaEstacionModel();
    this.tecnologiaEstacionModel.setApiService(this.apiService);
    this.tecnologiaEstacionModel.refresh();

    this.categoriaEstacionModel = new CategoriaEstacionModel();
    this.categoriaEstacionModel.setApiService(this.apiService);
    this.categoriaEstacionModel.refresh(); 

    this.id = this.activatedRoute.snapshot.params.id;
    this.operacion = !this.id ? "crear" : "modificar";
    this.loadDataForm();

  }

  loadDataForm() {
    if (this.operacion == "modificar") {
      this.title = this.labelsApp.estaciones.title_editar;
      this.rutaAccion = `${this.urls.estaciones.modificar}/${this.id}`
      this.estacionModel.getItemDetail(this.id, () => {
        this.itemForm = this.estacionModel.getItem();
      });
    } else {
      this.title = this.labelsApp.estaciones.title_crear;
      this.rutaAccion = this.urls.estaciones.crear;
    }
  }

  ngOnInit() {
    this.initBreadcrumb();
  }

  private initBreadcrumb() {
    let rows = this.breadcrumbService.getBreadcrumb();
    if (rows[rows.length - 1].title != this.labelsApp.estaciones.title_index) {
      let breadcrumb_father: IBreadcrumb = { title: this.labelsApp.estaciones.title_index, url: this.urls.estaciones.index };
      this.breadcrumbService.addItemBreadcrumb(breadcrumb_father);
    }
    let breadcrumb: IBreadcrumb = { title: this.title, url: this.rutaAccion, };
    this.breadcrumbService.validationAddItem(breadcrumb);
  }

  onSubmit() {
    this.permisosAppService.onSubmit(this.estacionModel, this.itemForm, this.operacion, this.urls.estaciones.index);
  }



}
