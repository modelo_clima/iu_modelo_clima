import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";


import { IBreadcrumb } from "../../../../classmodels/interfaces/ibreadcrumb";
import { labels_app } from '../../../../config_app/labels-app';
import { urls_app } from '../../../../config_app/urls-app';

import { ApiService } from "../../../../services/api/api.service";
import { MessageBoxService } from '../../../../services/message_box_app/message-box.service';
import { PermisosAppService } from '../../../../services/permisos_app/permisos-app.service';
import { BreadcrumbService } from "../../../../services/breadcrumb/breadcrumb.service";

import { ILimite } from 'src/app/classmodels/interfaces/ilimite';
import { LimiteModel } from "src/app/classmodels/models/limite-model";
import { PuntoGestionModel } from 'src/app/classmodels/models/punto-gestion-model';
import { VariableModel } from "src/app/classmodels/models/variable-model";
import { TipoUsoModel } from "src/app/classmodels/models/tipo-uso-model";


@Component({
  selector: "app-form-limite",
  templateUrl: "./form.component.html",
  styleUrls: ["./form.component.sass"],
})
/**
 * class FormComponent{}
 * se encarga de los  procesos de creación y modificación de
 * para la base de datos
 * @authors
 * @author Johan Mauricio Fonseca Molano
 * @author Brayan Hernan Castillo Rodriguez
 * @author Federico Estupiñan Carreño
 */
export class FormComponent implements OnInit {
  labelsApp: any;
  urls: any;

  id: number;
  title: string;
  operacion: string;
  rutaAccion: string;
  itemForm: ILimite = {id: null, nombre: "", valor: 0, constante:null, id_punto_gestion: null, id_variable: null, id_tipo_uso: null };
  limiteModel: LimiteModel;
  puntoGestionModel:PuntoGestionModel;
  variableModel: VariableModel;
  tipoUsoModel: TipoUsoModel;


  constructor(private activatedRoute: ActivatedRoute,private apiService: ApiService,private permisosAppService: PermisosAppService,private breadcrumbService: BreadcrumbService,
    private messageBoxService: MessageBoxService
  ) {

    this.labelsApp = labels_app;
    this.urls = urls_app;

    this.limiteModel = new LimiteModel();
    this.limiteModel.setApiService(this.apiService);
    this.limiteModel.setMessageBoxService(this.messageBoxService);
    
    this.puntoGestionModel = new PuntoGestionModel();
    this.puntoGestionModel.setApiService(this.apiService);
    this.puntoGestionModel.refresh();

    this.variableModel = new VariableModel();
    this.variableModel.setApiService(this.apiService);
    this.variableModel.refresh();

    this.tipoUsoModel = new TipoUsoModel();
    this.tipoUsoModel.setApiService(this.apiService);
    this.tipoUsoModel.refresh();

    this.id = this.activatedRoute.snapshot.params.id;
    this.operacion = !this.id ? "crear" : "modificar";
    this.loadDataForm();

  }

  loadDataForm() {
    if (this.operacion == "modificar") {
      this.title = this.labelsApp.limites.title_editar;
      this.rutaAccion = `${this.urls.limites.modificar}/${this.id}`
      this.limiteModel.getItemDetail(this.id, () => {
        this.itemForm = this.limiteModel.getItem();
      });
    } else {
      this.title = this.labelsApp.limites.title_crear;
      this.rutaAccion = this.urls.limites.crear;
    }
  }

  ngOnInit() {
    this.initBreadcrumb();
  }

  private initBreadcrumb() {
    let rows = this.breadcrumbService.getBreadcrumb();
    if (rows[rows.length - 1].title != this.labelsApp.limites.title_index) {
      let breadcrumb_father: IBreadcrumb = { title: this.labelsApp.limites.title_index, url: this.urls.limites.index };
      this.breadcrumbService.addItemBreadcrumb(breadcrumb_father);
    }
    let breadcrumb: IBreadcrumb = { title: this.title, url: this.rutaAccion, };
    this.breadcrumbService.validationAddItem(breadcrumb);
  }

  onSubmit() {
    this.permisosAppService.onSubmit(this.limiteModel, this.itemForm, this.operacion, this.urls.limites.index);
  }



}
