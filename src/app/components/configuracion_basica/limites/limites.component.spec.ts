import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LimitesComponent } from './limites.component';

describe('SeccionesComponent', () => {
  let component: LimitesComponent;
  let fixture: ComponentFixture<LimitesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LimitesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LimitesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
