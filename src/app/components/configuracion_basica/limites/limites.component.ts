import { Component, OnInit, ViewChild } from "@angular/core";
import { LimiteModel } from "../../../classmodels/models/limite-model";

import { IBreadcrumb } from "src/app/classmodels/interfaces/ibreadcrumb";
import { labels_app } from 'src/app/config_app/labels-app';
import { urls_app } from 'src/app/config_app/urls-app';

import { ApiService } from "../../../services/api/api.service";
import { MessageBoxService } from 'src/app/services/message_box_app/message-box.service';
import { PermisosAppService } from 'src/app/services/permisos_app/permisos-app.service';
import { BreadcrumbService } from "../../../services/breadcrumb/breadcrumb.service";

import { PuntoGestionModel } from 'src/app/classmodels/models/punto-gestion-model';
import { VariableModel } from 'src/app/classmodels/models/variable-model';
import { TipoUsoModel } from 'src/app/classmodels/models/tipo-uso-model';


@Component({
  selector: "app-limites",
  templateUrl: "./limites.component.html",
  styleUrls: ["./limites.component.sass"],
})
/**
 * class LimitesComponent{}
 * se encarga de los  procesos de listar, eliminación de
 * para la base de datos
 * además de hacer redireccionamiento a rutas de creación y modificación
 * @authors
 * @author Johan Mauricio Fonseca Molano
 * @author Brayan Hernan Castillo Rodriguez
 * @author Federico Estupiñan Carreño
 */
export class LimitesComponent implements OnInit {
  labelsApp: any;
  urls: any;
  itemsRol: any = [];
  nombreEstadoTrue: string = "Activo";
  nombreEstadoFalse: string = "Inactivo";

  permisosAppSeccion: any = [];
  limiteModel: LimiteModel;
  puntoGestionModel: PuntoGestionModel;
  variableModel: VariableModel;
  tipoUsoModel: TipoUsoModel;



  constructor(private apiService: ApiService, private permisosAppService: PermisosAppService, private breadcrumbService: BreadcrumbService, private messageBoxService: MessageBoxService) {

    this.labelsApp = labels_app;
    this.urls = urls_app;
    
    this.limiteModel = new LimiteModel();
    this.limiteModel.setApiService(this.apiService);
    this.limiteModel.setMessageBoxService(this.messageBoxService);
    this.limiteModel.refresh();

    this.puntoGestionModel = new PuntoGestionModel();
    this.puntoGestionModel.setApiService(this.apiService);
    this.puntoGestionModel.refresh();

    this.variableModel = new VariableModel();
    this.variableModel.setApiService(this.apiService);
    this.variableModel.refresh();

    this.tipoUsoModel = new TipoUsoModel();
    this.tipoUsoModel.setApiService(this.apiService);
    this.tipoUsoModel.refresh();

    this.initBreadcrumb();
  }

  async ngOnInit() {
    this.initBreadcrumb();
  }



  private initBreadcrumb() {
    this.breadcrumbService.initBreadcrumb();
    let breadcrumb: IBreadcrumb = {
      title: this.labelsApp.limites.title_index,
      url: this.urls.limites.index,
    };
    this.breadcrumbService.validationAddItem(breadcrumb);
  }

 


  onClickCrear() {
    this.permisosAppService.onClickCrear(this.urls.limites.crear)
  }

  onClickModificar(model) {
    this.permisosAppService.onClickModificar(this.urls.limites.modificar, model.id)
  }

  onClickEliminar(model) {
    this.permisosAppService.onClickEliminar(model.id, this.limiteModel, this.urls.limites.index)
  }

  nombrePuntoGestion(id_punto_gestion: number) {
    let items = this.puntoGestionModel.getItems();

    for (let i = 0; i < items.length; i++) {
      if (id_punto_gestion === items[i]["id"]) {
        return items[i]["nombre"];
      }
    }
  }

  nombreVariable(id_variable: number) {
    let items = this.variableModel.getItems();

    for (let i = 0; i < items.length; i++) {
      if (id_variable === items[i]["id"]) {
        return items[i]["nombre"];
      }
    }
  }

  nombreTipoUso(id_tipo_uso: number) {
    let items = this.tipoUsoModel.getItems();

    for (let i = 0; i < items.length; i++) {
      if (id_tipo_uso === items[i]["id"]) {
        return items[i]["nombre"];
      }
    }
  }








}
