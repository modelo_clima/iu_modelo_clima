import { Component, OnInit } from "@angular/core";
import { IBreadcrumb } from "src/app/classmodels/interfaces/ibreadcrumb";
import { labels_app } from 'src/app/config_app/labels-app';
import { urls_app } from 'src/app/config_app/urls-app';

import { ApiService } from "../../../services/api/api.service";
import { MessageBoxService } from 'src/app/services/message_box_app/message-box.service';
import { PermisosAppService } from 'src/app/services/permisos_app/permisos-app.service';
import { BreadcrumbService } from "../../../services/breadcrumb/breadcrumb.service";

import { VariableModel } from "src/app/classmodels/models/variable-model";
import { UnidadMedidaModel } from 'src/app/classmodels/models/unidad-medida-model';
import { TipoVariableModel } from 'src/app/classmodels/models/tipo-variable-model';

@Component({
  selector: "app-variables",
  templateUrl: "./variables.component.html",
  styleUrls: ["./variables.component.sass"],
})
/**
 * class VariablesComponent{}
 * se encarga de los  procesos de listar, eliminación de
 * para la base de datos
 * además de hacer redireccionamiento a rutas de creación y modificación
 * @authors
 * @author Johan Mauricio Fonseca Molano
 * @author Brayan Hernan Castillo Rodriguez
 * @author Federico Estupiñan Carreño
 */
export class VariablesComponent implements OnInit {

  labelsApp: any;
  urls: any;
  itemsRol: any = [];
  nombreEstadoTrue: string = "Activo";
  nombreEstadoFalse: string = "Inactivo";

  variableModel: VariableModel;
  unidadMedidaModel: UnidadMedidaModel;
  tipoVariableModel: TipoVariableModel;

  permisosAppSeccion: any = [];
  
  constructor(private apiService: ApiService, private permisosAppService: PermisosAppService, private breadcrumbService: BreadcrumbService, private messageBoxService: MessageBoxService) {

    this.labelsApp = labels_app;
    this.urls = urls_app;

    this.variableModel = new VariableModel();
    this.variableModel.setApiService(this.apiService);
    this.variableModel.setMessageBoxService(this.messageBoxService);
    this.variableModel.refresh();
    

    this.unidadMedidaModel = new UnidadMedidaModel();
    this.unidadMedidaModel.setApiService(this.apiService);
    this.unidadMedidaModel.refresh();

    this.tipoVariableModel = new TipoVariableModel();
    this.tipoVariableModel.setApiService(this.apiService);
    this.tipoVariableModel.refresh();
  
    this.initBreadcrumb();
  }

  async ngOnInit() {
    this.initBreadcrumb();
  }


  private initBreadcrumb() {
    this.breadcrumbService.initBreadcrumb();
    let breadcrumb: IBreadcrumb = {
      title: this.labelsApp.variables.title_index,
      url: this.urls.variables.index,
    };
    this.breadcrumbService.validationAddItem(breadcrumb);
  }

  onClickCrear() {
    this.permisosAppService.onClickCrear(this.urls.variables.crear)
  }

  onClickModificar(model) {
    this.permisosAppService.onClickModificar(this.urls.variables.modificar, model.id)
  }

  onClickEliminar(model) {
    this.permisosAppService.onClickEliminar(model.id, this.variableModel, this.urls.variables.index)
  }

  nombreUnidadMedida(id_unidad_medida: number) {
    let items = this.unidadMedidaModel.getItems();

    for (let i = 0; i < items.length; i++) {
      if (id_unidad_medida === items[i]["id"]) {
        return items[i]["nombre"];
      }
    }
  }

  nombreTipoVariablea(id_tipo_variable: number) {
    let items = this.tipoVariableModel.getItems();

    for (let i = 0; i < items.length; i++) {
      if (id_tipo_variable === items[i]["id"]) {
        return items[i]["nombre"];
      }
    }
  }






}
