import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";

import { IUnidadMedida } from "../../../../classmodels/interfaces/iunidad-medida";
import { UnidadMedidaModel } from "../../../../classmodels/models/unidad-medida-model";

import { IBreadcrumb } from "../../../../classmodels/interfaces/ibreadcrumb";
import { labels_app } from '../../../../config_app/labels-app';
import { urls_app } from '../../../../config_app/urls-app';

import { ApiService } from "../../../../services/api/api.service";
import { MessageBoxService } from '../../../../services/message_box_app/message-box.service';
import { PermisosAppService } from '../../../../services/permisos_app/permisos-app.service';
import { BreadcrumbService } from "../../../../services/breadcrumb/breadcrumb.service";

@Component({
  selector: "app-form-entidad",
  templateUrl: "./form.component.html",
  styleUrls: ["./form.component.sass"],
})
/**
 * class FormComponent{}
 * se encarga de los  procesos de creación y modificación de
 * para la base de datos
 * @authors
 * @author Johan Mauricio Fonseca Molano
 * @author Brayan Hernan Castillo Rodriguez
 */
export class FormComponent implements OnInit {
  labelsApp: any;
  urls: any;

  id: number;
  title: string;
  operacion: string;
  rutaAccion: string;
  itemForm: IUnidadMedida = { id: null, nombre: "",simbolo:"" };
  unidadMedidaModel: UnidadMedidaModel;

  constructor(private activatedRoute: ActivatedRoute,private apiService: ApiService,private permisosAppService: PermisosAppService,private breadcrumbService: BreadcrumbService,
    private messageBoxService: MessageBoxService
  ) {

    this.labelsApp = labels_app;
    this.urls = urls_app;

    this.unidadMedidaModel = new UnidadMedidaModel();
    this.unidadMedidaModel.setApiService(this.apiService);
    this.unidadMedidaModel.setMessageBoxService(this.messageBoxService);

    this.id = this.activatedRoute.snapshot.params.id;
    this.operacion = !this.id ? "crear" : "modificar";
    this.loadDataForm();

  }

  loadDataForm() {
    if (this.operacion == "modificar") {
      this.title = this.labelsApp.unidades_medidas.title_editar;
      this.rutaAccion = `${this.urls.unidades_medidas.modificar}/${this.id}`
      this.unidadMedidaModel.getItemDetail(this.id, () => {
        this.itemForm = this.unidadMedidaModel.getItem();
      });
    } else {
      this.title = this.labelsApp.unidades_medidas.title_crear;
      this.rutaAccion = this.urls.unidades_medidas.crear;
    }
  }

  ngOnInit() {
    this.initBreadcrumb();
  }

  private initBreadcrumb() {
    let rows = this.breadcrumbService.getBreadcrumb();
    if (rows[rows.length - 1].title != this.labelsApp.unidades_medidas.title_index) {
      let breadcrumb_father: IBreadcrumb = { title: this.labelsApp.unidades_medidas.title_index, url: this.urls.unidades_medidas.index };
      this.breadcrumbService.addItemBreadcrumb(breadcrumb_father);
    }
    let breadcrumb: IBreadcrumb = { title: this.title, url: this.rutaAccion, };
    this.breadcrumbService.validationAddItem(breadcrumb);
  }

  onSubmit() {
    this.permisosAppService.onSubmit(this.unidadMedidaModel, this.itemForm, this.operacion, this.urls.unidades_medidas.index);
  }
}
