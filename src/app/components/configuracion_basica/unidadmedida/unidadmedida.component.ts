import { Component, OnInit } from "@angular/core";

import { UnidadMedidaModel } from "../../../classmodels/models/unidad-medida-model";

import { IBreadcrumb } from "src/app/classmodels/interfaces/ibreadcrumb";
import { labels_app } from 'src/app/config_app/labels-app';
import { urls_app } from 'src/app/config_app/urls-app';

import { ApiService } from "../../../services/api/api.service";
import { MessageBoxService } from 'src/app/services/message_box_app/message-box.service';
import { PermisosAppService } from 'src/app/services/permisos_app/permisos-app.service';
import { BreadcrumbService } from "../../../services/breadcrumb/breadcrumb.service";
@Component({
  selector: "app-unidad-medida",
  templateUrl: "./unidadmedida.component.html",
  styleUrls: ["./unidadmedida.component.sass"],
})
/**
 * class UnidadesMedidasComponent{}
 * se encarga de los  procesos de listar, eliminación de
 * para la base de datos
 * además de hacer redireccionamiento a rutas de creación y modificación
 * @authors
 * @author Johan Mauricio Fonseca Molano
 * @author Brayan Hernan Castillo Rodriguez
 */
export class UnidadesMedidasComponent implements OnInit {
  labelsApp: any;
  urls: any;

  permisosAppSeccion: any = [];
  unidadMedidaModel: UnidadMedidaModel;


  constructor(private apiService: ApiService, private permisosAppService: PermisosAppService, private breadcrumbService: BreadcrumbService, private messageBoxService: MessageBoxService) {

    this.labelsApp = labels_app;
    this.urls = urls_app;

    this.unidadMedidaModel = new UnidadMedidaModel();
    this.unidadMedidaModel.setApiService(this.apiService);
    this.unidadMedidaModel.setMessageBoxService(this.messageBoxService);
    this.unidadMedidaModel.refresh();
    this.initBreadcrumb();
  }

  async ngOnInit() {
    this.initBreadcrumb();
  }

  private initBreadcrumb() {
    this.breadcrumbService.initBreadcrumb();
    let breadcrumb: IBreadcrumb = {
      title: this.labelsApp.unidades_medidas.title_index,
      url: this.urls.unidades_medidas.index,
    };
    this.breadcrumbService.validationAddItem(breadcrumb);
  }

  onClickCrear() {
    this.permisosAppService.onClickCrear(this.urls.unidades_medidas.crear)
  }

  onClickModificar(model) {
    this.permisosAppService.onClickModificar(this.urls.unidades_medidas.modificar, model.id)
  }

  onClickEliminar(model) {
    this.permisosAppService.onClickEliminar(model.id, this.unidadMedidaModel, this.urls.unidades_medidas.index)
  }
}