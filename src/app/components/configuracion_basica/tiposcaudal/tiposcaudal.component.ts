import { Component, OnInit } from "@angular/core";

import { TipoCaudalModel } from "../../../classmodels/models/tipo-caudal-model";

import { IBreadcrumb } from "src/app/classmodels/interfaces/ibreadcrumb";
import { labels_app } from 'src/app/config_app/labels-app';
import { urls_app } from 'src/app/config_app/urls-app';

import { ApiService } from "../../../services/api/api.service";
import { MessageBoxService } from 'src/app/services/message_box_app/message-box.service';
import { PermisosAppService } from 'src/app/services/permisos_app/permisos-app.service';
import { BreadcrumbService } from "../../../services/breadcrumb/breadcrumb.service";
@Component({
  selector: "app-tipos-caudal",
  templateUrl: "./tiposcaudal.component.html",
  styleUrls: ["./tiposcaudal.component.sass"],
})
/**
 * class TiposCaudalComponent{}
 * se encarga de los  procesos de listar, eliminación de
 * para la base de datos
 * además de hacer redireccionamiento a rutas de creación y modificación
 * @authors
 * @author Johan Mauricio Fonseca Molano
 * @author Brayan Hernan Castillo Rodriguez
 */
export class TiposCaudalComponent implements OnInit {
  labelsApp: any;
  urls: any;

  permisosAppSeccion: any = [];
  tipoCaudalModel: TipoCaudalModel;


  constructor(private apiService: ApiService, private permisosAppService: PermisosAppService, private breadcrumbService: BreadcrumbService, private messageBoxService: MessageBoxService) {

    this.labelsApp = labels_app;
    this.urls = urls_app;

    this.tipoCaudalModel = new TipoCaudalModel();
    this.tipoCaudalModel.setApiService(this.apiService);
    this.tipoCaudalModel.setMessageBoxService(this.messageBoxService);
    this.tipoCaudalModel.refresh();
    this.initBreadcrumb();
  }

  async ngOnInit() {
    this.initBreadcrumb();
  }

  private initBreadcrumb() {
    this.breadcrumbService.initBreadcrumb();
    let breadcrumb: IBreadcrumb = {
      title: this.labelsApp.tipos_caudales.title_index,
      url: this.urls.tipos_caudales.index,
    };
    this.breadcrumbService.validationAddItem(breadcrumb);
  }

  onClickCrear() {
    this.permisosAppService.onClickCrear(this.urls.tipos_caudales.crear)
  }

  onClickModificar(model) {
    this.permisosAppService.onClickModificar(this.urls.tipos_caudales.modificar, model.id)
  }

  onClickEliminar(model) {
    this.permisosAppService.onClickEliminar(model.id, this.tipoCaudalModel, this.urls.tipos_caudales.index)
  }
}