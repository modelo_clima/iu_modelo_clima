import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TiposCaudalComponent } from './tiposcaudal.component';

describe('TipocaudalComponent', () => {
  let component: TiposCaudalComponent;
  let fixture: ComponentFixture<TiposCaudalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TiposCaudalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TiposCaudalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
