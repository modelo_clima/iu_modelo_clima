import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoriasSeriesComponent } from './categoriasseries.component';

describe('CategoriasSeriesComponent', () => {
  let component: CategoriasSeriesComponent;
  let fixture: ComponentFixture<CategoriasSeriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CategoriasSeriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoriasSeriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
