import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TiposUsosComponent } from './tiposusos.component';

describe('TiposUsosComponent', () => {
  let component: TiposUsosComponent;
  let fixture: ComponentFixture<TiposUsosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TiposUsosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TiposUsosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
