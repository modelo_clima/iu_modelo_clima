import { Component, OnInit } from "@angular/core";

import { TipoUsoModel } from "../../../classmodels/models/tipo-uso-model";

import { IBreadcrumb } from "src/app/classmodels/interfaces/ibreadcrumb";
import { labels_app } from 'src/app/config_app/labels-app';
import { urls_app } from 'src/app/config_app/urls-app';

import { ApiService } from "../../../services/api/api.service";
import { MessageBoxService } from 'src/app/services/message_box_app/message-box.service';
import { PermisosAppService } from 'src/app/services/permisos_app/permisos-app.service';
import { BreadcrumbService } from "../../../services/breadcrumb/breadcrumb.service";
@Component({
  selector: "app-tipos-usos",
  templateUrl: "./tiposusos.component.html",
  styleUrls: ["./tiposusos.component.sass"],
})
/**
 * class TiposUsosComponent{}
 * se encarga de los  procesos de listar, eliminación de
 * para la base de datos
 * además de hacer redireccionamiento a rutas de creación y modificación
 * @authors
 * @author Johan Mauricio Fonseca Molano
 * @author Brayan Hernan Castillo Rodriguez
 */
export class TiposUsosComponent implements OnInit {
  labelsApp: any;
  urls: any;

  permisosAppSeccion: any = [];
  tipoUsoModel: TipoUsoModel;


  constructor(private apiService: ApiService, private permisosAppService: PermisosAppService, private breadcrumbService: BreadcrumbService, private messageBoxService: MessageBoxService) {

    this.labelsApp = labels_app;
    this.urls = urls_app;

    this.tipoUsoModel = new TipoUsoModel();
    this.tipoUsoModel.setApiService(this.apiService);
    this.tipoUsoModel.setMessageBoxService(this.messageBoxService);
    this.tipoUsoModel.refresh();
    this.initBreadcrumb();
  }

  async ngOnInit() {
    this.initBreadcrumb();
  }

  private initBreadcrumb() {
    this.breadcrumbService.initBreadcrumb();
    let breadcrumb: IBreadcrumb = {
      title: this.labelsApp.tipos_usos.title_index,
      url: this.urls.tipos_usos.index,
    };
    this.breadcrumbService.validationAddItem(breadcrumb);
  }

  onClickCrear() {
    this.permisosAppService.onClickCrear(this.urls.tipos_usos.crear)
  }

  onClickModificar(model) {
    this.permisosAppService.onClickModificar(this.urls.tipos_usos.modificar, model.id)
  }

  onClickEliminar(model) {
    this.permisosAppService.onClickEliminar(model.id, this.tipoUsoModel, this.urls.tipos_usos.index)
  }
}