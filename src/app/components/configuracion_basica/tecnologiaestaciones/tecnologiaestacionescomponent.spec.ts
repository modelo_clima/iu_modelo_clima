import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { TecnologiasEstacionesComponent } from "./tecnologiaestaciones.component";

describe("TecnologiasEstacionesComponent", () => {
  let component: TecnologiasEstacionesComponent;
  let fixture: ComponentFixture<TecnologiasEstacionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TecnologiasEstacionesComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TecnologiasEstacionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
