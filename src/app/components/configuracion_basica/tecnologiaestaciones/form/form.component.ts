import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";

import { ITecnologiaEstacion } from "../../../../classmodels/interfaces/itecnologia-estacion";
import { TecnologiaEstacionModel } from "../../../../classmodels/models/tecnologia-estacion-model";

import { IBreadcrumb } from "../../../../classmodels/interfaces/ibreadcrumb";
import { labels_app } from '../../../../config_app/labels-app';
import { urls_app } from '../../../../config_app/urls-app';

import { ApiService } from "../../../../services/api/api.service";
import { MessageBoxService } from '../../../../services/message_box_app/message-box.service';
import { PermisosAppService } from '../../../../services/permisos_app/permisos-app.service';
import { BreadcrumbService } from "../../../../services/breadcrumb/breadcrumb.service";

@Component({
  selector: "app-form-tecnologia_estacion",
  templateUrl: "./form.component.html",
  styleUrls: ["./form.component.sass"],
})
/**
 * class FormComponent{}
 * se encarga de los  procesos de creación y modificación de
 * para la base de datos
 * @authors
 * @author Johan Mauricio Fonseca Molano
 * @author Brayan Hernan Castillo Rodriguez
 */
export class FormComponent implements OnInit {
  labelsApp: any;
  urls: any;

  id: number;
  title: string;
  operacion: string;
  rutaAccion: string;
  itemForm: ITecnologiaEstacion = { id: null, nombre: "" };
  tecnologiaEstacionModel: TecnologiaEstacionModel;

  constructor(private activatedRoute: ActivatedRoute,private apiService: ApiService,private permisosAppService: PermisosAppService,private breadcrumbService: BreadcrumbService,
    private messageBoxService: MessageBoxService
  ) {

    this.labelsApp = labels_app;
    this.urls = urls_app;

    this.tecnologiaEstacionModel = new TecnologiaEstacionModel();
    this.tecnologiaEstacionModel.setApiService(this.apiService);
    this.tecnologiaEstacionModel.setMessageBoxService(this.messageBoxService);

    this.id = this.activatedRoute.snapshot.params.id;
    this.operacion = !this.id ? "crear" : "modificar";
    this.loadDataForm();

  }

  loadDataForm() {
    if (this.operacion == "modificar") {
      this.title = this.labelsApp.tecnologias_estaciones.title_editar;
      this.rutaAccion = `${this.urls.tecnologias_estaciones.modificar}/${this.id}`
      this.tecnologiaEstacionModel.getItemDetail(this.id, () => {
        this.itemForm = this.tecnologiaEstacionModel.getItem();
      });
    } else {
      this.title = this.labelsApp.tecnologias_estaciones.title_crear;
      this.rutaAccion = this.urls.tecnologias_estaciones.crear;
    }
  }

  ngOnInit() {
    this.initBreadcrumb();
  }

  private initBreadcrumb() {
    let rows = this.breadcrumbService.getBreadcrumb();
    if (rows[rows.length - 1].title != this.labelsApp.tecnologias_estaciones.title_index) {
      let breadcrumb_father: IBreadcrumb = { title: this.labelsApp.tecnologias_estaciones.title_index, url: this.urls.tecnologias_estaciones.index };
      this.breadcrumbService.addItemBreadcrumb(breadcrumb_father);
    }
    let breadcrumb: IBreadcrumb = { title: this.title, url: this.rutaAccion, };
    this.breadcrumbService.validationAddItem(breadcrumb);
  }

  onSubmit() {
    this.permisosAppService.onSubmit(this.tecnologiaEstacionModel, this.itemForm, this.operacion, this.urls.tecnologias_estaciones.index);
  }
}
