import { Component, OnInit } from "@angular/core";

import { ModeloModel } from "../../../classmodels/models/modelo-model";

import { IBreadcrumb } from "src/app/classmodels/interfaces/ibreadcrumb";
import { labels_app } from 'src/app/config_app/labels-app';
import { urls_app } from 'src/app/config_app/urls-app';

import { ApiService } from "../../../services/api/api.service";
import { MessageBoxService } from 'src/app/services/message_box_app/message-box.service';
import { PermisosAppService } from 'src/app/services/permisos_app/permisos-app.service';
import { BreadcrumbService } from "../../../services/breadcrumb/breadcrumb.service";
@Component({
  selector: "app-modelos",
  templateUrl: "./modelos.component.html",
  styleUrls: ["./modelos.component.sass"],
})
/**
 * class ModelosComponent{}
 * se encarga de los  procesos de listar, eliminación de
 * para la base de datos
 * además de hacer redireccionamiento a rutas de creación y modificación
 * @authors
 * @author Johan Mauricio Fonseca Molano
 * @author Brayan Hernan Castillo Rodriguez
 */
export class ModelosComponent implements OnInit {
  labelsApp: any;
  urls: any;

  permisosAppSeccion: any = [];
  modeloModel: ModeloModel;


  constructor(private apiService: ApiService, private permisosAppService: PermisosAppService, private breadcrumbService: BreadcrumbService, private messageBoxService: MessageBoxService) {

    this.labelsApp = labels_app;
    this.urls = urls_app;

    this.modeloModel = new ModeloModel();
    this.modeloModel.setApiService(this.apiService);
    this.modeloModel.setMessageBoxService(this.messageBoxService);
    this.modeloModel.refresh();
    this.initBreadcrumb();
  }

  async ngOnInit() {
    this.initBreadcrumb();
  }

  private initBreadcrumb() {
    this.breadcrumbService.initBreadcrumb();
    let breadcrumb: IBreadcrumb = {
      title: this.labelsApp.modelos.title_index,
      url: this.urls.modelos.index,
    };
    this.breadcrumbService.validationAddItem(breadcrumb);
  }

  onClickCrear() {
    this.permisosAppService.onClickCrear(this.urls.modelos.crear)
  }

  onClickModificar(model) {
    this.permisosAppService.onClickModificar(this.urls.modelos.modificar, model.id)
  }

  onClickEliminar(model) {
    this.permisosAppService.onClickEliminar(model.id, this.modeloModel, this.urls.modelos.index)
  }
}