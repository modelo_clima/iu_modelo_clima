import { Component, OnInit } from "@angular/core";
import { IBreadcrumb } from "src/app/classmodels/interfaces/ibreadcrumb";
import { labels_app } from 'src/app/config_app/labels-app';
import { urls_app } from 'src/app/config_app/urls-app';

import { ApiService } from "../../../services/api/api.service";
import { MessageBoxService } from 'src/app/services/message_box_app/message-box.service';
import { PermisosAppService } from 'src/app/services/permisos_app/permisos-app.service';
import { BreadcrumbService } from "../../../services/breadcrumb/breadcrumb.service";

import { SeccionModel } from "../../../classmodels/models/seccion-model";
import { PuntoGestionModel } from 'src/app/classmodels/models/punto-gestion-model';

@Component({
  selector: "app-secciones",
  templateUrl: "./secciones.component.html",
  styleUrls: ["./secciones.component.sass"],
})
/**
 * class SeccionesComponent{}
 * se encarga de los  procesos de listar, eliminación de
 * para la base de datos
 * además de hacer redireccionamiento a rutas de creación y modificación
 * @authors
 * @author Johan Mauricio Fonseca Molano
 * @author Brayan Hernan Castillo Rodriguez
 */
export class SeccionesComponent implements OnInit {
  labelsApp: any;
  urls: any;
  itemsRol: any = [];
  nombreEstadoTrue: string = "Activo";
  nombreEstadoFalse: string = "Inactivo";

  puntoGestionModel:PuntoGestionModel

  permisosAppSeccion: any = [];
  seccionModel: SeccionModel;


  constructor(private apiService: ApiService, private permisosAppService: PermisosAppService, private breadcrumbService: BreadcrumbService, private messageBoxService: MessageBoxService) {

    this.labelsApp = labels_app;
    this.urls = urls_app;

    this.seccionModel = new SeccionModel();
    this.seccionModel.setApiService(this.apiService);
    this.seccionModel.setMessageBoxService(this.messageBoxService);
    this.seccionModel.refresh();

    this.puntoGestionModel = new PuntoGestionModel();
    this.puntoGestionModel.setApiService(this.apiService);
    this.puntoGestionModel.refresh();

    this.initBreadcrumb();
  }

  async ngOnInit() {
    this.initBreadcrumb();
  }

  private initBreadcrumb() {
    this.breadcrumbService.initBreadcrumb();
    let breadcrumb: IBreadcrumb = {
      title: this.labelsApp.secciones.title_index,
      url: this.urls.secciones.index,
    };
    this.breadcrumbService.validationAddItem(breadcrumb);
  }

  onClickCrear() {
    this.permisosAppService.onClickCrear(this.urls.secciones.crear)
  }

  onClickModificar(model) {
    this.permisosAppService.onClickModificar(this.urls.secciones.modificar, model.id)
  }

  onClickEliminar(model) {
    this.permisosAppService.onClickEliminar(model.id, this.seccionModel, this.urls.secciones.index)
  }

  nombrePuntoGestion(id_punto_gestion: number) {
    let items = this.puntoGestionModel.getItems();

    for (let i = 0; i < items.length; i++) {
      if (id_punto_gestion === items[i]["id"]) {
        return items[i]["nombre"];
      }
    }
  }






}