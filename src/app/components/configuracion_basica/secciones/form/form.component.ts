import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";

import { ISeccion } from "../../../../classmodels/interfaces/iseccion";
import { SeccionModel } from "../../../../classmodels/models/seccion-model";

import { IBreadcrumb } from "../../../../classmodels/interfaces/ibreadcrumb";
import { labels_app } from '../../../../config_app/labels-app';
import { urls_app } from '../../../../config_app/urls-app';

import { ApiService } from "../../../../services/api/api.service";
import { MessageBoxService } from '../../../../services/message_box_app/message-box.service';
import { PermisosAppService } from '../../../../services/permisos_app/permisos-app.service';
import { BreadcrumbService } from "../../../../services/breadcrumb/breadcrumb.service";

import { PuntoGestionModel } from 'src/app/classmodels/models/punto-gestion-model';

@Component({
  selector: "app-form-seccion",
  templateUrl: "./form.component.html",
  styleUrls: ["./form.component.sass"],
})
/**
 * class FormComponent{}
 * se encarga de los  procesos de creación y modificación de
 * para la base de datos
 * @authors
 * @author Johan Mauricio Fonseca Molano
 * @author Brayan Hernan Castillo Rodriguez
 */
export class FormComponent implements OnInit {
  labelsApp: any;
  urls: any;

  id: number;
  title: string;
  operacion: string;
  rutaAccion: string;
  itemForm: ISeccion = { id: null, nombre: "",id_punto_gestion:null };
  seccionModel: SeccionModel;
  puntoGestionModel:PuntoGestionModel;

  constructor(private activatedRoute: ActivatedRoute,private apiService: ApiService,private permisosAppService: PermisosAppService,private breadcrumbService: BreadcrumbService,
    private messageBoxService: MessageBoxService
  ) {

    this.labelsApp = labels_app;
    this.urls = urls_app;

    this.seccionModel = new SeccionModel();
    this.seccionModel.setApiService(this.apiService);
    this.seccionModel.setMessageBoxService(this.messageBoxService);

    this.puntoGestionModel = new PuntoGestionModel();
    this.puntoGestionModel.setApiService(this.apiService);
    this.puntoGestionModel.refresh();    

    this.id = this.activatedRoute.snapshot.params.id;
    this.operacion = !this.id ? "crear" : "modificar";
    this.loadDataForm();

  }

  loadDataForm() {
    if (this.operacion == "modificar") {
      this.title = this.labelsApp.secciones.title_editar;
      this.rutaAccion = `${this.urls.secciones.modificar}/${this.id}`
      this.seccionModel.getItemDetail(this.id, () => {
        this.itemForm = this.seccionModel.getItem();
      });
    } else {
      this.title = this.labelsApp.secciones.title_crear;
      this.rutaAccion = this.urls.secciones.crear;
    }
  }

  ngOnInit() {
    this.initBreadcrumb();
  }

  private initBreadcrumb() {
    let rows = this.breadcrumbService.getBreadcrumb();
    if (rows[rows.length - 1].title != this.labelsApp.secciones.title_index) {
      let breadcrumb_father: IBreadcrumb = { title: this.labelsApp.secciones.title_index, url: this.urls.secciones.index };
      this.breadcrumbService.addItemBreadcrumb(breadcrumb_father);
    }
    let breadcrumb: IBreadcrumb = { title: this.title, url: this.rutaAccion, };
    this.breadcrumbService.validationAddItem(breadcrumb);
  }

  onSubmit() {
    this.permisosAppService.onSubmit(this.seccionModel, this.itemForm, this.operacion, this.urls.secciones.index);
  }
}
