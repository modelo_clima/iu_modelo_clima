import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PuntosGestionComponent } from './puntosgestion.component';

describe('PuntosGestionComponent', () => {
  let component: PuntosGestionComponent;
  let fixture: ComponentFixture<PuntosGestionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PuntosGestionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PuntosGestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
