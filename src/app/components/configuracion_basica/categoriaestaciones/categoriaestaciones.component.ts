import { Component, OnInit } from "@angular/core";

import { CategoriaEstacionModel } from "../../../classmodels/models/categoria-estacion-model";

import { IBreadcrumb } from "src/app/classmodels/interfaces/ibreadcrumb";
import { labels_app } from 'src/app/config_app/labels-app';
import { urls_app } from 'src/app/config_app/urls-app';

import { ApiService } from "../../../services/api/api.service";
import { MessageBoxService } from 'src/app/services/message_box_app/message-box.service';
import { PermisosAppService } from 'src/app/services/permisos_app/permisos-app.service';
import { BreadcrumbService } from "../../../services/breadcrumb/breadcrumb.service";
@Component({
  selector: "app-categorias-estaciones",
  templateUrl: "./categoriaestaciones.component.html",
  styleUrls: ["./categoriaestaciones.component.sass"],
})
/**
 * class CategoriaEstacionesComponent{}
 * se encarga de los  procesos de listar, eliminación de
 * para la base de datos
 * además de hacer redireccionamiento a rutas de creación y modificación
 * @authors
 * @author Johan Mauricio Fonseca Molano
 * @author Brayan Hernan Castillo Rodriguez
 */
export class CategoriaEstacionesComponent implements OnInit {
  labelsApp: any;
  urls: any;

  permisosAppSeccion: any = [];
  categoriaEstacionModel: CategoriaEstacionModel;


  constructor(private apiService: ApiService, private permisosAppService: PermisosAppService, private breadcrumbService: BreadcrumbService, private messageBoxService: MessageBoxService) {

    this.labelsApp = labels_app;
    this.urls = urls_app;

    this.categoriaEstacionModel = new CategoriaEstacionModel();
    this.categoriaEstacionModel.setApiService(this.apiService);
    this.categoriaEstacionModel.setMessageBoxService(this.messageBoxService);
    this.categoriaEstacionModel.refresh();
    this.initBreadcrumb();
  }

  async ngOnInit() {
    this.initBreadcrumb();
  }

  private initBreadcrumb() {
    this.breadcrumbService.initBreadcrumb();
    let breadcrumb: IBreadcrumb = {
      title: this.labelsApp.categorias_estaciones.title_index,
      url: this.urls.categorias_estaciones.index,
    };
    this.breadcrumbService.validationAddItem(breadcrumb);
  }

  onClickCrear() {
    this.permisosAppService.onClickCrear(this.urls.categorias_estaciones.crear)
  }

  onClickModificar(model) {
    this.permisosAppService.onClickModificar(this.urls.categorias_estaciones.modificar, model.id)
  }

  onClickEliminar(model) {
    this.permisosAppService.onClickEliminar(model.id, this.categoriaEstacionModel, this.urls.categorias_estaciones.index)
  }
}