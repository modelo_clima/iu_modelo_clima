import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoriaEstacionesComponent } from './categoriaestaciones.component';

describe('CategoriaEstacionesComponent', () => {
  let component: CategoriaEstacionesComponent;
  let fixture: ComponentFixture<CategoriaEstacionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CategoriaEstacionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoriaEstacionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
