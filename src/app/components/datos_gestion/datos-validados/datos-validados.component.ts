import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api/api.service';
import { BreadcrumbService } from 'src/app/services/breadcrumb/breadcrumb.service';
import { Router } from '@angular/router';
import { IBreadcrumb } from 'src/app/classmodels/interfaces/ibreadcrumb';
import { MessageBoxSise } from '../../template/alertas-sise/message-box';
import { MessageBoxService } from 'src/app/services/message_box_app/message-box.service';
import * as jQuery from 'jquery';

function changeLabelInputFile(label_txt) {
  if (label_txt == null) {
    jQuery('div.custom-file input.custom-file-input').val(null);
    jQuery('div.custom-file label.custom-file-label').text('Seleccionar Archivo');
  } else {
    jQuery('div.custom-file label.custom-file-label').text(label_txt)
  }
}

@Component({
  selector: 'app-datos-validados',
  templateUrl: './datos-validados.component.html',
  styleUrls: ['./datos-validados.component.sass']
})
export class DatosValidadosComponent implements OnInit {

  title: string = "Datos validados";
  itemFormBqs: any;
  graficaSelect: any = {};
  itemForm: any;

  series: any;
  rescursos: any;

  showHelp: boolean = false

  constructor(private apiService: ApiService, private breadcrumbService: BreadcrumbService, private router: Router, private messageBoxService: MessageBoxService) {
    this.initBreadcrumb();
    this.apiService.loadingShow = false;
    this.graficaSelect = { src: '', titulo: '', ver: false };
    this.itemFormBqs = { fechaUno: '', fechaDos: '', id_variable: '', id_estacion: '' };
    this.itemForm = { ver: false, datos: { file_name: null } };
    this.series = {
      datos: [],
      grafica: {}
    };
    this.rescursos = { variables: [], estaciones: [] };
    this.recursosDatosValidados();
  }

  ngOnInit() {
  }

  private initBreadcrumb() {
    this.breadcrumbService.initBreadcrumb();
    let breadcrumb: IBreadcrumb = { 'title': this.title, 'url': '/datosvalidados' };
    this.breadcrumbService.validationAddItem(breadcrumb);
  }

  private recursosDatosValidados() {
    this.apiService.loadingShow = true;
    this.apiService.list('datosvalidados/resources').subscribe(data => {
      this.rescursos = data;
      this.apiService.loadingShow = false;
    }, error => {
      this.messageBoxService.showMessageError('Se presentó un error.');
      this.apiService.loadingShow = false;
    });
  }

  onSubmitBsq() {
    this.apiService.loadingShow = true;
    this.apiService.list('datosvalidados', this.itemFormBqs).subscribe(data => {
      this.series = data;
      this.apiService.loadingShow = false;
      if (this.series.datos.length == 0) {
        this.messageBoxService.showMessageError('No se encontraron datos con ese rango de fechas.');
      }
    }, error => {
      this.messageBoxService.showMessageError('Se presentó un error.');
      this.apiService.loadingShow = false;

    });
  }

  verGrafica(grafica, titulo) {
    this.graficaSelect.src = grafica.img;
    this.graficaSelect.ver = true;
    this.graficaSelect.titulo = titulo;
  }

  onClickRegistrar() {
    this.itemForm.ver = true;
    changeLabelInputFile(null);
    this.LoadFile = () => { return null; }
  }

  onChangeFile(e) {
    if (e.target.files.length > 0) {
      changeLabelInputFile(e.target.files[0].name);
      this.LoadFile = () => {
        let form = new FormData();
        form.append('file_name', e.target.files[0]);
        return form;
      }
    } else {
      changeLabelInputFile(null);
      this.LoadFile = () => { return null; }
    }
  }

  LoadFile() {
    return null;
  }

  guardar() {
    let form = this.LoadFile();
    if (form == null) {
      this.messageBoxService.showMessageError('Debe seleccionar un archivo.');
      this.apiService.loadingShow = false;
    } else {
      this.messageBoxService.showMessageConfirmation('Si hay fechas registradas, se modificaran los datos con los valores del archivo. ¿Desea continuar?',
        () => {
          this.messageBoxService.closeMessage();
          this.apiService.loadingShow = true;
          this.apiService.create('datosvalidados', form).subscribe(data => {
            this.apiService.loadingShow = false;
            this.itemForm.ver = false;
            this.messageBoxService.showMessage('Datos Guardados.');
          }, error => {
            this.messageBoxService.showMessageError('Se presentó un error.');
            this.apiService.loadingShow = false;
          });
        });
    }
  }
}
