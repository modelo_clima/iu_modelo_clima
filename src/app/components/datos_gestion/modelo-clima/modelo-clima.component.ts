import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api/api.service';
import { BreadcrumbService } from 'src/app/services/breadcrumb/breadcrumb.service';
import { Router } from '@angular/router';
import { IBreadcrumb } from 'src/app/classmodels/interfaces/ibreadcrumb';
import { MessageBoxService } from 'src/app/services/message_box_app/message-box.service';

@Component({
  selector: 'app-modelo-clima',
  templateUrl: './modelo-clima.component.html',
  styleUrls: ['./modelo-clima.component.sass']
})
export class ModeloClimaComponent implements OnInit {

  title: string = "Modelo clima";
  itemFormBqs: any;
  series: any;
  graficaSelect: any = {};
  alert_sise: any
  showHelp: boolean = false
  rescursos: any;

  constructor(private apiService: ApiService, private breadcrumbService: BreadcrumbService, private router: Router, private messageBoxService: MessageBoxService) {
    this.initBreadcrumb();
    this.apiService.loadingShow = false;
    this.graficaSelect = { src: '', titulo: '', ver: false };
    this.itemFormBqs = { fecha: '', periodo: 'mensual', id_estacion: '' };
    this.alert_sise = { message: '', type: '', show: false };
    this.series = {
      resultados: {},
      graficas: []
    };
    this.rescursos = { estaciones: [] };
    this.recursosDatosValidados();
  }

  ngOnInit() {
  }

  private recursosDatosValidados() {
    this.apiService.loadingShow = true;
    this.apiService.list('modelo_clima/resources').subscribe(data => {
      this.rescursos = data;
      this.apiService.loadingShow = false;
    }, error => {
      this.messageBoxService.showMessageError('Se presentó un error.');
      this.apiService.loadingShow = false;
    });
  }

  onSubmitBsq() {
    let fecha = (this.itemFormBqs.fecha + '').split('-');

    let params = {
      year: fecha[0],
      month: fecha[1],
      periodo: this.itemFormBqs.periodo,
      id_estacion: this.itemFormBqs.id_estacion
    };

    this.apiService.loadingShow = true;
    this.apiService.list('modelo_clima/series', params).subscribe(data => {
      this.series = data;
      this.apiService.loadingShow = false;
    }, error => {
      this.messageBoxService.showMessageError(error.error.detail + '. No se pudo ejecutar el modelo debido a que no hay datos para esa fecha.');
      this.apiService.loadingShow = false;

    });
  }

  verGrafica(grafica, titulo) {
    this.graficaSelect.src = grafica.img;
    this.graficaSelect.ver = true;
    this.graficaSelect.titulo = titulo;
  }

  private initBreadcrumb() {
    this.breadcrumbService.initBreadcrumb();
    let breadcrumb: IBreadcrumb = { 'title': this.title, 'url': '/modeloclima' };
    this.breadcrumbService.validationAddItem(breadcrumb);
  }

}
