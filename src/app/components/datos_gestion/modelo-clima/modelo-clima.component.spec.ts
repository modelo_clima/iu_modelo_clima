import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModeloClimaComponent } from './modelo-clima.component';

describe('ModeloClimaComponent', () => {
  let component: ModeloClimaComponent;
  let fixture: ComponentFixture<ModeloClimaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModeloClimaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModeloClimaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
